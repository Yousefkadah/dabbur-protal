// Animation (Don't touch)
function delay(n) {
	n = n || 2000;
	return new Promise((done) => {
		 setTimeout(() => {
			  done();
		 }, n);
	});
}

// Animation - Homepage
function loadAnimation() {
	var tl = gsap.timeline();

	tl.set(".loading-screen", { 
		left: "0"
	});	

	tl.to(".loading-screen", {
		duration: .75,
		width: "100%",
		left: "calc(100% + 45vh)",
		ease: "Expo.easeInOut",
		delay: .75,
	});

	tl.set(".loading-screen", { 
		left: "calc(-100% - 45vh)" 
	});	
}

// Animation - Transition
function pageTransition() {
	var tl = gsap.timeline();

	tl.to(".loading-screen", {
		duration: .75,
		width: "100%",
		left: "0%",
		ease: "Expo.easeInOut",
	});

	tl.to(".loading-screen", {
		duration: .75,
		width: "100%",
		left: "calc(100% + 45vh)",
		ease: "Expo.easeInOut",
		delay: 0,
	});

	tl.set(".loading-screen", { 
		left: "calc(-100% - 45vh)" 
	});
}


// Animation - Loading page First time
function contentAnimation() {
	var tl = gsap.timeline();

	tl.set(".animate-fade-in", { 
		y: 50, 
		opacity: 0
	});

	tl.to(".animate-fade-in", { 
		ease: "Expo.easeInOut",
		duration: 1.5, 
		y: 0, 
		opacity: 1, 
		stagger: 0, 
		delay: 0
	});

	
}

if ('scrollRestoration' in history) { history.scrollRestoration = 'manual'; }

// Call animation 
$(function () {
	barba.init({
		
		sync: true,

		transitions: [
			{
				async afterEnter(data) {
					contentAnimation();
				},

				async once(data) {
					loadAnimation();
					await delay(750);
					contentAnimation();
				},
				
				async leave(data) {
					const done = this.async();

					pageTransition();
					await delay(750);
					$(window).scrollTop(0);
					
					done();
					
				},
			},
		],
	});
});

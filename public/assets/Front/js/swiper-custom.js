// Barba Load Wrapper
barba.hooks.beforeEnter(() => {

   var swiper = new Swiper('.swiper-images-slider', {
      speed: 500,
      touch: true,
      grabCursor: true,
      slidesPerView: 3,
      spaceBetween: 30,
      loop: false,
      navigation: {
         nextEl: '.swiper-button-next-images-slider',
         prevEl: '.swiper-button-prev-images-slider',
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        980: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
      }
   });

   var swiper = new Swiper('.swiper-quotes', {
      speed: 500,
      touch: true,
      grabCursor: true,
      slidesPerView: 1,
      centeredSlides: true,
      navigation: {
          nextEl: '.swiper-button-next-quotes-slider',
          prevEl: '.swiper-button-prev-quotes-slider',
      },
  }); 

});    
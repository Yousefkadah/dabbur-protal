// Barba Load Wrapper
barba.hooks.beforeEnter(() => {


	var iframe = document.querySelector('#vimeo-iframe');
	var player = new Vimeo.Player(iframe);

	player.on('play', function() {
	  console.log('Played the video');
	  $(".home-header-bg").addClass('loaded');
	});

	$(document).ready(function(){
		function video() {
			var video = document.querySelector('.video-wrapper');
	  
			if (video !== null) {
			  var wrapperWidth = window.outerWidth,
					videoWidth = video.offsetWidth,
					videoHeight = video.offsetHeight; //this is to get around the elastic url bar on mobiles like ios...
	  
			  if (wrapperWidth < 1024) {
				 var wrapperHeight = window.innerHeight + 100;
			  } else {
				 var wrapperHeight = window.innerHeight;
			  }
	  
			  var scale = Math.max(wrapperWidth / videoWidth, wrapperHeight / videoHeight);
			  document.querySelector('.video-wrapper').style.transform = "translate(-50%, -50%) " + "scale(" + scale + ")";
			}
		 }
	  
		 video();
	  
		 window.onresize = function (event) {
			video();
		 };
 
	});
	
});

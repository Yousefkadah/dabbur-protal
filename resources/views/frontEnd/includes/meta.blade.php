<meta charset="utf-8">
<meta name="description" content="{{$PageDescription}}"/>
<meta name="keywords" content="{{$PageKeywords}}"/>
<meta name="author" content="{{ URL::to('') }}"/>
<link rel="shortlink" href="{{ url()->full()  }}" />
<link rel="canonical" href="{{ url()->full()  }}" />
<link rel="image_src" href="{{ URL::to('uploads/settings/'.Helper::GeneralSiteSettings("style_logo_" . @Helper::currentLanguage()->code)) }}" />
<meta name="Generator" content="Yousef Kadah" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />

<meta name="robots" content="index, follow">

<!-- Start Google meta -->
<meta property='og:title'
      content='{{$PageTitle}} {{($PageTitle =="")? Helper::GeneralSiteSettings("site_title_" . trans('backLang.boxCode')):""}}'/>
@if(@$Topic->photo_file !="")
    <meta property='og:image' content='{{ URL::asset('uploads/topics/'.@$Topic->photo_file) }}'/>
@elseif(Helper::GeneralSiteSettings("style_apple") !="")
    <meta property='og:image'
          content='{{ URL::asset('uploads/settings/'.Helper::GeneralSiteSettings("style_apple")) }}'/>
@else
    <meta property='og:image'
          content='{{ URL::asset('uploads/settings/nofav.png') }}'/>
@endif
<meta property="og:site_name" content="{{ Helper::GeneralSiteSettings("site_title_" . trans('backLang.boxCode')) }}">
<meta property="og:description" content="{{$PageDescription}}"/>
<meta property="og:url" content="{{ url()->full()  }}"/>
<meta property="og:type" content="website"/>
<!-- End Google meta -->

<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "url": "{{ URL::to('') }}",
      "logo": "{{ URL::to('uploads/settings/'.Helper::GeneralSiteSettings("style_logo_" . @Helper::currentLanguage()->code)) }}"
    }
</script>
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "WebSite",
      "url": "{{ URL::to('') }}",
      "potentialAction": [{
        "@type": "SearchAction",
        "target": {
          "@type": "EntryPoint",
          "urlTemplate": "{{ URL::to('') }}/search?search_word={search_term_string}"
        },
        "query-input": "required name=search_term_string"
      }]
    }
</script>
<?php
    // Get list of footer menu links by group Id
    $HeaderMenuLinks = Helper::MenuList(Helper::GeneralWebmasterSettings("header_menu_id"));
    // Current Full URL
    $fullPagePath = Request::url();
    // Char Count of Backend folder Plus 1
    $envAdminCharCount = strlen(env('BACKEND_PATH')) + 1;
    // URL after Root Path EX: admin/home
    $urlAfterRoot = substr($fullPagePath, strpos($fullPagePath, env('BACKEND_PATH')) + $envAdminCharCount);
    ?>
    <?php
    $category_title_var = "title_" . @Helper::currentLanguage()->code;
    $category_title_var2 = "title_" . env('DEFAULT_LANGUAGE');
    $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
    $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
?>
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [
        @foreach($HeaderMenuLinks as $HeaderMenuLink)
        <?php
        if ($HeaderMenuLink->$category_title_var != "") {
            $link_title = $HeaderMenuLink->$category_title_var;
        } else {
            $link_title = $HeaderMenuLink->$category_title_var2;
        }
        ?>
        @if($HeaderMenuLink->type==2)
        {
        "@type": "ListItem",
        "position": {{$loop->iteration}},
        "name": "{{ $link_title }}",
        "item": "{{ Helper::sectionURL($HeaderMenuLink->cat_id) }}"
        }
        @elseif($HeaderMenuLink->type==1)
        <?php
        // Direct Link
        $this_link_url = "";
        if ($HeaderMenuLink->link != "") {
            if (@Helper::currentLanguage()->code != env('DEFAULT_LANGUAGE')) {
                $f3c = mb_substr($HeaderMenuLink->link, 0, 3);
                if ($f3c == "htt" || $f3c == "www") {
                    $this_link_url = $HeaderMenuLink->link;
                } else {
                    $this_link_url = url(@Helper::currentLanguage()->code . "/" . $HeaderMenuLink->link);
                }
            } else {
                $this_link_url = url($HeaderMenuLink->link);
            }
        }
        ?>
        {
        "@type": "ListItem",
        "position": {{$loop->iteration}},
        "name": "{{ $link_title }}",
        "item": "{{ $this_link_url }}"
        }
        @endif
      @if (!$loop->last)
            ,
        @endif
      @endforeach
      ]
    }
    </script>
@yield('Meta')

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style>div#sliding-popup, div#sliding-popup .eu-cookie-withdraw-banner, .eu-cookie-withdraw-tab {background: #0779bf} div#sliding-popup.eu-cookie-withdraw-wrapper { background: transparent; } #sliding-popup h1, #sliding-popup h2, #sliding-popup h3, #sliding-popup p, #sliding-popup label, #sliding-popup div, .eu-cookie-compliance-more-button, .eu-cookie-compliance-secondary-button, .eu-cookie-withdraw-tab { color: #ffffff;} .eu-cookie-withdraw-tab { border-color: #ffffff;}</style>

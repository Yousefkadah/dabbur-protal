<div class="drawer">
    <div class="drawer-wrapper">
        <div class="content-wrapper">
            <div class="content">
              <div class="more-about">المزيد عن  iziENG</div>
              @if(count($HomeMoreLinks)>0)
                <?php
                    $title_var = "title_" . @Helper::currentLanguage()->code;
                    $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                ?>
                @foreach($HomeMoreLinks as $MoreLink)
                    @if ((($loop->index)%4) == 0)
                    <div class="row">
                    @endif
                    <a href="{{$MoreLink->link_url}}" class="page" target="_blank" rel="noreferrer" tabindex="-1">{{$MoreLink->$title_var}}</a>
                    @if ((($loop->index)%4) == 3 || $loop->last)
                    </div>
                    @endif
                @endforeach
              @endif
            </div>
        </div>
    </div>
</div>
<div class="header-data-pc">
        <div id="header-desktop">
        <div class="site-header-content clearfix">
            <div class="menu-top">
                <ul class="top-menu-list">
                    <li class="top-item">
                        @if(Helper::GeneralSiteSettings("contact_t3") !="")
                            <i class="fa fa-phone"></i> &nbsp;<a
                                href="tel:{{ Helper::GeneralSiteSettings("contact_t5") }}"><span dir="ltr">{{ Helper::GeneralSiteSettings("contact_t5") }}</span></a>
                        @endif
                    </li>
                    <li class="top-item">
                        @if(Helper::GeneralSiteSettings("contact_t6") !="")
                        <span class="top-email">&nbsp; | &nbsp;
                             <i class="fa fa-envelope"></i> &nbsp;<a href="mailto:{{ Helper::GeneralSiteSettings("contact_t6") }}">{{ Helper::GeneralSiteSettings("contact_t6") }}</a>
                    </span>
                    @endif
                    </li>
                </ul>
            </div>
            <div class="right">
              <div class="department-name">
                <a href="#" target="_blank" rel="noreferrer"></a>
              </div>
              <div class="drawer-toggle">
                <a href='#' title="Expand Drawer"><span class="fa fa-chevron-up"></span></a>
              </div>
              <div class="search-font accessibility">
                  <a class="small-size size-small" href='#'>A</a>
                  <a class="normal-size size-normal" href='#'>A</a>
                  <a class="large-size size-large" href='#'>A</a>
              </div>
              @if(count(Helper::languagesList()) >1)
                <div class="switch-language">
                <div class="ico-language"></div><span class="fa fa-caret-down" aria-hidden="true"></span>
                  <ul class="language">
                    @foreach(Helper::languagesList() as $ActiveLanguage)
                    <li>
                        <a href="{{ URL::to('lang/'.$ActiveLanguage->code) }}" class="dropdown-item">
                            @if($ActiveLanguage->icon !="")
                                <img src="{{ asset('assets/dashboard/images/flags/'.$ActiveLanguage->icon.".svg") }}" alt="{{ $ActiveLanguage->title }}">
                            @endif
                            {{ $ActiveLanguage->title }}
                        </a>
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
            </div>
          </div>
        </div>
        @include('frontEnd.includes.menu')
</div>

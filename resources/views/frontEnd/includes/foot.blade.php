<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://unpkg.com/@barba/core"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="https://player.vimeo.com/api/player.js"></script>
<script src='https://www.google.com/recaptcha/api.js?render=6LdQNc8ZAAAAANnlBc3p4GH15E2H_yZ0ULNZvjS7'></script>
<script>
// Barba Load Wrapper
barba.hooks.beforeEnter(() => {

    grecaptcha.ready(function() {
        grecaptcha.execute('6LdQNc8ZAAAAANnlBc3p4GH15E2H_yZ0ULNZvjS7', {action: 'submit'})
        .then(function(token) {
            document.getElementById('g-recaptcha-response').value=token;
        });
    });
});
</script>
<script src="{{ URL::asset('assets/Front/js/navigation.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/cookies.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/page-transition.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/scroll-on-click.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/observers.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/swiper-custom.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/video-background.js')}}"></script>
<script src="{{ URL::asset('assets/Front/js/slideNav.js')}}"></script>

{{-- Google Tags and google analytics --}}
@if($WebmasterSettings->google_tags_status && $WebmasterSettings->google_tags_id !="")
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id={!! $WebmasterSettings->google_tags_id !!}"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
@endif
@if($WebmasterSettings->google_analytics_code !="")
    {!! $WebmasterSettings->google_analytics_code !!}
@endif


<?php
if ($PageTitle == "") {
    $PageTitle = Helper::GeneralSiteSettings("site_title_" . @Helper::currentLanguage()->code);
}
?>
{!! Helper::SaveVisitorInfo($PageTitle) !!}

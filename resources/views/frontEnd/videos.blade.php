@extends('frontEnd.layout')
@section('content')
<link rel="stylesheet" media="all" href="{{ URL::asset('assets/Front/css/magnific-popup.css')}}"/>
<div class="standard-page">
    <div class="standard-page__header">
      <div class="header-banner__image">
        <picture>
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" media="(max-width: 768px)" type="image/jpeg">
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" type="image/jpeg">
          <img src="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" alt="" typeof="foaf:Image">
        </picture>
      </div>
      <div class="header-banner__container container">
        <div class="header-banner__text">
        <div class="block block-system block-system-breadcrumb-block">
            <div class="wrapper">
                <div class="block-body ">
                    <nav class="breadcrumb" role="navigation" aria-labelledby="system-breadcrumb">
                    <h2 id="system-breadcrumb" class="visually-hidden">Breadcrumb</h2>
                    <ol>
                        <li><a href="{{ route("Home") }}">{{__('backend.home')}}</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <h1 class="title">
            @if(@$WebmasterSection!="none")
                            <?php
                            $title_var = "title_" . @Helper::currentLanguage()->code;
                            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                            if (@$WebmasterSection->$title_var != "") {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var;
                            } else {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var2;
                            }
                            ?>
                            {!! $WebmasterSectionTitle !!}
                        @elseif(@$search_word!="")
                            {{ @$search_word }}
                        @else
                            {{ $User->name }}
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . @Helper::currentLanguage()->code;
                                ?>
                               - {{ $CurrentCategory->$category_title_var }}
                            @endif
                        @endif
        </h1>
        </div>
      </div>
    </div>
@if($Topics->total() == 0)
    <div class="alert alert-warning">
        <i class="fa fa-info"></i> &nbsp; {{ __('frontend.noData') }}
    </div>
@else
<div class="container">
    <div class="row">
        @if(count($Categories)>0)
            <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $category_title_var = "title_" . @Helper::currentLanguage()->code;
            $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
            $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
            ?>
            <div class="widget">
                <ul class="cat">
                    <li>
                        <a class="active" href="./">{{__('backend.all')}}</a></li>
                    @foreach($Categories as $Category)
                        <?php $active_cat = ""; ?>
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                @if($Category->id == $CurrentCategory->id)
                                    <?php $active_cat = "class=active"; ?>
                                @endif
                            @endif
                        @endif
                        <?php
                        if ($Category->$title_var != "") {
                            $Category_title = $Category->$title_var;
                        } else {
                            $Category_title = $Category->$title_var2;
                        }
                        $ccount = $category_and_topics_count[$Category->id];
                        ?>
                        <li>
                            @if($Category->icon !="")
                                <i class="fa {{$Category->icon}}"></i> &nbsp;
                            @endif
                            <a {{ $active_cat }} href="{{ Helper::categoryURL($Category->id) }}">{{ $Category_title }} <span>({{ $ccount }})</span></a></li>
                    @endforeach
                </ul>
            </div>

        @endif
    </div>
</div>

<section id="inner-headline">
    <div class="component" style="direction: ltr">
        <div class="video-gallery">
            <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $details_var = "details_" . @Helper::currentLanguage()->code;
            $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
            $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
            $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
            $i = 0;
            ?>
            @foreach($Topics as $Topic)
            <?php
            if ($Topic->$title_var != "") {
                $title = $Topic->$title_var;
            } else {
                $title = $Topic->$title_var2;
            }
            if ($Topic->$details_var != "") {
                $details = $details_var;
            } else {
                $details = $details_var2;
            }
            $section = "";
            try {
                if ($Topic->section->$title_var != "") {
                    $section = $Topic->section->$title_var;
                } else {
                    $section = $Topic->section->$title_var2;
                }
            } catch (Exception $e) {
                $section = "";
            }
            ?>
            <div class="gallery-item">
                <img src="@if($Topic->photo_file) {{URL::to('uploads/topics/'.$Topic->photo_file)}} @else {{URL::to('uploads/topics/16536009305816.jpg')}} @endif" alt="{{ $title }}" />
                <div class="gallery-item-caption">
                  <div>
                    <h5>{{$title}}</h5>
                  </div>
                  <a class="vimeo-popup" href="{{$Topic->video_file}}" title="{{ $title }}"></a>
                </div>
              </div>
          @endforeach
        </div>
      </div>
    </section>
    <div class="container">
        <div class="col-lg-8">
            {!! $Topics->appends(request()->input())->links() !!}
        </div>
        <div class="col-lg-4 text-right">
            <br>
            <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ __('backend.of') }}
                ( {{ $Topics->total()  }} ) {{ __('backend.records') }}</small>
        </div>
    </div>

@endif
</div>
<style>
    <style>
    .widget .cat{
        list-style: none;
        padding: 0;
    }
    .widget .cat li{
        display: inline-block;
        padding: 0.6rem 1.5rem;
        position: relative;
        border: 1px solid black;
        border-radius: 50px;
        margin-inline-end: 0.3rem;
        transition: 0.5s;
        margin-bottom: 0.6rem;
    }
    @media only screen and (max-width:767px){
        .widget .cat li{
            padding: 5px 10px;
            font-size: 12px;
        }
    }
    .widget .cat li:hover{
        background-color: rgba(0, 51, 102, .75);
        border-color: #faa61a;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    }
    .widget .cat li:hover a,.widget .cat li:hover span{color:white;}
    .widget .cat li a{ color:black}
    .btn {
        display: inline-block;
        text-transform: uppercase;
        border: 2px solid #2c3e50;
        margin-top: 100px;
        font-size: 0.7em;
        font-weight: 700;
        padding: 0.1em 0.4em;
        text-align: center;
        -webkit-transition: color 0.3s, border-color 0.3s;
        -moz-transition: color 0.3s, border-color 0.3s;
        transition: color 0.3s, border-color 0.3s;
        }

        .btn:hover {
        border-color: #16a085;
        color: #16a085;
        }
.video-gallery {
  position: relative;
  margin: 0 auto;
  max-width: 1000px;
  text-align: center;
}

.video-gallery .gallery-item {
  position: relative;
  float: left;
  overflow: hidden;
  padding: 10px 1%;
  min-width: 320px;
  max-width: 580px;
  max-height: 360px;
  width: 33%;
  background: #000;
  cursor: pointer;
}

.video-gallery .gallery-item img {
  position: relative;
  display: block;
  opacity: .45;
  width: 105%;
  height: 300px;
  transition: opacity 0.35s, transform 0.35s;
  backface-visibility: hidden;
}

.video-gallery .gallery-item .gallery-item-caption {
  padding: 2em;
  color: #fff;
  text-transform: uppercase;
  font-size: 1.25em;
}

.video-gallery .gallery-item .gallery-item-caption,
.video-gallery .gallery-item .gallery-item-caption > a {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}

.video-gallery .gallery-item h5 {
  font-weight: 300;
  overflow: hidden;
  padding: 0.5em 0;
}


.video-gallery .gallery-item h5 {
  position: relative;
  margin: 0;
  z-index: 10;
  text-align: center;
}

.video-gallery .gallery-item:hover img {
  opacity: .3;
  transform: translate3d(0, 0, 0);

}

.video-gallery .gallery-item .gallery-item-caption {
  text-align: left;
}


.video-gallery .gallery-item:hover p {
  opacity: 1;
}

@media screen and (max-width: 50em) {
  .video-gallery .gallery-item {
    display: inline-block;
    float: none;
    margin: 10px auto;
    width: 100%;
  }
}
</style>
@endsection

@section('footerInclude')
<script src="{{ URL::asset('assets/Front/js/jquery.magnific-popup.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            $('.video-gallery').magnificPopup({
            delegate: 'a',
            type: 'iframe',
            gallery:{
                enabled:true
            }
            });
        });

    </script>
@endsection

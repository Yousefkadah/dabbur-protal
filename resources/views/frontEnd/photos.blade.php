@extends('frontEnd.layout')

@section('content')
<div class="standard-page">
    <div class="standard-page__header">
      <div class="header-banner__image">
        <picture>
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" media="(max-width: 768px)" type="image/jpeg">
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" type="image/jpeg">
          <img src="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" alt="" typeof="foaf:Image">
        </picture>
      </div>
      <div class="header-banner__container container">
        <div class="header-banner__text">
        <div class="block block-system block-system-breadcrumb-block">
            <div class="wrapper">
                <div class="block-body ">
                    <nav class="breadcrumb" role="navigation" aria-labelledby="system-breadcrumb">
                    <h2 id="system-breadcrumb" class="visually-hidden">Breadcrumb</h2>
                    <ol>
                        <li><a href="{{ route("Home") }}">{{__('backend.home')}}</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <h1 class="title">
            @if(@$WebmasterSection!="none")
                            <?php
                            $title_var = "title_" . @Helper::currentLanguage()->code;
                            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                            if (@$WebmasterSection->$title_var != "") {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var;
                            } else {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var2;
                            }
                            ?>
                            {!! $WebmasterSectionTitle !!}
                        @elseif(@$search_word!="")
                            {{ @$search_word }}
                        @else
                            {{ $User->name }}
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . @Helper::currentLanguage()->code;
                                ?>
                                {{ $CurrentCategory->$category_title_var }}
                            @endif
                        @endif
        </h1>
        </div>
      </div>
    </div>
@if($Topics->total() == 0)
    <div class="alert alert-warning">
        <i class="fa fa-info"></i> &nbsp; {{ __('frontend.noData') }}
    </div>
@else
<?php
$title_var = "title_" . @Helper::currentLanguage()->code;
$title_var2 = "title_" . env('DEFAULT_LANGUAGE');
$details_var = "details_" . @Helper::currentLanguage()->code;
$details_var2 = "details_" . env('DEFAULT_LANGUAGE');
$slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
$slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
$i = 0;
?>
<section class="hero-section">
    <div class="card-grid">
    @foreach($Topics as $Topic)
        <?php
        if ($Topic->$title_var != "") {
            $title = $Topic->$title_var;
        } else {
            $title = $Topic->$title_var2;
        }
        if ($Topic->$details_var != "") {
            $details = $details_var;
        } else {
            $details = $details_var2;
        }
        $section = "";
        try {
            if ($Topic->section->$title_var != "") {
                $section = $Topic->section->$title_var;
            } else {
                $section = $Topic->section->$title_var2;
            }
        } catch (Exception $e) {
            $section = "";
        }
        $topic_link_url = Helper::topicURL($Topic->id);
        ?>
        <a class="card" href="{{ $topic_link_url }}">
        <div class="card__background" style="background-image: url({{ URL::to('uploads/topics/'.$Topic->photo_file) }})"></div>
        <div class="card__content">
            <p class="card__category"><i class="fa fa-calendar"></i> <time datetime="{!! Helper::formatDate($Topic->date)  !!}" class="datetime">{!! Helper::formatDate($Topic->date)  !!}</time></p>
            <h3 class="card__heading">{{$title}}</h3>
        </div>
        </a>
    @endforeach
    <div>
</section>
<div class="container">
    <div class="col-lg-8">
        {!! $Topics->appends(request()->input())->links() !!}
    </div>
    <div class="col-lg-4 text-right">
        <br>
        <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ __('backend.of') }}
            ( {{ $Topics->total()  }} ) {{ __('backend.records') }}</small>
    </div>
</div>
<style>
    :root{
    --text-light: rgba(255,255,255,0.6);
    --text-lighter: rgba(255,255,255,0.9);
    --spacing-s: 8px;
    --spacing-m: 16px;
    --spacing-l: 24px;
    --spacing-xl: 32px;
    --spacing-xxl: 64px;
    --width-container: 1200px;
    }
    .hero-section{
    align-items: flex-start;
    display: flex;
    min-height: 100%;
    justify-content: center;
    padding: var(--spacing-xxl) var(--spacing-l);
    }

    .card-grid{
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    grid-column-gap: var(--spacing-l);
    grid-row-gap: var(--spacing-l);
    max-width: var(--width-container);
    width: 100%;
    }

    @media(min-width: 540px){
    .card-grid{
        grid-template-columns: repeat(2, 1fr);
    }
    }

    @media(min-width: 960px){
    .card-grid{
        grid-template-columns: repeat(4, 1fr);
    }
    }

    .card{
    list-style: none;
    position: relative;
    }

    .card:before{
    content: '';
    display: block;
    padding-bottom: 150%;
    width: 100%;
    }

    .card__background{
    background-size: cover;
    background-position: center;
    border-radius: var(--spacing-l);
    bottom: 0;
    filter: brightness(0.75) saturate(1.2) contrast(0.85);
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    transform-origin: center;
    trsnsform: scale(1) translateZ(0);
    transition:
        filter 200ms linear,
        transform 200ms linear;
    }

    .card:hover .card__background{
    transform: scale(1.05) translateZ(0);
    }

    .card-grid:hover > .card:not(:hover) .card__background{
    filter: brightness(0.5) saturate(0) contrast(1.2) blur(20px);
    }

    .card__content{
    padding: var(--spacing-l);
    position: absolute;
    top: 0;
    }

    .card__category{
    color: var(--text-light);
    font-size: 0.9rem;
    margin-bottom: var(--spacing-s);
    text-transform: uppercase;
    }

    .card__heading{
    color: var(--text-lighter);
    font-size: 1.9rem;
    text-shadow: 2px 2px 20px rgba(0,0,0,0.2);
    line-height: 1.4;
    }
</style>
</div>
@endif
@endsection

@extends('frontEnd.layout')

@section('content')
<main role="main">
    <a id="main-content" tabindex="-1"></a>
    <div class="layout-content">
        <div class="region region-content fixed-width">
            <div data-drupal-messages-fallback="" class="hidden"></div>
            @include('frontEnd.includes.contactus')
            <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $details_var = "details_" . @Helper::currentLanguage()->code;
            $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
            $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
            $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
            $i = 0;
            ?>
<article data-history-node-id="61" role="article" about="/news" class="node node--type-standard-page node--promoted node--view-mode-full">
  <div class="standard-page">
    <div class="standard-page__header">
      <div class="header-banner__image">
        <picture>
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" media="(max-width: 768px)" type="image/jpeg">
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" type="image/jpeg">
          <img src="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" alt="" typeof="foaf:Image">
        </picture>
      </div>
      <div class="header-banner__container container">
        <div class="header-banner__text">
        <div class="block block-system block-system-breadcrumb-block">
            <div class="wrapper">
                <div class="block-body ">
                    <nav class="breadcrumb" role="navigation" aria-labelledby="system-breadcrumb">
                    <h2 id="system-breadcrumb" class="visually-hidden">Breadcrumb</h2>
                    <ol>
                        <li><a href="{{ route("Home") }}">{{__('backend.home')}}</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <h1 class="title">
            @if(@$WebmasterSection!="none")
                            <?php
                            $title_var = "title_" . @Helper::currentLanguage()->code;
                            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                            if (@$WebmasterSection->$title_var != "") {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var;
                            } else {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var2;
                            }
                            ?>
                            {!! $WebmasterSectionTitle !!}
                        @elseif(@$search_word!="")
                            {{ @$search_word }}
                        @else
                            {{ $User->name }}
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . @Helper::currentLanguage()->code;
                                ?>
                                {{ $CurrentCategory->$category_title_var }}
                            @endif
                        @endif
        </h1>
        </div>
      </div>
    </div>
   @if( $WebmasterSection->id==9)
  <div class="row">

 
        <div class="teamCard">
        <div class="directorcard">
            <div class="firstinfo">
                <img src="{{ URL::to('uploads/topics/'.$Topics->first()->photo_file) }}" alt="">
                <div class="profileinfo">
                    <h3>{{$Topics->first()->$title_var}}</h3>
                    <h4>{{$Topics->first()->$details_var}}</h4>
                    <p class="bio"></p>

                </div>
                <div class="badgescard">
                    <span class="devicons devicons-django"></span>
                    <span class="devicons devicons-python"></span>
                    <span class="devicons devicons-codepen"></span>
                    <span class="devicons devicons-django"></span>


                </div>
            </div>
        </div> 
    
    </div>  
  </div>
   <div class="teamCard">
       <?php
       $Topics=$Topics->skip(1)
       ?>
      @foreach ($Topics as $topic )
    <div class="content">
        <div class="card">
            <div class="firstinfo">
                <img src="{{ URL::to('uploads/topics/'.$topic->photo_file) }}" alt="">
                <div class="profileinfo">
                    <h3>{{$topic->title_ar}}</h3>
                    <h4>{{$topic->details_ar}}</h4>
                    <p class="bio"></p>

                </div>
                <div class="badgescard">
                    <span class="devicons devicons-django"></span>
                    <span class="devicons devicons-python"></span>
                    <span class="devicons devicons-codepen"></span>
                    <span class="devicons devicons-django"></span>


                </div>
            </div>
        </div> 
    </div>   
      @endforeach
   </div>
   @else
    <div class="standard-page__content">
        <div class="container">
          <div id="ust30a_news_filter">
              <div class="news-filter">
                <div class="news-filter__category-picker">
                      <select></select>
                </div>
            </div>
        </div>
        </div>
        <div class="container">
        <div class="field field--name-field-standardpage-modules field--type-entity-reference-revisions field--label-hidden field__items">
            <div class="field__item">
                <div class="paragraph paragraph--type--standard-page-views-module paragraph--view-mode--default">
                    <div class="field field--name-field-standard-page-views-ref field--type-viewsreference field--label-hidden field__item">
                        <div class="views-element-container">
                            <div class="view view-news view-id-news view-display-id-press_releases js-view-dom-id-f9d260ac283ac505d53a4dd548538fe6189e7d5faf36cd9e20429750eccaefdf">
                                <div class="view-content">
                                    @if($Topics->total() == 0)
                                        <div class="alert alert-warning">
                                            <i class="fa fa-info"></i> &nbsp; {{ __('frontend.noData') }}
                                        </div>
                                    @else

                                    @foreach($Topics as $Topic)
                                        <?php
                                        if ($Topic->$title_var != "") {
                                            $title = $Topic->$title_var;
                                        } else {
                                            $title = $Topic->$title_var2;
                                        }
                                        if ($Topic->$details_var != "") {
                                            $details = $details_var;
                                        } else {
                                            $details = $details_var2;
                                        }
                                        $section = "";
                                        try {
                                            if ($Topic->section->$title_var != "") {
                                                $section = $Topic->section->$title_var;
                                            } else {
                                                $section = $Topic->section->$title_var2;
                                            }
                                        } catch (Exception $e) {
                                            $section = "";
                                        }
                                        $topic_link_url = Helper::topicURL($Topic->id);
                                        ?>
                                    <div class="views-row">
                                        <article>
                                        <div class="views-field views-field-field-news-thumbnail">
                                            <div class="field-content">
                                                <a href="{{ $topic_link_url }}" class="conditional">
                                                    <figure role="group">
                                                        @if($Topic->webmasterSection->type==2 && $Topic->video_file!="")
                                                        @if($Topic->video_type ==1)
                                                            <?php
                                                            $Youtube_id = Helper::Get_youtube_video_id($Topic->video_file);
                                                            ?>
                                                            @if($Youtube_id !="")
                                                                {{-- Youtube Video --}}
                                                                <iframe allowfullscreen
                                                                        src="https://www.youtube.com/embed/{{ $Youtube_id }}">
                                                                </iframe>
                                                            @endif
                                                        @elseif($Topic->video_type ==2)
                                                            <?php
                                                            $Vimeo_id = Helper::Get_vimeo_video_id($Topic->video_file);
                                                            ?>
                                                            @if($Vimeo_id !="")
                                                                {{-- Vimeo Video --}}
                                                                <iframe allowfullscreen
                                                                        src="http://player.vimeo.com/video/{{ $Vimeo_id }}?title=0&amp;byline=0">
                                                                </iframe>
                                                            @endif

                                                        @elseif($Topic->video_type ==3)
                                                            @if($Topic->video_file !="")
                                                                {{-- Embed Video --}}
                                                                {!! $Topic->video_file !!}
                                                            @endif

                                                        @else
                                                            <video width="100%" height="300" controls>
                                                                <source
                                                                    src="{{ URL::to('uploads/topics/'.$Topic->video_file) }}"
                                                                    type="video/mp4">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        @endif
                                                        @elseif($Topic->webmasterSection->type==3 && $Topic->audio_file!="")
                                                            @if($Topic->photo_file !="")
                                                            <img src="{{ URL::to('uploads/topics/'.$Topic->photo_file) }}" width="812" height="812" alt="{{ $title }}" typeof="foaf:Image" loading="lazy" class="image-style-press-release-thumbnail" style="height:350px;object-fit:cover;">
                                                            @endif
                                                            <audio controls>
                                                                <source
                                                                    src="{{ URL::to('uploads/topics/'.$Topic->audio_file) }}"
                                                                    type="audio/mpeg">
                                                                Your browser does not support the audio element.
                                                            </audio>
                                                        @else
                                                            <img src="{{ URL::to('uploads/topics/'.$Topic->photo_file) }}" width="812" height="812" alt="{{ $title }}" typeof="foaf:Image" loading="lazy" class="image-style-press-release-thumbnail" style="height:350px;object-fit:cover;">
                                                        @endif
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="views-field views-field-title post-title">
                                            <h4 class="field-content">
                                                <a href="{{ $topic_link_url }}" class="conditional">
                                                    @if($Topic->icon !="")
                                                        <i class="fa {!! $Topic->icon !!} "></i>&nbsp;
                                                    @endif
                                                    {{ $title }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div class="views-field views-field-body-1">
                                            {{--Additional Feilds--}}
                                            @if(count($Topic->webmasterSection->customFields->where("in_listing",true)) >0)
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="col-lg-12">
                                                            <?php
                                                            $cf_title_var = "title_" . @Helper::currentLanguage()->code;
                                                            $cf_title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                                                            ?>
                                                            @foreach($Topic->webmasterSection->customFields->where("in_listing",true) as $customField)
                                                                <?php
                                                                // check permission
                                                                $view_permission_groups = [];
                                                                if ($customField->view_permission_groups != "") {
                                                                    $view_permission_groups = explode(",", $customField->view_permission_groups);
                                                                }
                                                                if (in_array(0, $view_permission_groups) || $customField->view_permission_groups=="") {
                                                                // have permission & continue
                                                                ?>
                                                                @if ($customField->in_listing)
                                                                    <?php
                                                                    if ($customField->$cf_title_var != "") {
                                                                        $cf_title = $customField->$cf_title_var;
                                                                    } else {
                                                                        $cf_title = $customField->$cf_title_var2;
                                                                    }


                                                                    $cf_saved_val = "";
                                                                    $cf_saved_val_array = array();
                                                                    if (count($Topic->fields) > 0) {
                                                                        foreach ($Topic->fields as $t_field) {
                                                                            if ($t_field->field_id == $customField->id) {
                                                                                if ($customField->type == 7) {
                                                                                    // if multi check
                                                                                    $cf_saved_val_array = explode(", ", $t_field->field_value);
                                                                                } else {
                                                                                    $cf_saved_val = $t_field->field_value;
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    ?>

                                                                    @if(($cf_saved_val!="" || count($cf_saved_val_array) > 0) && ($customField->lang_code == "all" || $customField->lang_code == @Helper::currentLanguage()->code))
                                                                        @if($customField->type ==12)
                                                                            {{--Vimeo Video Link--}}
                                                                        @elseif($customField->type ==11)
                                                                            {{--Youtube Video Link--}}
                                                                        @elseif($customField->type ==10)
                                                                            {{--Video File--}}
                                                                        @elseif($customField->type ==9)
                                                                            {{--Attach File--}}
                                                                        @elseif($customField->type ==8)
                                                                            {{--Photo File--}}
                                                                        @elseif($customField->type ==7)
                                                                            {{--Multi Check--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    <?php
                                                                                    $cf_details_var = "details_" . @Helper::currentLanguage()->code;
                                                                                    $cf_details_var2 = "details_en" . env('DEFAULT_LANGUAGE');
                                                                                    if ($customField->$cf_details_var != "") {
                                                                                        $cf_details = $customField->$cf_details_var;
                                                                                    } else {
                                                                                        $cf_details = $customField->$cf_details_var2;
                                                                                    }
                                                                                    $cf_details_lines = preg_split('/\r\n|[\r\n]/', $cf_details);
                                                                                    $line_num = 1;
                                                                                    ?>
                                                                                    @foreach ($cf_details_lines as $cf_details_line)
                                                                                        @if (in_array($line_num,$cf_saved_val_array))
                                                                                            <span class="badge">
                                                            {!! $cf_details_line !!}
                                                        </span>
                                                                                        @endif
                                                                                        <?php
                                                                                        $line_num++;
                                                                                        ?>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        @elseif($customField->type ==6)
                                                                            {{--Select--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    <?php
                                                                                    $cf_details_var = "details_" . @Helper::currentLanguage()->code;
                                                                                    $cf_details_var2 = "details_en" . env('DEFAULT_LANGUAGE');
                                                                                    if ($customField->$cf_details_var != "") {
                                                                                        $cf_details = $customField->$cf_details_var;
                                                                                    } else {
                                                                                        $cf_details = $customField->$cf_details_var2;
                                                                                    }
                                                                                    $cf_details_lines = preg_split('/\r\n|[\r\n]/', $cf_details);
                                                                                    $line_num = 1;
                                                                                    ?>
                                                                                    @foreach ($cf_details_lines as $cf_details_line)
                                                                                        @if ($line_num == $cf_saved_val)
                                                                                            {!! $cf_details_line !!}
                                                                                        @endif
                                                                                        <?php
                                                                                        $line_num++;
                                                                                        ?>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        @elseif($customField->type ==5)
                                                                            {{--Date & Time--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    {!! Helper::formatDate($cf_saved_val)." ".date("h:i A", strtotime($cf_saved_val)) !!}
                                                                                </div>
                                                                            </div>
                                                                        @elseif($customField->type ==4)
                                                                            {{--Date--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    {!! Helper::formatDate($cf_saved_val) !!}
                                                                                </div>
                                                                            </div>
                                                                        @elseif($customField->type ==3)
                                                                            {{--Email Address--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    {!! $cf_saved_val !!}
                                                                                </div>
                                                                            </div>
                                                                        @elseif($customField->type ==2)
                                                                            {{--Number--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    {!! $cf_saved_val !!}
                                                                                </div>
                                                                            </div>
                                                                        @elseif($customField->type ==1)
                                                                            {{--Text Area--}}
                                                                        @else
                                                                            {{--Text Box--}}
                                                                            <div class="row field-row">
                                                                                <div class="col-lg-3">
                                                                                    {!!  $cf_title !!} :
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    {!! $cf_saved_val !!}
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                                <?php
                                                                }
                                                                ?>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            {{--End of -- Additional Feilds--}}
                                            <p class="field-content">{!! mb_substr(strip_tags($Topic->$details),0, 300)."..." !!}</p>
                                        </div>
                                        <div class="views-field views-field-field-published-date">
                                            <div class="field-content"><i class="fa fa-calendar"></i> <time datetime="{!! Helper::formatDate($Topic->date)  !!}" class="datetime">{!! Helper::formatDate($Topic->date)  !!}</time>
                                            </div>
                                        </div>
                                    </article>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
      <div class="container">
        <div class="col-lg-8">
            {!! $Topics->appends(request()->input())->links() !!}
        </div>
        <div class="col-lg-4 text-right">
            <br>
            <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ __('backend.of') }}
                ( {{ $Topics->total()  }} ) {{ __('backend.records') }}</small>
        </div>
    </div>
    </div>
    @endif
  </div>
</article>
  </div>
    </div>
  </main>
@endsection

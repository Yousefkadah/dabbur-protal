@extends('frontEnd.layout')
@section('content')
<div class="standard-page">
    <div class="standard-page__header">
      <div class="header-banner__image">
        <picture>
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" media="(max-width: 768px)" type="image/jpeg">
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" type="image/jpeg">
          <img src="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" alt="" typeof="foaf:Image">
        </picture>
      </div>
      <div class="header-banner__container container">
        <div class="header-banner__text">
        <div class="block block-system block-system-breadcrumb-block">
            <div class="wrapper">
                <div class="block-body ">
                    <nav class="breadcrumb" role="navigation" aria-labelledby="system-breadcrumb">
                    <h2 id="system-breadcrumb" class="visually-hidden">Breadcrumb</h2>
                    <ol>
                        <li><a href="{{ route("Home") }}">{{__('backend.home')}}</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <h1 class="title">
            @if(@$WebmasterSection!="none")
                            <?php
                            $title_var = "title_" . @Helper::currentLanguage()->code;
                            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                            if (@$WebmasterSection->$title_var != "") {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var;
                            } else {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var2;
                            }
                            ?>
                            {!! $WebmasterSectionTitle !!}
                        @elseif(@$search_word!="")
                            {{ @$search_word }}
                        @else
                            {{ $User->name }}
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . @Helper::currentLanguage()->code;
                                ?>
                               - {{ $CurrentCategory->$category_title_var }}
                            @endif
                        @endif
        </h1>
        </div>
      </div>
    </div>
@if($Topics->total() == 0)
    <div class="alert alert-warning">
        <i class="fa fa-info"></i> &nbsp; {{ __('frontend.noData') }}
    </div>
@else
<div class="container">
    <div class="row">
        @if(count($Categories)>0)
            <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $category_title_var = "title_" . @Helper::currentLanguage()->code;
            $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
            $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
            ?>
            <div class="widget">
                <ul class="cat">
                    <li>
                        <a class="active" href="./">{{__('backend.all')}}</a></li>
                    @foreach($Categories as $Category)
                        <?php $active_cat = ""; ?>
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                @if($Category->id == $CurrentCategory->id)
                                    <?php $active_cat = "class=active"; ?>
                                @endif
                            @endif
                        @endif
                        <?php
                        if ($Category->$title_var != "") {
                            $Category_title = $Category->$title_var;
                        } else {
                            $Category_title = $Category->$title_var2;
                        }
                        $ccount = $category_and_topics_count[$Category->id];
                        ?>
                        <li>
                            @if($Category->icon !="")
                                <i class="fa {{$Category->icon}}"></i> &nbsp;
                            @endif
                            <a {{ $active_cat }} href="{{ Helper::categoryURL($Category->id) }}">{{ $Category_title }} <span>({{ $ccount }})</span></a></li>
                    @endforeach
                </ul>
            </div>

        @endif
    </div>
</div>
<section id="inner-headline">
<div class="component" style="direction: ltr">
    <ul class="align">
        <?php
        $title_var = "title_" . @Helper::currentLanguage()->code;
        $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
        $details_var = "details_" . @Helper::currentLanguage()->code;
        $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
        $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
        $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
        $i = 0;
        ?>
        @foreach($Topics as $Topic)
        <?php
        if ($Topic->$title_var != "") {
            $title = $Topic->$title_var;
        } else {
            $title = $Topic->$title_var2;
        }
        if ($Topic->$details_var != "") {
            $details = $details_var;
        } else {
            $details = $details_var2;
        }
        $section = "";
        try {
            if ($Topic->section->$title_var != "") {
                $section = $Topic->section->$title_var;
            } else {
                $section = $Topic->section->$title_var2;
            }
        } catch (Exception $e) {
            $section = "";
        }
        ?>
      <li>
        <figure class='book'>
          <!-- Front -->
          <ul class='hardcover_front'>
            <li>
              <img src="{{ URL::to('uploads/topics/'.$Topic->photo_file) }}" alt="{{$title}}" width="100%" height="100%">
            </li>
            <li></li>
          </ul>
          <!-- Pages -->
          <ul class='page'>
            <li></li>
            <li>
                @if($Topic->attach_file !="")
                <?php
                $file_ext = strrchr($Topic->attach_file, ".");
                $file_ext = strtolower($file_ext);
                ?>
                <a class="btn" href="{{ URL::to('uploads/topics/'.$Topic->attach_file) }}" target="_blank">{{__('frontend.download')}}</a>
                @endif
            </li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <!-- Back -->
          <ul class='hardcover_back'>
            <li></li>
            <li></li>
          </ul>
          <ul class='book_spine'>
            <li></li>
            <li></li>
          </ul>
          <figcaption>
            <h1>{{$title}}</h1>
            <span>{{$Topic->categories[0]->section->$title_var}}</span>
            <p>{!! mb_substr(strip_tags($Topic->$details),0, 300) !!}</p>
          </figcaption>
        </figure>
      </li>
      @endforeach
    </ul>
  </div>
</section>
<div class="container">
    <div class="col-lg-8">
        {!! $Topics->appends(request()->input())->links() !!}
    </div>
    <div class="col-lg-4 text-right">
        <br>
        <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ __('backend.of') }}
            ( {{ $Topics->total()  }} ) {{ __('backend.records') }}</small>
    </div>
</div>
@endif
</div>
<style>
    .widget .cat{
        list-style: none;
        padding: 0;
    }
    .widget .cat li{
        display: inline-block;
        padding: 0.6rem 1.5rem;
        position: relative;
        border: 1px solid black;
        border-radius: 50px;
        margin-inline-end: 0.3rem;
        transition: 0.5s;
    }
    @media only screen and (max-width:767px){
        .widget .cat li{
            padding: 5px 10px;
            font-size: 12px;
        }
    }
    .widget .cat li:hover{
        background-color: rgba(0, 51, 102, .75);
        border-color: #faa61a;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    }
    .widget .cat li:hover a,.widget .cat li:hover span{color:white;}
    .widget .cat li a{ color:black}
    .btn {
        display: inline-block;
        text-transform: uppercase;
        border: 2px solid #2c3e50;
        margin-top: 100px;
        font-size: 0.7em;
        font-weight: 700;
        padding: 0.1em 0.4em;
        text-align: center;
        -webkit-transition: color 0.3s, border-color 0.3s;
        -moz-transition: color 0.3s, border-color 0.3s;
        transition: color 0.3s, border-color 0.3s;
        }

        .btn:hover {
        border-color: #16a085;
        color: #16a085;
        }

        /* basic grid, only for this demo */
        .align {
        clear: both;
        margin: 90px auto 20px;
        width: 100%;
        max-width: 1170px;
        text-align: center;
        }

        .align > li {
        width: 500px;
        min-height: 300px;
        display: inline-block;
        margin: 30px 20px 30px 30px;
        padding: 0 0 0 60px;
        vertical-align: top;
        }
            .book {
        position: relative;
        width: 160px;
        height: 220px;
        -webkit-perspective: 1000px;
        -moz-perspective: 1000px;
        perspective: 1000px;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        transform-style: preserve-3d;
        }

        /*
            2. background & color
        */
        /* HARDCOVER FRONT */
        .hardcover_front li:first-child {
        background-color: #eee;
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        backface-visibility: hidden;
        }
        .hardcover_front img{
            height: 100%;
            object-fit: cover;
        }
        /* reverse */
        .hardcover_front li:last-child {
        background: #fffbec;
        }

        /* HARDCOVER BACK */
        .hardcover_back li:first-child {
        background: #fffbec;
        }

        /* reverse */
        .hardcover_back li:last-child {
        background: #fffbec;
        }

        .book_spine li:first-child {
        background: #eee;
        }

        .book_spine li:last-child {
        background: #333;
        }

        /* thickness of cover */
        .hardcover_front li:first-child:after,
        .hardcover_front li:first-child:before,
        .hardcover_front li:last-child:after,
        .hardcover_front li:last-child:before,
        .hardcover_back li:first-child:after,
        .hardcover_back li:first-child:before,
        .hardcover_back li:last-child:after,
        .hardcover_back li:last-child:before,
        .book_spine li:first-child:after,
        .book_spine li:first-child:before,
        .book_spine li:last-child:after,
        .book_spine li:last-child:before {
        background: #999;
        }

        /* page */
        .book .page > li {
        background: -webkit-linear-gradient(left, #e1ddd8 0%, #fffbf6 100%);
        background: -moz-linear-gradient(left, #e1ddd8 0%, #fffbf6 100%);
        background: -ms-linear-gradient(left, #e1ddd8 0%, #fffbf6 100%);
        background: linear-gradient(to right, #e1ddd8 0%, #fffbf6 100%);
        box-shadow: inset 0px -1px 2px rgba(50, 50, 50, 0.1), inset -1px 0px 1px rgba(150, 150, 150, 0.2);
        border-radius: 0px 5px 5px 0px;
        }

        /*
            3. opening cover, back cover and pages
        */
        .hardcover_front {
        -webkit-transform: rotateY(-34deg) translateZ(8px);
        -moz-transform: rotateY(-34deg) translateZ(8px);
        transform: rotateY(-34deg) translateZ(8px);
        z-index: 100;
        }

        .hardcover_back {
        -webkit-transform: rotateY(-15deg) translateZ(-8px);
        -moz-transform: rotateY(-15deg) translateZ(-8px);
        transform: rotateY(-15deg) translateZ(-8px);
        }

        .book .page li:nth-child(1) {
        -webkit-transform: rotateY(-28deg);
        -moz-transform: rotateY(-28deg);
        transform: rotateY(-28deg);
        }

        .book .page li:nth-child(2) {
        -webkit-transform: rotateY(-30deg);
        -moz-transform: rotateY(-30deg);
        transform: rotateY(-30deg);
        }

        .book .page li:nth-child(3) {
        -webkit-transform: rotateY(-32deg);
        -moz-transform: rotateY(-32deg);
        transform: rotateY(-32deg);
        }

        .book .page li:nth-child(4) {
        -webkit-transform: rotateY(-34deg);
        -moz-transform: rotateY(-34deg);
        transform: rotateY(-34deg);
        }

        .book .page li:nth-child(5) {
        -webkit-transform: rotateY(-36deg);
        -moz-transform: rotateY(-36deg);
        transform: rotateY(-36deg);
        }

        /*
            4. position, transform & transition
        */
        .hardcover_front,
        .hardcover_back,
        .book_spine,
        .hardcover_front li,
        .hardcover_back li,
        .book_spine li {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        transform-style: preserve-3d;
        }

        .hardcover_front,
        .hardcover_back {
        -webkit-transform-origin: 0% 100%;
        -moz-transform-origin: 0% 100%;
        transform-origin: 0% 100%;
        }

        .hardcover_front {
        -webkit-transition: all 0.8s ease, z-index 0.6s;
        -moz-transition: all 0.8s ease, z-index 0.6s;
        transition: all 0.8s ease, z-index 0.6s;
        }

        /* HARDCOVER front */
        .hardcover_front li:first-child {
        cursor: default;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        -webkit-transform: translateZ(2px);
        -moz-transform: translateZ(2px);
        transform: translateZ(2px);
        }

        .hardcover_front li:last-child {
        -webkit-transform: rotateY(180deg) translateZ(2px);
        -moz-transform: rotateY(180deg) translateZ(2px);
        transform: rotateY(180deg) translateZ(2px);
        }

        /* HARDCOVER back */
        .hardcover_back li:first-child {
        -webkit-transform: translateZ(2px);
        -moz-transform: translateZ(2px);
        transform: translateZ(2px);
        }

        .hardcover_back li:last-child {
        -webkit-transform: translateZ(-2px);
        -moz-transform: translateZ(-2px);
        transform: translateZ(-2px);
        }

        /* thickness of cover */
        .hardcover_front li:first-child:after,
        .hardcover_front li:first-child:before,
        .hardcover_front li:last-child:after,
        .hardcover_front li:last-child:before,
        .hardcover_back li:first-child:after,
        .hardcover_back li:first-child:before,
        .hardcover_back li:last-child:after,
        .hardcover_back li:last-child:before,
        .book_spine li:first-child:after,
        .book_spine li:first-child:before,
        .book_spine li:last-child:after,
        .book_spine li:last-child:before {
        position: absolute;
        top: 0;
        left: 0;
        }

        /* HARDCOVER front */
        .hardcover_front li:first-child:after,
        .hardcover_front li:first-child:before {
        width: 4px;
        height: 100%;
        }

        .hardcover_front li:first-child:after {
        -webkit-transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        -moz-transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        }

        .hardcover_front li:first-child:before {
        -webkit-transform: rotateY(90deg) translateZ(158px) translateX(2px);
        -moz-transform: rotateY(90deg) translateZ(158px) translateX(2px);
        transform: rotateY(90deg) translateZ(158px) translateX(2px);
        }

        .hardcover_front li:last-child:after,
        .hardcover_front li:last-child:before {
        width: 4px;
        height: 160px;
        }

        .hardcover_front li:last-child:after {
        -webkit-transform: rotateX(90deg) rotateZ(90deg) translateZ(80px) translateX(-2px) translateY(-78px);
        -moz-transform: rotateX(90deg) rotateZ(90deg) translateZ(80px) translateX(-2px) translateY(-78px);
        transform: rotateX(90deg) rotateZ(90deg) translateZ(80px) translateX(-2px) translateY(-78px);
        }

        .hardcover_front li:last-child:before {
        box-shadow: 0px 0px 30px 5px #333;
        -webkit-transform: rotateX(90deg) rotateZ(90deg) translateZ(-140px) translateX(-2px) translateY(-78px);
        -moz-transform: rotateX(90deg) rotateZ(90deg) translateZ(-140px) translateX(-2px) translateY(-78px);
        transform: rotateX(90deg) rotateZ(90deg) translateZ(-140px) translateX(-2px) translateY(-78px);
        }

        /* thickness of cover */
        .hardcover_back li:first-child:after,
        .hardcover_back li:first-child:before {
        width: 4px;
        height: 100%;
        }

        .hardcover_back li:first-child:after {
        -webkit-transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        -moz-transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        }

        .hardcover_back li:first-child:before {
        -webkit-transform: rotateY(90deg) translateZ(158px) translateX(2px);
        -moz-transform: rotateY(90deg) translateZ(158px) translateX(2px);
        transform: rotateY(90deg) translateZ(158px) translateX(2px);
        }

        .hardcover_back li:last-child:after,
        .hardcover_back li:last-child:before {
        width: 4px;
        height: 160px;
        }

        .hardcover_back li:last-child:after {
        -webkit-transform: rotateX(90deg) rotateZ(90deg) translateZ(80px) translateX(2px) translateY(-78px);
        -moz-transform: rotateX(90deg) rotateZ(90deg) translateZ(80px) translateX(2px) translateY(-78px);
        transform: rotateX(90deg) rotateZ(90deg) translateZ(80px) translateX(2px) translateY(-78px);
        }

        .hardcover_back li:last-child:before {
        box-shadow: 10px -1px 80px 20px #666;
        -webkit-transform: rotateX(90deg) rotateZ(90deg) translateZ(-140px) translateX(2px) translateY(-78px);
        -moz-transform: rotateX(90deg) rotateZ(90deg) translateZ(-140px) translateX(2px) translateY(-78px);
        transform: rotateX(90deg) rotateZ(90deg) translateZ(-140px) translateX(2px) translateY(-78px);
        }

        /* BOOK SPINE */
        .book_spine {
        -webkit-transform: rotateY(60deg) translateX(-5px) translateZ(-12px);
        -moz-transform: rotateY(60deg) translateX(-5px) translateZ(-12px);
        transform: rotateY(60deg) translateX(-5px) translateZ(-12px);
        width: 16px;
        z-index: 0;
        }

        .book_spine li:first-child {
        -webkit-transform: translateZ(2px);
        -moz-transform: translateZ(2px);
        transform: translateZ(2px);
        }

        .book_spine li:last-child {
        -webkit-transform: translateZ(-2px);
        -moz-transform: translateZ(-2px);
        transform: translateZ(-2px);
        }

        /* thickness of book spine */
        .book_spine li:first-child:after,
        .book_spine li:first-child:before {
        width: 4px;
        height: 100%;
        }

        .book_spine li:first-child:after {
        -webkit-transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        -moz-transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        transform: rotateY(90deg) translateZ(-2px) translateX(2px);
        }

        .book_spine li:first-child:before {
        -webkit-transform: rotateY(-90deg) translateZ(-12px);
        -moz-transform: rotateY(-90deg) translateZ(-12px);
        transform: rotateY(-90deg) translateZ(-12px);
        }

        .book_spine li:last-child:after,
        .book_spine li:last-child:before {
        width: 4px;
        height: 16px;
        }

        .book_spine li:last-child:after {
        -webkit-transform: rotateX(90deg) rotateZ(90deg) translateZ(8px) translateX(2px) translateY(-6px);
        -moz-transform: rotateX(90deg) rotateZ(90deg) translateZ(8px) translateX(2px) translateY(-6px);
        transform: rotateX(90deg) rotateZ(90deg) translateZ(8px) translateX(2px) translateY(-6px);
        }

        .book_spine li:last-child:before {
        box-shadow: 5px -1px 100px 40px rgba(0, 0, 0, 0.2);
        -webkit-transform: rotateX(90deg) rotateZ(90deg) translateZ(-210px) translateX(2px) translateY(-6px);
        -moz-transform: rotateX(90deg) rotateZ(90deg) translateZ(-210px) translateX(2px) translateY(-6px);
        transform: rotateX(90deg) rotateZ(90deg) translateZ(-210px) translateX(2px) translateY(-6px);
        }

        .book .page,
        .book .page > li {
        position: absolute;
        top: 0;
        left: 0;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        transform-style: preserve-3d;
        }

        .book .page {
        width: 100%;
        height: 98%;
        top: 1%;
        left: 3%;
        z-index: 10;
        }

        .book .page > li {
        width: 100%;
        height: 100%;
        -webkit-transform-origin: left center;
        -moz-transform-origin: left center;
        transform-origin: left center;
        -webkit-transition-property: transform;
        -moz-transition-property: transform;
        transition-property: transform;
        -webkit-transition-timing-function: ease;
        -moz-transition-timing-function: ease;
        transition-timing-function: ease;
        }

        .book .page > li:nth-child(1) {
        -webkit-transition-duration: 0.6s;
        -moz-transition-duration: 0.6s;
        transition-duration: 0.6s;
        }

        .book .page > li:nth-child(2) {
        -webkit-transition-duration: 0.6s;
        -moz-transition-duration: 0.6s;
        transition-duration: 0.6s;
        }

        .book .page > li:nth-child(3) {
        -webkit-transition-duration: 0.4s;
        -moz-transition-duration: 0.4s;
        transition-duration: 0.4s;
        }

        .book .page > li:nth-child(4) {
        -webkit-transition-duration: 0.5s;
        -moz-transition-duration: 0.5s;
        transition-duration: 0.5s;
        }

        .book .page > li:nth-child(5) {
        -webkit-transition-duration: 0.6s;
        -moz-transition-duration: 0.6s;
        transition-duration: 0.6s;
        }

        /*
            5. events
        */
        .book:hover > .hardcover_front {
        -webkit-transform: rotateY(-145deg) translateZ(0);
        -moz-transform: rotateY(-145deg) translateZ(0);
        transform: rotateY(-145deg) translateZ(0);
        z-index: 0;
        }

        .book:hover > .page li:nth-child(1) {
        -webkit-transform: rotateY(-30deg);
        -moz-transform: rotateY(-30deg);
        transform: rotateY(-30deg);
        -webkit-transition-duration: 1.5s;
        -moz-transition-duration: 1.5s;
        transition-duration: 1.5s;
        }

        .book:hover > .page li:nth-child(2) {
        -webkit-transform: rotateY(-35deg);
        -moz-transform: rotateY(-35deg);
        transform: rotateY(-35deg);
        -webkit-transition-duration: 1.8s;
        -moz-transition-duration: 1.8s;
        transition-duration: 1.8s;
        }

        .book:hover > .page li:nth-child(3) {
        -webkit-transform: rotateY(-118deg);
        -moz-transform: rotateY(-118deg);
        transform: rotateY(-118deg);
        -webkit-transition-duration: 1.6s;
        -moz-transition-duration: 1.6s;
        transition-duration: 1.6s;
        }

        .book:hover > .page li:nth-child(4) {
        -webkit-transform: rotateY(-130deg);
        -moz-transform: rotateY(-130deg);
        transform: rotateY(-130deg);
        -webkit-transition-duration: 1.4s;
        -moz-transition-duration: 1.4s;
        transition-duration: 1.4s;
        }

        .book:hover > .page li:nth-child(5) {
        -webkit-transform: rotateY(-140deg);
        -moz-transform: rotateY(-140deg);
        transform: rotateY(-140deg);
        -webkit-transition-duration: 1.2s;
        -moz-transition-duration: 1.2s;
        transition-duration: 1.2s;
        }

        /*
            6. Bonus
        */
        /* cover CSS */
        .coverDesign {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        overflow: hidden;
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        backface-visibility: hidden;
        }

        .coverDesign::after {
        background-image: -webkit-linear-gradient(-135deg, rgba(255, 255, 255, 0.45) 0%, transparent 100%);
        background-image: -moz-linear-gradient(-135deg, rgba(255, 255, 255, 0.45) 0%, transparent 100%);
        background-image: linear-gradient(-135deg, rgba(255, 255, 255, 0.45) 0%, rgba(0, 0, 0, 0) 100%);
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        }

        .coverDesign h1 {
        color: #fff;
        font-size: 2.2em;
        letter-spacing: 0.05em;
        text-align: center;
        margin: 54% 0 0 0;
        text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.1);
        }

        .coverDesign p {
        color: #f8f8f8;
        font-size: 1em;
        text-align: center;
        text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.1);
        }

        .yellow {
        background-color: #f1c40f;
        background-image: -webkit-linear-gradient(top, #f1c40f 58%, #e7ba07 0%);
        background-image: -moz-linear-gradient(top, #f1c40f 58%, #e7ba07 0%);
        background-image: linear-gradient(to bottom, #f1c40f 58%, #e7ba07 0%);
        }

        .blue {
        background-color: #3498db;
        background-image: -webkit-linear-gradient(top, #3498db 58%, #2a90d4 0%);
        background-image: -moz-linear-gradient(top, #3498db 58%, #2a90d4 0%);
        background-image: linear-gradient(to bottom, #3498db 58%, #2a90d4 0%);
        }

        .grey {
        background-color: #f8e9d1;
        background-image: -webkit-linear-gradient(top, #f8e9d1 58%, #e7d5b7 0%);
        background-image: -moz-linear-gradient(top, #f8e9d1 58%, #e7d5b7 0%);
        background-image: linear-gradient(to bottom, #f8e9d1 58%, #e7d5b7 0%);
        }

        /* figcaption */
        figcaption {
        padding-left: 40px;
        text-align: center;
        position: absolute;
        top: 0%;
        left: 160px;
        width: 310px;
        }

        figcaption h1 {
        margin: 0;
        }

        figcaption span {
        color: #16a085;
        padding: 0.6em 0 1em 0;
        display: block;
        }

        figcaption p {
        color: #63707d;
        line-height: 1.3;
        }

        /* Media Queries */
        @media screen and (max-width: 37.8125em) {
        .align > li {
            width: 100%;
            min-height: 440px;
            height: auto;
            padding: 0;
            margin: 0 0 30px 0;
        }

        .book {
            margin: 0 auto;
        }

        figcaption {
            text-align: center;
            width: 320px;
            top: 250px;
            padding-left: 0;
            left: -80px;
            font-size: 90%;
        }
        }
</style>
@endsection

@section('footerInclude')
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
@endsection

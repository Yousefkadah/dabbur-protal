@extends('frontEnd.layout')
@section('content')
<div class="standard-page">
    <div class="standard-page__header">
      <div class="header-banner__image">
        <picture>
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" media="(max-width: 768px)" type="image/jpeg">
          <source srcset="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" type="image/jpeg">
          <img src="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" alt="" typeof="foaf:Image">
        </picture>
      </div>
      <div class="header-banner__container container">
        <div class="header-banner__text">
        <div class="block block-system block-system-breadcrumb-block">
            <div class="wrapper">
                <div class="block-body ">
                    <nav class="breadcrumb" role="navigation" aria-labelledby="system-breadcrumb">
                    <h2 id="system-breadcrumb" class="visually-hidden">Breadcrumb</h2>
                    <ol>
                        <li><a href="{{ route("Home") }}">{{__('backend.home')}}</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <h1 class="title">
            @if(@$WebmasterSection!="none")
                            <?php
                            $title_var = "title_" . @Helper::currentLanguage()->code;
                            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                            if (@$WebmasterSection->$title_var != "") {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var;
                            } else {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var2;
                            }
                            ?>
                            {!! $WebmasterSectionTitle !!}
                        @elseif(@$search_word!="")
                            {{ @$search_word }}
                        @else
                            {{ $User->name }}
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . @Helper::currentLanguage()->code;
                                ?>
                               - {{ $CurrentCategory->$category_title_var }}
                            @endif
                        @endif
        </h1>
        </div>
      </div>
    </div>
@if($Topics->total() == 0)
    <div class="alert alert-warning">
        <i class="fa fa-info"></i> &nbsp; {{ __('frontend.noData') }}
    </div>
@else
<div class="container">
    <div class="row">
        @if(count($Categories)>0)
            <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $category_title_var = "title_" . @Helper::currentLanguage()->code;
            $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
            $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
            ?>
            <div class="widget">
                <ul class="cat">
                    @foreach($Categories as $Category)
                        <?php $active_cat = ""; ?>
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                @if($Category->id == $CurrentCategory->id)
                                    <?php $active_cat = "class=active"; ?>
                                @endif
                            @endif
                        @endif
                        <?php
                        if ($Category->$title_var != "") {
                            $Category_title = $Category->$title_var;
                        } else {
                            $Category_title = $Category->$title_var2;
                        }
                        $ccount = $category_and_topics_count[$Category->id];
                        ?>
                        <li>
                            @if($Category->icon !="")
                                <i class="fa {{$Category->icon}}"></i> &nbsp;
                            @endif
                            <a {{ $active_cat }} href="{{ Helper::categoryURL($Category->id) }}">{{ $Category_title }} <span>({{ $ccount }})</span></a></li>
                    @endforeach
                </ul>
            </div>

        @endif
    </div>
</div>
<section id="inner-headline">
    <div class="container">
        <div class="row justify-content-center">
        <?php
        $title_var = "title_" . @Helper::currentLanguage()->code;
        $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
        $details_var = "details_" . @Helper::currentLanguage()->code;
        $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
        $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
        $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
        $i = 0;
        ?>
        @foreach($Topics as $Topic)
        <?php
        if ($Topic->$title_var != "") {
            $title = $Topic->$title_var;
        } else {
            $title = $Topic->$title_var2;
        }
        if ($Topic->$details_var != "") {
            $details = $details_var;
        } else {
            $details = $details_var2;
        }
        $section = "";
        try {
            if ($Topic->section->$title_var != "") {
                $section = $Topic->section->$title_var;
            } else {
                $section = $Topic->section->$title_var2;
            }
        } catch (Exception $e) {
            $section = "";
        }
        $topic_link_url = Helper::topicURL($Topic->id);
        ?>
      <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card" style='background: url(@if($Topic->photo_file =="")"https://image.freepik.com/free-vector/colorful-gradient-liquid-shapes-background_23-2148250143.jpg"@else{{ URL::to('uploads/topics/'.$Topic->photo_file) }}@endif),	#ffffff;'>
            <div class="overlay"></div>
            <div class="content">
                <h3>{{$title}}</h3>
                <h6>{{$Topic->categories[0]->section->$title_var}}</h6>
            </div>
            <a href="{{ $topic_link_url }}">
            <div class="fav">
                <i class="fa fa-link"></i>
            </div>
            </a>
        </div>
    </div>
      @endforeach
    </div>
  </div>
</div>
</section>
<div class="container">
    <div class="col-lg-8">
        {!! $Topics->appends(request()->input())->links() !!}
    </div>
    <div class="col-lg-4 text-right">
        <br>
        <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ __('backend.of') }}
            ( {{ $Topics->total()  }} ) {{ __('backend.records') }}</small>
    </div>
</div>
@endif
</div>
<style>
    .container{
        float: unset;
    }
    .widget .cat{
        list-style: none;
        padding: 0;
    }
    .widget .cat li{
        display: inline-block;
        padding: 0.6rem 1.5rem;
        position: relative;
        border: 1px solid black;
        border-radius: 50px;
        margin-inline-end: 0.3rem;
        transition: 0.5s;
    }
    @media only screen and (max-width:767px){
        .widget .cat li{
            padding: 5px 10px;
            font-size: 12px;
        }
    }
    .widget .cat li:hover{
        background-color: rgba(0, 51, 102, .75);
        border-color: #faa61a;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    }
    .widget .cat li:hover a,.widget .cat li:hover span{color:white;}
    .widget .cat li a{ color:black}
    .btn {
        display: inline-block;
        text-transform: uppercase;
        border: 2px solid #2c3e50;
        margin-top: 100px;
        font-size: 0.7em;
        font-weight: 700;
        padding: 0.1em 0.4em;
        text-align: center;
        -webkit-transition: color 0.3s, border-color 0.3s;
        -moz-transition: color 0.3s, border-color 0.3s;
        transition: color 0.3s, border-color 0.3s;
        }

    .btn:hover {
    border-color: #16a085;
    color: #16a085;
    }
    .card {
	position: relative;
	height: 170px;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	border-radius: 25px;
	margin: 10px;
	transition: 0.25s ease-in;
}

.card .overlay {
	position: absolute;
	top: 0;
	bottom: 0;
	width: 100%;
	height: 170px;
	background: linear-gradient(
		120.55deg,
		rgba(39, 39, 39, 0.38) 0%,
		rgba(39, 39, 39, 0.2394) 100%
	);
	border-radius: 25px;
}

.card .content {
	position: absolute;
	bottom: 10px;
	left: 25px;
}

.card .content h3 {
	font-weight: bold;
	font-size: 18px;
	color: #f2f2f2;
	margin-bottom: 10px;
	margin-top: 0;
}

.card .content h6 {
	font-weight: 500;
	font-size: 14px;
	color: #f2f2f2;
	margin-bottom: 10px;
	margin-top: 0;
}

.card .fav {
	position: absolute;
	top: 25px;
	right: 25px;
	cursor: pointer;
}

.card .fav i {
	color: white;
}

.card:hover {
	transform: scale(1.05);
}

.card:hover {
	background-position: top;
}
    </style>
@endsection

@section('Meta')
<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "ItemList",
    "itemListElement": [
        <?php
        $title_var = "title_" . @Helper::currentLanguage()->code;
        $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
        $details_var = "details_" . @Helper::currentLanguage()->code;
        $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
        $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
        $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
        $i = 0;
        ?>
        @foreach($Topics as $Topic)
            <?php
            if ($Topic->$title_var != "") {
                $title = $Topic->$title_var;
            } else {
                $title = $Topic->$title_var2;
            }
            if ($Topic->$details_var != "") {
                $details = $details_var;
            } else {
                $details = $details_var2;
            }
            $section = "";
            try {
                if ($Topic->section->$title_var != "") {
                    $section = $Topic->section->$title_var;
                } else {
                    $section = $Topic->section->$title_var2;
                }
            } catch (Exception $e) {
                $section = "";
            }
            $topic_link_url = Helper::topicURL($Topic->id);
            ?>
            {
            "@type": "ListItem",
            "position": "{{$loop->iteration }}",
            "item": {
                "@type": "Course",
                "name": "{{ $title }}",
                "url": "{{$topic_link_url}}",
                "description": "{!! mb_substr(strip_tags($Topic->$details),0, 300)."..." !!}",
                "provider": {
                    "@type": "Organization",
                    "name": "IziEng - the international studying center",
                    "sameAs": "https://izieng.com"
                    }
                }
            }
            @if (!$loop->last)
            ,
            @endif
            @endforeach
        ]
    }
    </script>
@endsection

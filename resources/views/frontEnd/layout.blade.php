<!DOCTYPE html>
<html lang="{{ @Helper::currentLanguage()->code }}" dir="{{ @Helper::currentLanguage()->direction }}">
<head>
    @include('frontEnd.includes.head')
</head>
<?php
$currentRoute= Route::currentRouteName();
if($currentRoute=='Home' || $currentRoute=='HomePage')
    $bdy_class = "ust30a-time-tunnel path-frontpage page-node-type-ust30a-time-tunnel hide-header-footer";
else
    $bdy_class = "press-release-listing path-node page-node-type-standard-page eu-cookie-compliance-status-2";
$bdy_bg_color = "";
$bdy_bg_image = "";
if (Helper::GeneralSiteSettings("style_type")) {
    $bdy_class .= "boxed-layout";
    if (Helper::GeneralSiteSettings("style_bg_type") == 0) {
        $bdy_bg_color = "background-color: #000;";
        if (Helper::GeneralSiteSettings("style_bg_color") != "") {
            $bdy_bg_color = "background-attachment: fixed;background-color: " . Helper::GeneralSiteSettings("style_bg_color") . ";";
        }
        $bdy_bg_image = "";
    } elseif (Helper::GeneralSiteSettings("style_bg_type") == 1) {
        $bdy_bg_color = "";
        $bdy_bg_image = "background-attachment: fixed;background-image:url(" . URL::to('uploads/pattern/' . Helper::GeneralSiteSettings("style_bg_pattern")) . ")";
    } elseif (Helper::GeneralSiteSettings("style_bg_type") == 2) {
        $bdy_bg_color = "";
        $bdy_bg_image = "background-attachment: fixed;background-image:url(" . URL::to('uploads/settings/' . Helper::GeneralSiteSettings("style_bg_image")) . ")";
    }
}
?>


<body class="js {!!  $bdy_class !!}" style=" {!!  $bdy_bg_color !!} {!! $bdy_bg_image !!}" data-barba="wrapper">
    <div class="loading-container">
        <div class="loading-screen">
            <img class="logo-svg" src="./assets/Front/media/logo.png" style="filter:brightness(50);"/>
            <!-- <svg class="logo-svg" width="168px" height="50px" viewBox="0 0 168 50" xmlns="http://www.w3.org/2000/svg">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="logo-svg" transform="translate(-57.000000, -45.000000)" fill="#FFFFFF">
                        <g transform="translate(57.000000, 45.000000)">
                            <path id="svg-path-c" d="M-1.78419582e-05,20.2642321 C-1.78419582e-05,14.6829821 4.29240046,10.643875 10.0521414,10.643875 C13.6108984,10.643875 16.068985,12.112625 17.7199014,13.875125 L17.7199014,14.3890536 L13.7944921,18.0610179 L13.2808222,18.0610179 C12.6203129,17.2533393 11.5929729,16.7024464 10.3823961,16.7024464 C8.43797946,16.7024464 7.04398726,18.2079821 7.04398726,20.2642321 C7.04398726,22.3203036 8.43797946,23.8258393 10.3823961,23.8258393 C11.5929729,23.8258393 12.6203129,23.275125 13.2808222,22.4672679 L13.7944921,22.4672679 L17.7199014,26.138875 L17.7199014,26.6531607 C16.068985,28.4154821 13.6108984,29.8844107 10.0521414,29.8844107 C4.29240046,29.8844107 -1.78419582e-05,25.8453036 -1.78419582e-05,20.2642321"></path>
                            <polygon id="svg-path-play" points="1.39666849 31.9897857 1.39666849 49.29925 2.56460307 49.9740714 17.5423917 41.31925 17.5423917 39.9697857 2.56460307 31.3153214"></polygon>
                            <path id="svg-path-i" d="M20.2538341,29.44375 L27.443608,29.44375 L27.443608,11.0873214 L20.2538341,11.0873214 L20.2538341,29.44375 Z M19.6671905,4.18517857 C19.6671905,1.8725 21.5379199,0 23.8488103,0 C26.1598791,0 28.03043,1.8725 28.03043,4.18517857 C28.03043,6.49803571 26.1598791,8.37053571 23.8488103,8.37053571 C21.5379199,8.37053571 19.6671905,6.49803571 19.6671905,4.18517857 L19.6671905,4.18517857 Z"></path>
                            <path id="svg-path-n" d="M50.626271,18.7235357 L50.626271,29.4437143 L43.4368539,29.4437143 L43.4368539,19.2374643 C43.4368539,17.8056786 42.6298621,16.70425 41.0157002,16.70425 C39.6218864,16.70425 38.6682337,17.6955 38.6682337,19.2374643 L38.6682337,29.4437143 L31.4784598,29.4437143 L31.4784598,11.0872857 L37.347572,11.0872857 L37.347572,13.3269286 C38.5949033,11.748 40.4286997,10.6465714 42.9599384,10.6465714 C47.4718127,10.6465714 50.626271,13.9508571 50.626271,18.7235357"></path>
                            <path id="svg-path-e" d="M60.2731323,18.0627857 L66.1788205,18.0627857 C66.0685572,17.3285 65.7388378,16.7042143 65.3350743,16.3004643 C64.7848283,15.7499286 64.0511669,15.456 63.2441752,15.456 C61.5568612,15.456 60.5296996,16.4840357 60.2731323,18.0627857 L60.2731323,18.0627857 Z M60.2731323,21.7706429 C60.5296996,23.8267143 62.0703527,24.8177857 64.1614302,24.8177857 C65.9220747,24.8177857 67.4627278,24.2306429 68.5630413,23.1658214 L69.0765329,23.1658214 L72.0845086,26.13975 L72.0845086,26.6536786 C69.9202791,29.0765357 66.5822271,29.8842143 63.7210907,29.8842143 C57.5586568,29.8842143 53.3768586,25.8458214 53.3768586,20.2654643 C53.3768586,14.6849286 57.4487503,10.6467143 63.4643449,10.6467143 C66.2889053,10.6467143 68.7097022,11.6011786 70.4707035,13.2901071 C72.1212631,14.9420714 73.1114917,17.2549286 73.1114917,20.1186786 C73.1114917,20.6693929 73.0749157,21.2201071 72.9650093,21.7706429 L60.2731323,21.7706429 Z"></path>
                            <path id="svg-path-m" d="M106.711521,18.7235357 L106.711521,29.4437143 L99.5215686,29.4437143 L99.5215686,19.2374643 C99.5215686,17.6955 98.6780009,16.70425 97.2474327,16.70425 C95.8537973,16.70425 94.9732967,17.6955 94.9732967,19.2374643 L94.9732967,29.4437143 L87.7835228,29.4437143 L87.7835228,19.2374643 C87.7835228,17.6955 86.9401334,16.70425 85.5093868,16.70425 C84.1153946,16.70425 83.2354292,17.6955 83.2354292,19.2374643 L83.2354292,29.4437143 L76.0456553,29.4437143 L76.0456553,11.0872857 L81.914589,11.0872857 L81.914589,13.3269286 C83.1617419,11.748 84.9957168,10.6465714 87.5269554,10.6465714 C89.8376674,10.6465714 91.7083967,11.4544286 93.0292369,12.8862143 C94.4596267,11.5644286 96.4404409,10.6465714 98.898349,10.6465714 C103.446621,10.6465714 106.711521,13.7672857 106.711521,18.7235357"></path>
                            <path id="svg-path-a" d="M121.16299,23.2758929 L121.16299,22.3948214 C120.539413,22.10125 119.4391,21.7341071 118.375184,21.7341071 C116.871285,21.7341071 115.880878,22.5417857 115.880878,23.7166071 C115.880878,24.89125 116.871285,25.6623214 118.375184,25.6623214 C120.136007,25.6623214 121.16299,24.3407143 121.16299,23.2758929 M127.619281,18.54 L127.619281,29.44375 L122.006736,29.44375 L122.006736,27.975 C120.502837,29.15 118.595353,29.8842857 116.4677,29.8842857 C112.102665,29.8842857 109.204774,27.57125 109.204774,24.1203571 C109.204774,20.3389286 112.359411,17.8057143 117.128031,17.8057143 C118.448514,17.8057143 119.769176,18.0994643 121.016329,18.5033929 L121.016329,17.9894643 C121.016329,16.5942857 119.29226,15.6764286 117.311625,15.6764286 C115.954208,15.6764286 114.486886,16.1903571 113.459903,16.9248214 L112.946233,16.9248214 L110.782182,13.4369643 L110.782182,12.9230357 C112.909478,11.3076786 116.174378,10.6467857 118.48509,10.6467857 C122.923634,10.6467857 127.619281,12.4091071 127.619281,18.54"></path>
                            <path id="svg-path-t" d="M145.812779,27.7551607 L145.592788,28.3056964 C144.198617,29.3337321 142.328066,29.8842679 140.457337,29.8842679 C136.679124,29.8842679 132.864156,27.7917679 132.864156,22.468375 L132.864156,16.227125 L129.636189,16.227125 L129.636189,11.0874821 L132.864156,11.0874821 L132.864156,5.76408929 L139.430354,5.06641071 L139.9806,5.39694643 L139.9806,11.0874821 L145.189381,11.0874821 L145.189381,16.227125 L139.9806,16.227125 L139.9806,21.6606964 C139.9806,23.3492679 140.823989,23.8265893 141.814396,23.8265893 C142.328066,23.8265893 142.914888,23.6064107 143.42838,23.2392679 L143.94205,23.4964107 L145.812779,27.7551607 Z"></path>
                            <path id="svg-path-o" d="M160.558515,20.2655357 C160.558515,18.2094643 159.164701,16.7042857 157.257218,16.7042857 C155.349912,16.7042857 153.955742,18.2094643 153.955742,20.2655357 C153.955742,22.3214286 155.349912,23.8267857 157.257218,23.8267857 C159.164701,23.8267857 160.558515,22.3214286 160.558515,20.2655357 M146.913164,20.2655357 C146.913164,14.685 151.241266,10.6466071 157.257218,10.6466071 C163.272812,10.6466071 167.601628,14.685 167.601628,20.2655357 C167.601628,25.8458929 163.272812,29.8842857 157.257218,29.8842857 C151.241266,29.8842857 146.913164,25.8458929 146.913164,20.2655357"></path>
                        </g>
                    </g>
                </g>
            </svg> -->
            <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                        <polygon points="260 0 320 0 260 140"></polygon>
                    </g>
                </g>
            </svg>
            <svg class="rec-shape-svg before" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                        <polygon points="260 0 320 0 260 140"></polygon>
                    </g>
                </g>
            </svg>
        </div>
    </div>
    <div id="gdpr-box" class="cookies-accept">
        <div class="gdpr-text">
            <h4>Cookies! 🍪</h4>
            <span>Wij gebruiken <a href="cookie-beleid">cookies</a> om onze website te verbeteren en analyseren.</span>
        </div>
        <div class="gdpr-button">
            <div class="btn gdpr-button-accept">Ok!</div>
        </div>
    </div>

    <main data-barba="container" data-barba-namespace="@yield('namespace')" class="dark" id="@yield('namespace_id')">
        <!-- start header -->
        @include('frontEnd.includes.header')
        <!-- end header -->
        @yield('content')
        @include('frontEnd.includes.footer')
        @include('frontEnd.includes.foot')
        @yield('footerInclude')

    </main>
@if(Helper::GeneralSiteSettings("style_preload"))
    <div id="preloader"></div>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(window).load(function () {
                $('#preloader').fadeOut('slow', function () {
                    // $(this).remove();
                });
            });
        });
    </script>
@endif
<script type="text/javascript">

    var coll = document.getElementsByClassName("collapsible");
    var i;
    for (i = 0; i < coll.length; i++){
        coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
            content.style.maxHeight = null;
            content.style.visibility = "hidden";
            content.style.margin = "0 2%";
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
            content.style.visibility = "visible";
            content.style.margin = "12px 2%";
        }
    });
    }

    </script>
<script type="text/javascript">
    const t = [`%c Made with \u2665 by Y2 Information System LTD %c %c %c http://www.y2erp.com/ %c %c `, "color: #fff; background: #e43333; padding:5px 0;", "background: #131419; padding:5px 0;", "background: #131419; padding:5px 0;", "color: #fff; background: #1c1c1c; padding:5px 0;", "background: #fff; padding:5px 0;", "color: #e43333; background: #fff; padding:5px 0;"];
        window.console.log.apply(console, t)
</script>
</body>
</html>

@extends('frontEnd.layout')
@section('namespace'){{'home'}}@endsection
@section('namespace_id'){{'home'}}@endsection
@section('content')
<div class="section default-header full-height">
    <div class="header-title-wrap">
       <div class="header-title-wrap-block">
          <div class="wrap animate-fade-in">
             <h1 class="home-title"><div class="home-white-back"></div>דבור <br> תכנון וביצוע</h1>
             <h2 style="color:white">אינסטלציה, ספרינקלרים, כיבוי אש</h2>
             <a href="#" class="btn orange">קביעת פגישה</a>
             <svg class="play-shape-header" width="374" height="432"><path stroke="#FFF" stroke-width="1px" d="M1 16.543v398.905L27.91 431 373 231.545v-31.099L27.91 1z" fill="none" fill-rule="evenodd"/></svg>
          </div>
          <h3 class="left-text"><span>דבור תכנון,ביצוע</span></h3>
       </div>
    </div>
    <div class="rec-shape">
       <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
             <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                <polygon points="260 0 320 0 260 140"></polygon>
             </g>
          </g>
       </svg>
    </div>
    <div class="scroll-indicator">
     <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"/></svg>
     <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"/></svg>
     <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"/></svg>
     <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"/></svg>
 </div></div>
 <div class="video-modal">
    <div style="padding:56.25% 0 0 0; position:relative;">
       <iframe id="vimeo-modal" src="https://player.vimeo.com/video/395731181?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
 </div>
 <div class="video-modal-bg">
    <div class="close-btn">
       <svg width="16" height="15"><path d="M14.718.075l.707.707L8.707 7.5l6.718 6.718-.707.707L8 8.207l-6.718 6.718-.707-.707L7.293 7.5.575.782l.707-.707L8 6.793 14.718.075z" fill="#171C21" fill-rule="evenodd"/></svg>
    </div>
 </div>
 <div class="home-header-bg video">
       <div id="video" class="video-wrapper">
       <iframe id="vimeo-iframe" src="https://player.vimeo.com/video/465303259?background=1&api=1&autoplay=1&loop=1&title=0&byline=0&portrait=0&muted=1&player_id=vvimeoVid" width="1920" height="1080" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
    <div class="dark-overlay"></div>
    <div class="video-thumbnail" style="background: url('./assets/Front/media/GalleryFlash_126.jpg') center center; background-size: cover;"></div>
 </div>
 <div class="section home-intro">
    <svg width="4000px" height="2000px" viewBox="0 0 4000 2000" xmlns="http://www.w3.org/2000/svg">
       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <path d="M4000,0 L4000,2000 L0,2000 L0,0 L4000,0 Z M2152.9094,788 L2126,803.543359 L2126,1202.44841 L2152.9094,1218 L2498,1018.54542 L2498,987.446353 L2152.9094,788 Z" fill="#FFFFFF"></path>
       </g>
    </svg>
    <div class="wrap">
       <div class="sub-wrap textblock">
        @if(!empty($HomePage))
            @if(@$HomePage->{"details_" . @Helper::currentLanguage()->code} !="")
            {!! @$HomePage->{"details_" . @Helper::currentLanguage()->code} !!}
            {{-- <h2>דבור תכנון , ביצוע , פיקוח וחישוב כמויות</h2>
            <p>דבור הינו עסק משפחתי המבצע  מגוון רחב של פרויקטים בתחום תברואה, כיבוי אש, הסקה, תחנות שאיבה והסקה. העסק  נוסד בשנת 1988 וממשיך דור שלישי בענף האינסטלציה ומערכות נוזלים (מים, כיבוי אש, סניטציה, תשתית, ניקוז).</p> --}}
            @endif
        @endif
        </div>
    </div>
 </div>
 @if(count($HomeTopics)>0)
 <div class="section projects-grid recent">
    <div class="wrap-text">
       <h3>גלריית פרויקטים</h3>
    </div>
    <div class="wrap-wide">
       <ul>
        <?php
        $title_var = "title_" . @Helper::currentLanguage()->code;
        $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
        $details_var = "details_" . @Helper::currentLanguage()->code;
        $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
        $section_url = "";
        ?>
        @foreach($HomeTopics as $HomeTopic)
            <?php
                if ($HomeTopic->$title_var != "") {
                    $title = $HomeTopic->$title_var;
                } else {
                    $title = $HomeTopic->$title_var2;
                }
                if ($HomeTopic->$details_var != "") {
                    $details = $details_var;
                } else {
                    $details = $details_var2;
                }
                if ($section_url == "") {
                    $section_url = Helper::sectionURL($HomeTopic->webmaster_id);
                }
            ?>
            <li style="background: url('{{ URL::to('uploads/topics/'.$HomeTopic->photo_file) }}') center center; background-size: cover;">
             <a href="{{ Helper::topicURL($HomeTopic->id) }}">
                <div class="project-textblock">
                   <h3 style="color: #FF4D0F;">{{$HomeTopic->categories[0]->section->$title_var}}</h3>
                   <h2>{{ $title }}</h2>
                </div>
                <div class="project-shape">
                   <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                         <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                            <polygon points="260 0 320 0 260 140"></polygon>
                         </g>
                      </g>
                   </svg>
                   <div class="circle-text small">
                   <svg width="80" height="80" viewBox="0 0 80 80">
                      <path id="textpath-2" d="M 40, 40 m -40, 0 a 40,40 0 1,0 80,0 a 40,40 0 1,0 -80,0 " />
                      <text fill="#FFF">
                         <textPath xlink:href="#textpath-2" startOffset="50%" text-anchor="middle">הצג פרויקט</textPath>
                      </text>
                   </svg>
                </div>
                </div>
                <span>{{ mb_substr($title,0,1) }}</span>
                <div class="dark-overlay"></div>
             </a>
            </li>
        @endforeach
             </div>
    <div class="wrap-text">
       <a href="{{ url($section_url) }}" class="btn orange">עוד פרויקטים</a>
    </div>
 </div>
 @endif
 @if(count($HomePhotos)>0)
 <div class="section services-grid">
    <div class="wrap-wide">
       <h3>שירותי החברה</h3>
       <ul>
        <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $details_var = "details_" . @Helper::currentLanguage()->code;
            $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
            $section_url = "";
            $ph_count = 0;
            ?>
            @foreach($HomePhotos as $HomePhoto)
                <?php
                if ($HomePhoto->$title_var != ""){
                    $title = $HomePhoto->$title_var;
                }else{
                    $title = $HomePhoto->$title_var2;
                }

                if($section_url == ""){
                    $section_url = Helper::sectionURL($HomePhoto->webmaster_id);
                }
            ?>
          <li>
             <a href="{{Helper::topicURL($HomePhoto->id)}}" style="background: url('{{ URL::to('uploads/topics/'.$HomePhoto->photo_file) }}') center center; background-size: cover;">
                <div class="dark-overlay"></div>
                <div class="services-shape">
                   <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                         <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                            <polygon points="260 0 320 0 260 140"></polygon>
                         </g>
                      </g>
                   </svg>
                </div>
                <div class="block">
                   <h2>{{ $title }}</h2>
                </div>
             </a>
          </li>
          @endforeach
    </div>
    <div class="wrap-text" style="text-align: center;">
        <a href="{{ url($section_url) }}" class="btn orange">לכל שירותי החברה</a>
     </div>
 </div>
 @endif
 <div class="section textimage">
    <div class="wrap">
       <div class="sub-wrap">
          <div class="textimage-textblock textblock">
             <h2>אודות החברה</h2>
             <p>הצוות שלנו מיומן ומבטיח מקצועיות וביצוע העבודות בסטנדרטים גבוהים ביותר של איכות ביצוע, איכות שרות ונאמנות ללקוחות החברה.
                 החברה מעסיקה 20 עובדים בין מהנדסים ,מנהלי עבודה בעלי מקצוע המבצעים את כל מגוון העבודות והשרות. בבעלות החברה רכבים, כלי חפירה, רתכות, משאית ,מלגזה טלסקופי ומלגזות, העומדים לרשות עובדי החברה ולמען ביצוע   הפרויקטים על הצד הטוב ביותר.
                 אנשי המקצוע של החברה יידעו לתת מענה לכל דרישה ואת הפתרון החברה יכולה לקבוע וליישם בכל מצב בשטח הלקוח על ידי הזמנת ביצוע העבודה ע"י צוותי השטח של החברה.
                 במהלך השנים החברה התמודדה עם אתגרים רבים של מכון התקנים הישראלי והציעה פתרונות מגובים לתקינה הישראלית ויצירתית במקביל. נציגי החברה מכינים מפרטים לביצוע העבודות  והגשת פתרונות הנחוצים לביצוע.</p>
             <a class="btn" href="#">קרא עוד</a>
                      </div>
       </div>
       <div class="sub-wrap false">
          <div class="image square">
             <div class="image-container">
                <div class="image-block" style="background: url('https://cinemato.nl/media/pages/home/8b7bebe784-1632386341/cinemato-team-productieshoot-foto-720x720.jpg') center center; background-size: cover;"></div>
             </div>
          </div>
          <div class="circle-text white">
             <svg width="110" height="110" viewBox="0 0 110 110">
                <path id="textpath-1" d="M 55, 55 m -55, 0 a 55,55 0 1,0 110,0 a 55,55 0 1,0 -110,0 " />
                <text fill="#FFF">
                   <textPath xlink:href="#textpath-1" startOffset="50%" text-anchor="middle">דבור תכנון וביצוע</textPath>
                </text>
             </svg>
          </div>
       </div>
    </div>
 </div>
 @if(count($HomePartners)>0)
 <div class="section logos-section">
    <div class="wrap">
       <h3>בין לקוחותינו</h3>
       <div class="row">
        <?php
            $ii = 0;
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $details_var = "details_" . @Helper::currentLanguage()->code;
            $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
            $section_url = "";
            ?>

            @foreach($HomePartners as $HomePartner)
                <?php
                if ($HomePartner->$title_var != "") {
                    $title = $HomePartner->$title_var;
                } else {
                    $title = $HomePartner->$title_var2;
                }

                if ($section_url == "") {
                    $section_url = Helper::sectionURL($HomePartner->webmaster_id);
                }
                ?>
                    <li class="single-logo">
                    <img src="{{ URL::to('uploads/topics/'.$HomePartner->photo_file) }}" height="98" width="320" alt="{{ $title }}" title="{{ $title }}"/>
                    </li>
                @endforeach
            </div>
    </div>
 </div>
 @endif
@endsection

@section('Meta')
<link rel="alternate" hreflang="he-il" href="{{ URL::to('') }}/he/" />
<link rel="alternate" hreflang="en-us" href="{{ URL::to('') }}/en/" />
<link rel="alternate" hreflang="ar-sa" href="{{ URL::to('') }}/ar/" />
@endsection

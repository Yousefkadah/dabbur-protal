@extends('frontEnd.layout')
@section('namespace'){{'single-project'}}@endsection
@section('namespace_id'){{'single-project'}}@endsection
@section('content')
<div class="scroll-indicator">
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
</div>
<?php
$title_var = "title_" . @Helper::currentLanguage()->code;
$title_var2 = "title_" . env('DEFAULT_LANGUAGE');
$details_var = "details_" . @Helper::currentLanguage()->code;
$details_var2 = "details_" . env('DEFAULT_LANGUAGE');
if ($Topic->$title_var != "") {
    $title = $Topic->$title_var;
} else {
    $title = $Topic->$title_var2;
}
if ($Topic->$details_var != "") {
    $details = $details_var;
} else {
    $details = $details_var2;
}
$section = "";
try {
    if ($Topic->section->$title_var != "") {
        $section = $Topic->section->$title_var;
    } else {
        $section = $Topic->section->$title_var2;
    }
} catch (Exception $e) {
    $section = "";
}
?>
<div class="section default-header full-height">
    <div class="header-title-wrap">
       <div class="header-title-wrap-block">
          <div class="wrap animate-fade-in" style="opacity: 1; transform: translate(0px, 0px);">
             <div class="sub-wrap">
                <h1>{{$title}}</h1>
                <div>
                   <div class="case-info">
                      <h3 style="color: #FF4D0F;">{{$Topic->categories[0]->section->$title_var}}</h3>
                   </div>
                    </div>
             </div>
          </div>
          <h3 class="left-text"><span>{{$title}}</span></h3>
       </div>
    </div>
    <div class="letter-block">
       <svg>
          <pattern id="pattern" patternUnits="userSpaceOnUse" width="560" height="400">
             <image x="0" width="100%" height="100%" xlink:href="{{ URL::to('uploads/topics/'.$Topic->photo_file) }}"></image>
          </pattern>
          <text class="big-letter" x="50%" y="57%" dominant-baseline="middle" text-anchor="middle" style="fill:url(#pattern);">{{ mb_substr($title,0,1) }}</text>
       </svg>
       <span class="big-letter-back animate-in appear">{{ mb_substr($title,0,1) }}</span>
    </div>
    <div class="bottom-bar"></div>
    <div class="dark-overlay"></div>
    <div class="header-photo" style="background: url('{{ URL::to('uploads/topics/'.$Topic->photo_file) }}') center center; background-size: cover;"></div>
 </div>

 <div class="section project-intro">
    <div class="wrap textblock">
        {!! $Topic->$details !!}
    </div>
 </div>
 @if(count($Topic->photos)>0)
 <div class="section image square image-slider">
    <div class="wrap-wide">
       <div class="swiper-container swiper-images-slider swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden">
          <div class="swiper-wrapper" id="swiper-wrapper-7a74c2d6bd7ac948" aria-live="polite">
            @foreach($Topic->photos as $photo)
            <div class="swiper-slide">
                <div class="image-container">
                   <div class="image-block" style="background: url('{{ URL::to('uploads/topics/'.$photo->file) }}') center center; background-size: cover;"></div>
                </div>
             </div>
             @endforeach
            </div>
          <div class="swiper-button-next-images-slider slider-next-button">
             <svg width="21" height="15"><path d="M13.501 14.572l-.707-.707L18.658 8H0V7h18.657l-5.863-5.863.707-.707 6.364 6.364.707.707-7.07 7.071z" fill="#171C21" fill-rule="evenodd"></path></svg>
          </div>
          <div class="swiper-button-prev-images-slider slider-prev-button swiper-button-disabled">
             <svg width="21" height="15"><path d="M13.501 14.572l-.707-.707L18.658 8H0V7h18.657l-5.863-5.863.707-.707 6.364 6.364.707.707-7.07 7.071z" fill="#171C21" fill-rule="evenodd"></path></svg>
          </div>
       <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
    </div>
 </div>
 @endif

@if($WebmasterSection->related_status)
@if(count($Topic->relatedTopics))
 <div class="section projects-grid recent single-project-recent">
    <div class="wrap-text">
       <h3>{{ __('backend.relatedTopics') }}</h3>
    </div>
    <div class="wrap-wide">
        <ul>
            <?php
            $title_var = "title_" . @Helper::currentLanguage()->code;
            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
            $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
            $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
            ?>
            @foreach($Topic->relatedTopics as $relatedTopic)
                <?php
                if ($relatedTopic->topic->$title_var != "") {
                    $relatedTopic_title = $relatedTopic->topic->$title_var;
                } else {
                    $relatedTopic_title = $relatedTopic->topic->$title_var2;
                }
                ?>
            <li style="background: url('{{ URL::to('uploads/topics/'.$relatedTopic->topic->photo_file) }}') center center; background-size: cover;">
             <a href="{{ Helper::topicURL($relatedTopic->topic->id) }}">
                <div class="project-textblock">
                   <h3 style="color: #FF4D0F;">{{$relatedTopic->topic->categories[0]->section->$title_var}}</h3>
                   <h2>{!! $relatedTopic_title !!}</h2>
                </div>
                <div class="project-shape">
                   <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                         <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                            <polygon points="260 0 320 0 260 140"></polygon>
                         </g>
                      </g>
                   </svg>
                   <div class="circle-text small">
                   <svg width="80" height="80" viewBox="0 0 80 80">
                      <path id="textpath-2" d="M 40, 40 m -40, 0 a 40,40 0 1,0 80,0 a 40,40 0 1,0 -80,0 "></path>
                      <text fill="#FFF">
                         <textPath xlink:href="#textpath-2" startOffset="50%" text-anchor="middle">Lees meer</textPath>
                      </text>
                   </svg>
                </div>
                </div>
                <span>{{ mb_substr($title,0,1) }}</span>
                <div class="dark-overlay"></div>
             </a>
            </li>
        @endforeach
        </ul>
            </div>
 </div>
@endif
@endif
@endsection

@extends('frontEnd.layout')
@section('namespace'){{'projects'}}@endsection
@section('namespace_id'){{'projects'}}@endsection
@section('content')
<?php
$title_var = "title_" . @Helper::currentLanguage()->code;
$title_var2 = "title_" . env('DEFAULT_LANGUAGE');
$details_var = "details_" . @Helper::currentLanguage()->code;
$details_var2 = "details_" . env('DEFAULT_LANGUAGE');
$slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
$slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
$i = 0;
?>
<div class="scroll-indicator">
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
</div>
<div class="section default-header">
    <div class="header-title-wrap">
       <div class="header-title-wrap-block">
          <div class="wrap-text animate-fade-in" style="opacity: 1; transform: translate(0px, 0px);">
             <h1>פרויקטים</h1>
             <h1 class="outlined animate-in appear">פרויקטים</h1>
             <p></p><p>הפרויקטים מבוצעים ע"י עובדי החברה וצוות הנדסי בעלי ניסיון של מעל שלושים שנים במקצוע.
                אחרי נסיון רב והסכלה הנדסית השתלב הביצוע והתכנון ,ומאז החברה נותנת מענה לשירותי תכנון ,יעוץ , פיקוח וחישוב כמויות לכל סוגי הפרויקטים וכל הלקוחות בין יזמים, קבלני ביצוע , קבלני משנה וכולי.
                הצוות שלנו מיומן ומבטיח מקצועיות וביצוע העבודות בסטנדרטים גבוהים ביותר של איכות ביצוע, איכות שרות ונאמנות ללקוחות החברה</p><p></p>
          </div>
          <h3 class="left-text"><span>פרויקטים</span></h3>
       </div>
    </div>
 </div>

<div class="section projects-grid">
    <div class="wrap-wide">
       <ul>
        @foreach($Topics as $Topic)
            <?php
                if ($Topic->$title_var != "") {
                    $title = $Topic->$title_var;
                } else {
                    $title = $Topic->$title_var2;
                }
                if ($Topic->$details_var != "") {
                    $details = $details_var;
                } else {
                    $details = $details_var2;
                }
                $section = "";
                try {
                    if ($Topic->section->$title_var != "") {
                        $section = $Topic->section->$title_var;
                    } else {
                        $section = $Topic->section->$title_var2;
                    }
                } catch (Exception $e) {
                    $section = "";
                }
                $topic_link_url = Helper::topicURL($Topic->id);
            ?>
            <li style="background: url('{{ URL::to('uploads/topics/'.$Topic->photo_file) }}') center center; background-size: cover;">
                <a href="{{ $topic_link_url }}">
                    <div class="project-textblock">
                    <h3 style="color: #FF4D0F;">{{$Topic->categories[0]->section->$title_var}}</h3>
                    <h2>{{$title}}</h2>
                    </div>
                    <div class="project-shape">
                    <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                                <polygon points="260 0 320 0 260 140"></polygon>
                            </g>
                        </g>
                    </svg>
                    <div class="circle-text small">
                    <svg width="80" height="80" viewBox="0 0 80 80">
                        <path id="textpath-2" d="M 40, 40 m -40, 0 a 40,40 0 1,0 80,0 a 40,40 0 1,0 -80,0 "></path>
                        <text fill="#FFF">
                            <textPath xlink:href="#textpath-2" startOffset="50%" text-anchor="middle">הצג פרויקט</textPath>
                        </text>
                    </svg>
                    </div>
                    </div>
                    <span>{{ mb_substr($title,0,1) }}</span>
                    <div class="dark-overlay"></div>
                </a>
            </li>
        @endforeach
    </ul>
</div>
</div>
@endsection

@extends('frontEnd.layout')

@section('content')
<section id="inner-headline">
<div class="standard-page">
    <div class="standard-page__header">
      <div class="header-banner__image">
        <picture>
          <source srcset="{{  URL::asset('uploads/topics/'.$WebmasterSection->section_photo)}}" media="(max-width: 768px)" type="image/jpeg">
          <source srcset="{{  URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" type="image/jpeg">
          <img src="{{ URL::asset('uploads/topics/'.$WebmasterSection->section_photo) }}" alt="" typeof="foaf:Image">
        </picture>
      </div>
      <div class="header-banner__container container">
        <div class="header-banner__text">
        <div class="block block-system block-system-breadcrumb-block">
            <div class="wrapper">
                <div class="block-body ">
                    <nav class="breadcrumb" role="navigation" aria-labelledby="system-breadcrumb">
                    <h2 id="system-breadcrumb" class="visually-hidden">Breadcrumb</h2>
                    <ol>
                        <li><a href="{{ route("Home") }}">{{__('backend.home')}}</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <h1 class="title">
            @if(@$WebmasterSection!="none")
                            <?php
                            $title_var = "title_" . @Helper::currentLanguage()->code;
                            $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                            if (@$WebmasterSection->$title_var != "") {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var;
                            } else {
                                $WebmasterSectionTitle = @$WebmasterSection->$title_var2;
                            }
                            ?>
                            {!! $WebmasterSectionTitle !!}
                        @elseif(@$search_word!="")
                            {{ @$search_word }}
                        @else
                            {{ $User->name }}
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . @Helper::currentLanguage()->code;
                                ?>
                                {{ $CurrentCategory->$category_title_var }}
                            @endif
                        @endif
        </h1>
        </div>
      </div>
    </div>
    <div class="standard-page__content">
        <div class="container">
          <div id="ust30a_news_filter">
              <div class="news-filter">
                <div class="news-filter__category-picker">
                      <select></select>
                </div>
            </div>
        </div>
        </div>


    </section>
    <section id="content" class="main-section">
        <div id="faqs" class="container">
                    @if($Topics->total() == 0)
                        <div class="alert alert-warning">
                            <i class="fa fa-info"></i> &nbsp; {{ __('frontend.noData') }}
                        </div>
                    @else
                            @if($Topics->total() > 0)

                                <?php
                                $title_var = "title_" . @Helper::currentLanguage()->code;
                                $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
                                $details_var = "details_" . @Helper::currentLanguage()->code;
                                $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
                                $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
                                $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
                                $i = 0;
                                ?>
                                <div  id="faqSection">
                                @foreach($Topics as $Topic)
                                    <?php
                                    if ($Topic->$title_var != "") {
                                        $title = $Topic->$title_var;
                                    } else {
                                        $title = $Topic->$title_var2;
                                    }
                                    if ($Topic->$details_var != "") {
                                        $details = $details_var;
                                    } else {
                                        $details = $details_var2;
                                    }
                                    $section = "";
                                    try {
                                        if ($Topic->section->$title_var != "") {
                                            $section = $Topic->section->$title_var;
                                        } else {
                                            $section = $Topic->section->$title_var2;
                                        }
                                    } catch (Exception $e) {
                                        $section = "";
                                    }

                                    // set row div
                                    //if (($i == 1 && count($Categories) > 0) || ($i == 2 && count($Categories) == 0)) {
                                    //    $i = 0;
                                   //     echo "</div><div class='row'>";
                                  //  }
                                    $topic_link_url = Helper::topicURL($Topic->id);
                                    ?>
                                        <div>
                                            <button class="collapsible">
                                                {{ $title }}
                                            </button>
                                            <div class="faq-content">
                                            <p>{!! strip_tags($Topic->$details) !!}</p>
                                            </div>

                                        </div>
                                    <?php
                                    $i++;
                                    ?>
                                @endforeach

                        <div class="row">
                            <div class="col-lg-8">
                                {!! $Topics->appends(request()->input())->links() !!}
                            </div>
                            <div class="col-lg-4 text-right">
                                <br>
                                <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ __('backend.of') }}
                                    ( {{ $Topics->total()  }} ) {{ __('backend.records') }}</small>
                            </div>
                        </div>
                    @endif
                    @endif
                @if(count($Categories)<0)
                    @include('frontEnd.includes.side')
                @endif
        </div>
    </section>

@endsection

@section('Meta')
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "FAQPage",
      "mainEntity": [
        <?php
        $title_var = "title_" . @Helper::currentLanguage()->code;
        $title_var2 = "title_" . env('DEFAULT_LANGUAGE');
        $details_var = "details_" . @Helper::currentLanguage()->code;
        $details_var2 = "details_" . env('DEFAULT_LANGUAGE');
        $slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
        $slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
        $i = 0;
        ?>
        @foreach($Topics as $Topic)
            <?php
            if ($Topic->$title_var != "") {
                $title = $Topic->$title_var;
            } else {
                $title = $Topic->$title_var2;
            }
            if ($Topic->$details_var != "") {
                $details = $details_var;
            } else {
                $details = $details_var2;
            }
            $section = "";
            try {
                if ($Topic->section->$title_var != "") {
                    $section = $Topic->section->$title_var;
                } else {
                    $section = $Topic->section->$title_var2;
                }
            } catch (Exception $e) {
                $section = "";
            }
            $topic_link_url = Helper::topicURL($Topic->id);
            ?>
        {
        "@type": "Question",
        "name": "{{ $title }}",
        "acceptedAnswer":
            {
            "@type": "Answer",
            "text": "{!! strip_tags($Topic->$details) !!}"
            }
        }
        @if (!$loop->last)
            ,
        @endif
        @endforeach
      ]
    }
    </script>
@endsection

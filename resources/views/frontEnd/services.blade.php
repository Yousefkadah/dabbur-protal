@extends('frontEnd.layout')
@section('namespace'){{'single-service'}}@endsection
@section('namespace_id'){{'single-service'}}@endsection
@section('content')
<?php
$title_var = "title_" . @Helper::currentLanguage()->code;
$title_var2 = "title_" . env('DEFAULT_LANGUAGE');
$details_var = "details_" . @Helper::currentLanguage()->code;
$details_var2 = "details_" . env('DEFAULT_LANGUAGE');
$slug_var = "seo_url_slug_" . @Helper::currentLanguage()->code;
$slug_var2 = "seo_url_slug_" . env('DEFAULT_LANGUAGE');
$i = 0;
?>
<div class="scroll-indicator">
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
    <svg width="14" height="12"><path d="M13.11.617H.992l-.473.815L6.58 11.885h.944l6.059-10.453z" fill="#FFF" fill-rule="evenodd"></path></svg>
</div>

<div class="section default-header full-height service-header">
    <div class="header-title-wrap">
       <div class="header-title-wrap-block">
          <div class="wrap animate-fade-in" style="opacity: 1; transform: translate(0px, 0px);">
            <div class="sub-wrap textblock">
                <h1>שירותי החברה</h1>
                <p>הפרויקטים מבוצעים ע"י עובדי החברה וצוות הנדסי בעלי ניסיון של מעל שלושים שנים במקצוע.
                 אחרי נסיון רב והסכלה הנדסית השתלב הביצוע והתכנון ,ומאז החברה נותנת מענה לשירותי תכנון ,יעוץ , פיקוח וחישוב כמויות לכל סוגי הפרויקטים וכל הלקוחות בין יזמים, קבלני ביצוע , קבלני משנה וכולי.
                    הצוות שלנו מיומן ומבטיח מקצועיות וביצוע העבודות בסטנדרטים גבוהים ביותר של איכות ביצוע, איכות שרות ונאמנות ללקוחות החברה.</p>
                <a class="btn blue" href="#services-grid">שירותי החברה</a>
            </div>
             <div class="sub-wrap">
                <div class="single-service-thumb" style="background: url('https://cinemato.nl/media/pages/instudio/4fd3142449-1632386463/webinar-livestream_thumb.png') center center no-repeat; background-size: contain;"></div>
             </div>
          </div>
          <h3 class="left-text" style="color:#171C21;"><span>שירותי החברה</span></h3>
       </div>
    </div>
    <div class="single-service-photo-back"></div>
    <div class="single-service-back">
       <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
             <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                <polygon points="260 0 320 0 260 140"></polygon>
             </g>
          </g>
       </svg>
    </div>
    <div class="background-art"><svg width="396" height="878"><path d="M58.969 462L392 654.489v30.014L58.969 877 33 861.99V477.002L58.969 462zM224.509 1C303.636 1 358.291 33.596 395 72.71v11.406l-87.28 81.49h-11.422c-14.687-17.924-37.53-30.15-64.446-30.15-43.234 0-74.23 33.412-74.23 79.046 0 45.63 30.996 79.042 74.23 79.042 26.917 0 49.76-12.222 64.446-30.15h11.421L395 344.875v11.414C358.292 395.4 303.637 428 224.508 428 96.441 428 1 338.361 1 214.502 1 90.639 96.441 1 224.508 1z" stroke="#171C21" fill="none" fill-rule="evenodd"></path></svg></div>
    <div class="circle-text" style="right: calc(50vw + 80px);">
       <svg width="110" height="110" viewBox="0 0 110 110">
       <path id="textpath-1" d="M 55, 55 m -55, 0 a 55,55 0 1,0 110,0 a 55,55 0 1,0 -110,0 "></path>
          <text fill="#FFF">
             <textPath xlink:href="#textpath-1" startOffset="50%" text-anchor="middle">דבור תכנון,ביצוע</textPath>
          </text>
       </svg>
    </div>
    <ul class="breadcrumb" aria-label="breadcrumb">
        <li class="">
          <a href="{{ route("Home") }}">{{__('backend.home')}}</a>
        </li>
        <li class="active">
          <a href="#">שירותי החברה</a>
        </li>
    </ul>
</div>

<div class="section services-grid" id="services-grid">
    <div class="wrap-wide">
       <h3>שירותי החברה</h3>
       <ul>
        @foreach($Topics as $Topic)
            <?php
                if ($Topic->$title_var != "") {
                    $title = $Topic->$title_var;
                } else {
                    $title = $Topic->$title_var2;
                }
                if ($Topic->$details_var != "") {
                    $details = $details_var;
                } else {
                    $details = $details_var2;
                }
                $section = "";
                try {
                    if ($Topic->section->$title_var != "") {
                        $section = $Topic->section->$title_var;
                    } else {
                        $section = $Topic->section->$title_var2;
                    }
                } catch (Exception $e) {
                    $section = "";
                }
                $topic_link_url = Helper::topicURL($Topic->id);
            ?>
            <li class="textblock">
             <a href="{{$topic_link_url}}" style="background: url('{{ URL::to('uploads/topics/'.$Topic->photo_file) }}') center center; background-size: cover;">
                <div class="dark-overlay"></div>
                <div class="services-shape">
                   <svg class="rec-shape-svg" width="60px" height="140px" viewBox="0 0 60 140" xmlns="http://www.w3.org/2000/svg">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                         <g class="svg-fill" transform="translate(-260.000000, 0.000000)" fill="#0038AE">
                            <polygon points="260 0 320 0 260 140"></polygon>
                         </g>
                      </g>
                   </svg>
                </div>
                <div class="block">
                   <h2>{{$title}}</h2>
                </div>
             </a>
             <p>{!! mb_substr(strip_tags($Topic->$details),0, 300)."..." !!}</p>
            </li>
            @endforeach
        </ul>
    </div>
 </div>
@endsection

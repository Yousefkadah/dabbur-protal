-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 23, 2022 at 09:54 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartend`
--

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_analytics_pages`
--

DROP TABLE IF EXISTS `iziengdb_analytics_pages`;
CREATE TABLE IF NOT EXISTS `iziengdb_analytics_pages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `visitor_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `load_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=838 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_analytics_pages`
--

INSERT INTO `iziengdb_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, '127.0.0.1', 'Dashboard &raquo; Sign in to CONTROL', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.5539701', '2022-04-03', '19:51:24', '2022-04-03 16:51:24', '2022-04-03 16:51:24'),
(2, 1, '127.0.0.1', 'Dashboard &raquo; Site Title', 'unknown', 'http://127.0.0.1:8000/admin', '1.44474196', '2022-04-03', '19:51:40', '2022-04-03 16:51:40', '2022-04-03 16:51:40'),
(3, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '1.84938002', '2022-04-03', '19:51:43', '2022-04-03 16:51:43', '2022-04-03 16:51:43'),
(4, 1, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js/install', '0.50413013', '2022-04-03', '19:51:44', '2022-04-03 16:51:44', '2022-04-03 16:51:44'),
(5, 1, '127.0.0.1', 'Dashboard &raquo; General Settings', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '1.84802008', '2022-04-03', '19:51:57', '2022-04-03 16:51:57', '2022-04-03 16:51:57'),
(6, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.65965295', '2022-04-03', '19:51:59', '2022-04-03 16:51:59', '2022-04-03 16:51:59'),
(7, 1, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css/install', '0.38098192', '2022-04-03', '19:51:59', '2022-04-03 16:51:59', '2022-04-03 16:51:59'),
(8, 1, '127.0.0.1', 'Dashboard &raquo; Contacts', 'unknown', 'http://127.0.0.1:8000/admin/contacts', '0.44104695', '2022-04-03', '19:53:48', '2022-04-03 16:53:48', '2022-04-03 16:53:48'),
(9, 1, '127.0.0.1', 'Dashboard &raquo; Inbox', 'unknown', 'http://127.0.0.1:8000/admin/webmails', '0.38695693', '2022-04-03', '19:53:52', '2022-04-03 16:53:52', '2022-04-03 16:53:52'),
(10, 1, '127.0.0.1', 'Dashboard &raquo; Inbox', 'unknown', 'http://127.0.0.1:8000/admin/webmails/create', '0.28775001', '2022-04-03', '19:53:54', '2022-04-03 16:53:54', '2022-04-03 16:53:54'),
(11, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/summernote/dist/summernote.css', '0.28897595', '2022-04-03', '19:53:55', '2022-04-03 16:53:55', '2022-04-03 16:53:55'),
(12, 1, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/summernote/dist/summernote.css/install', '0.25194311', '2022-04-03', '19:53:55', '2022-04-03 16:53:55', '2022-04-03 16:53:55'),
(13, 1, '127.0.0.1', 'Dashboard &raquo; Calendar', 'unknown', 'http://127.0.0.1:8000/admin/calendar', '0.40530586', '2022-04-03', '19:53:59', '2022-04-03 16:53:59', '2022-04-03 16:53:59'),
(14, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.28421712', '2022-04-03', '19:54:00', '2022-04-03 16:54:00', '2022-04-03 16:54:00'),
(15, 1, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css/install', '0.23791599', '2022-04-03', '19:54:00', '2022-04-03 16:54:00', '2022-04-03 16:54:00'),
(16, 1, '127.0.0.1', 'Dashboard &raquo; Site pages', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.41816592', '2022-04-03', '19:54:04', '2022-04-03 16:54:04', '2022-04-03 16:54:04'),
(17, 1, '127.0.0.1', 'Dashboard &raquo; Home Welcome', 'unknown', 'http://127.0.0.1:8000/admin/1/topics/5/edit', '1.37140203', '2022-04-03', '19:54:10', '2022-04-03 16:54:10', '2022-04-03 16:54:10'),
(18, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.js', '0.29183602', '2022-04-03', '19:54:49', '2022-04-03 16:54:49', '2022-04-03 16:54:49'),
(19, 1, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.js/install', '0.24410701', '2022-04-03', '19:54:49', '2022-04-03 16:54:49', '2022-04-03 16:54:49'),
(20, 1, '127.0.0.1', 'Dashboard &raquo; Services', 'unknown', 'http://127.0.0.1:8000/admin/2/topics', '0.25266385', '2022-04-03', '19:55:04', '2022-04-03 16:55:04', '2022-04-03 16:55:04'),
(21, 1, '127.0.0.1', 'Dashboard &raquo; Services', 'unknown', 'http://127.0.0.1:8000/admin/2/topics/create', '0.50595498', '2022-04-03', '19:55:07', '2022-04-03 16:55:07', '2022-04-03 16:55:07'),
(22, 1, '127.0.0.1', 'Dashboard &raquo; News', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.25329304', '2022-04-03', '19:55:12', '2022-04-03 16:55:12', '2022-04-03 16:55:12'),
(23, 1, '127.0.0.1', 'Dashboard &raquo; Photos', 'unknown', 'http://127.0.0.1:8000/admin/4/topics', '0.26695514', '2022-04-03', '19:55:14', '2022-04-03 16:55:14', '2022-04-03 16:55:14'),
(24, 1, '127.0.0.1', 'Dashboard &raquo; Products', 'unknown', 'http://127.0.0.1:8000/admin/8/topics', '0.27229595', '2022-04-03', '19:55:19', '2022-04-03 16:55:19', '2022-04-03 16:55:19'),
(25, 1, '127.0.0.1', 'Dashboard &raquo; Partners', 'unknown', 'http://127.0.0.1:8000/admin/9/topics', '0.25148487', '2022-04-03', '19:55:21', '2022-04-03 16:55:21', '2022-04-03 16:55:21'),
(26, 1, '127.0.0.1', 'Dashboard &raquo; Ad. Banners', 'unknown', 'http://127.0.0.1:8000/admin/banners', '0.37699199', '2022-04-03', '19:55:24', '2022-04-03 16:55:24', '2022-04-03 16:55:24'),
(27, 1, '127.0.0.1', 'Dashboard &raquo; Ad. Banners', 'unknown', 'http://127.0.0.1:8000/admin/banners/1/edit', '0.37560391', '2022-04-03', '19:55:27', '2022-04-03 16:55:27', '2022-04-03 16:55:27'),
(28, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/', '0.28648901', '2022-04-03', '19:55:33', '2022-04-03 16:55:33', '2022-04-03 16:55:33'),
(29, 1, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/install', '0.24124694', '2022-04-03', '19:55:33', '2022-04-03 16:55:33', '2022-04-03 16:55:33'),
(30, 1, '127.0.0.1', 'Dashboard &raquo; General Settings', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.71322894', '2022-04-03', '19:55:47', '2022-04-03 16:55:47', '2022-04-03 16:55:47'),
(31, 1, '127.0.0.1', 'Dashboard &raquo; Site Menus', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.41804004', '2022-04-03', '19:58:53', '2022-04-03 16:58:53', '2022-04-03 16:58:53'),
(32, 1, '127.0.0.1', 'Dashboard &raquo; File Manager', 'unknown', 'http://127.0.0.1:8000/admin/file-manager', '0.2668829', '2022-04-03', '19:58:56', '2022-04-03 16:58:56', '2022-04-03 16:58:56'),
(33, 1, '127.0.0.1', 'Dashboard &raquo; Users &amp; Permissions', 'unknown', 'http://127.0.0.1:8000/admin/users', '0.46013808', '2022-04-03', '19:59:11', '2022-04-03 16:59:11', '2022-04-03 16:59:11'),
(34, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.29777098', '2022-04-03', '19:59:22', '2022-04-03 16:59:22', '2022-04-03 16:59:22'),
(35, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.26804209', '2022-04-03', '19:59:22', '2022-04-03 16:59:22', '2022-04-03 16:59:22'),
(36, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.27000093', '2022-04-03', '19:59:22', '2022-04-03 16:59:22', '2022-04-03 16:59:22'),
(37, 1, '127.0.0.1', 'Dashboard &raquo; Site Sections', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.34838319', '2022-04-03', '19:59:26', '2022-04-03 16:59:26', '2022-04-03 16:59:26'),
(38, 1, '127.0.0.1', 'Dashboard &raquo; Banners settings', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/banners', '0.33980918', '2022-04-03', '19:59:33', '2022-04-03 16:59:33', '2022-04-03 16:59:33'),
(39, 1, '127.0.0.1', 'Dashboard &raquo; Banners settings', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/banners/1/edit', '0.31035209', '2022-04-03', '19:59:39', '2022-04-03 16:59:39', '2022-04-03 16:59:39'),
(40, 1, '127.0.0.1', 'Dashboard &raquo; Videos', 'unknown', 'http://127.0.0.1:8000/admin/5/topics', '0.26864386', '2022-04-03', '19:59:46', '2022-04-03 16:59:46', '2022-04-03 16:59:46'),
(41, 1, '127.0.0.1', 'Dashboard &raquo; Analytics', 'unknown', 'http://127.0.0.1:8000/admin/analytics/hostname', '0.33775115', '2022-04-03', '19:59:52', '2022-04-03 16:59:52', '2022-04-03 16:59:52'),
(42, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.28336501', '2022-04-03', '19:59:53', '2022-04-03 16:59:53', '2022-04-03 16:59:53'),
(43, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.31027412', '2022-04-03', '19:59:53', '2022-04-03 16:59:53', '2022-04-03 16:59:53'),
(44, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.27548385', '2022-04-03', '19:59:54', '2022-04-03 16:59:54', '2022-04-03 16:59:54'),
(45, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.29017711', '2022-04-03', '19:59:55', '2022-04-03 16:59:55', '2022-04-03 16:59:55'),
(46, 1, '127.0.0.1', 'Dashboard &raquo; Analytics', 'unknown', 'http://127.0.0.1:8000/admin/analytics/country', '0.24641109', '2022-04-03', '19:59:55', '2022-04-03 16:59:55', '2022-04-03 16:59:55'),
(47, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.2628181', '2022-04-03', '19:59:55', '2022-04-03 16:59:55', '2022-04-03 16:59:55'),
(48, 1, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/date', '1.33225989', '2022-04-03', '20:00:16', '2022-04-03 17:00:16', '2022-04-03 17:00:16'),
(49, 1, '127.0.0.1', 'من نحن', 'unknown', 'http://127.0.0.1:8000/ar/topic/about', '0.52858496', '2022-04-03', '20:00:28', '2022-04-03 17:00:28', '2022-04-03 17:00:28'),
(50, 1, '127.0.0.1', 'المدونة', 'unknown', 'http://127.0.0.1:8000/ar/blog', '0.42324996', '2022-04-03', '20:00:32', '2022-04-03 17:00:32', '2022-04-03 17:00:32'),
(51, 1, '127.0.0.1', 'اتصل بنا', 'unknown', 'http://127.0.0.1:8000/ar/contact', '0.41426301', '2022-04-03', '20:00:34', '2022-04-03 17:00:34', '2022-04-03 17:00:34'),
(52, 1, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/9/edit', '0.63391089', '2022-04-03', '20:03:17', '2022-04-03 17:03:17', '2022-04-03 17:03:17'),
(53, 1, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/create', '0.42135978', '2022-04-03', '20:03:43', '2022-04-03 17:03:43', '2022-04-03 17:03:43'),
(54, 1, '127.0.0.1', 'Dashboard &raquo; العملاء', 'unknown', 'http://127.0.0.1:8000/admin/9/topics/create', '0.24217677', '2022-04-03', '20:04:07', '2022-04-03 17:04:07', '2022-04-03 17:04:07'),
(55, 1, '127.0.0.1', 'Dashboard &raquo; الصوتيات', 'unknown', 'http://127.0.0.1:8000/admin/6/topics', '0.24838901', '2022-04-03', '20:04:14', '2022-04-03 17:04:14', '2022-04-03 17:04:14'),
(56, 1, '127.0.0.1', 'Dashboard &raquo; المدونة', 'unknown', 'http://127.0.0.1:8000/admin/7/topics', '0.23877311', '2022-04-03', '20:04:17', '2022-04-03 17:04:17', '2022-04-03 17:04:17'),
(57, 1, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.29320097', '2022-04-03', '20:04:18', '2022-04-03 17:04:18', '2022-04-03 17:04:18'),
(58, 1, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.28713107', '2022-04-03', '20:04:18', '2022-04-03 17:04:18', '2022-04-03 17:04:18'),
(59, 1, '127.0.0.1', 'Dashboard &raquo; المدونة', 'unknown', 'http://127.0.0.1:8000/admin/7/topics/create', '0.27178216', '2022-04-03', '20:04:19', '2022-04-03 17:04:19', '2022-04-03 17:04:19'),
(60, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/', '0.29147696', '2022-04-03', '20:08:06', '2022-04-03 17:08:06', '2022-04-03 17:08:06'),
(61, 1, '127.0.0.1', 'Dashboard &raquo; Sign in to CONTROL', 'unknown', 'http://localhost:8000/admin/login', '0.20254207', '2022-04-03', '20:08:09', '2022-04-03 17:08:09', '2022-04-03 17:08:09'),
(62, 1, '127.0.0.1', 'Dashboard &raquo; Site Title', 'unknown', 'http://localhost:8000/admin', '0.32000399', '2022-04-03', '20:08:29', '2022-04-03 17:08:29', '2022-04-03 17:08:29'),
(63, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.js', '0.27347708', '2022-04-03', '20:08:32', '2022-04-03 17:08:32', '2022-04-03 17:08:32'),
(64, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.27874708', '2022-04-03', '20:08:33', '2022-04-03 17:08:33', '2022-04-03 17:08:33'),
(65, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.28234887', '2022-04-03', '20:08:34', '2022-04-03 17:08:34', '2022-04-03 17:08:34'),
(66, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.24135399', '2022-04-03', '20:08:35', '2022-04-03 17:08:35', '2022-04-03 17:08:35'),
(67, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.31676507', '2022-04-03', '20:08:35', '2022-04-03 17:08:35', '2022-04-03 17:08:35'),
(68, 1, '127.0.0.1', 'Site Title', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.29281688', '2022-04-03', '20:08:36', '2022-04-03 17:08:36', '2022-04-03 17:08:36'),
(69, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/', '1.95808196', '2022-04-05', '22:30:43', '2022-04-05 19:30:43', '2022-04-05 19:30:43'),
(70, 2, '127.0.0.1', 'Dashboard &raquo; Sign in to CONTROL', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.34540606', '2022-04-05', '22:30:47', '2022-04-05 19:30:47', '2022-04-05 19:30:47'),
(71, 2, '127.0.0.1', 'Dashboard &raquo; Site Title', 'unknown', 'http://127.0.0.1:8000/admin', '0.49464393', '2022-04-05', '22:30:50', '2022-04-05 19:30:50', '2022-04-05 19:30:50'),
(72, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.50376797', '2022-04-05', '22:30:51', '2022-04-05 19:30:51', '2022-04-05 19:30:51'),
(73, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.4585619', '2022-04-05', '22:30:52', '2022-04-05 19:30:52', '2022-04-05 19:30:52'),
(74, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.4138701', '2022-04-05', '22:30:53', '2022-04-05 19:30:53', '2022-04-05 19:30:53'),
(75, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.43913317', '2022-04-05', '22:30:54', '2022-04-05 19:30:54', '2022-04-05 19:30:54'),
(76, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.46276903', '2022-04-05', '22:30:55', '2022-04-05 19:30:55', '2022-04-05 19:30:55'),
(77, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.4603219', '2022-04-05', '22:30:56', '2022-04-05 19:30:56', '2022-04-05 19:30:56'),
(78, 2, '127.0.0.1', 'Dashboard &raquo; General Settings', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '0.42393494', '2022-04-05', '22:31:02', '2022-04-05 19:31:02', '2022-04-05 19:31:02'),
(79, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.59044099', '2022-04-05', '22:31:03', '2022-04-05 19:31:03', '2022-04-05 19:31:03'),
(80, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.51686215', '2022-04-05', '22:31:04', '2022-04-05 19:31:04', '2022-04-05 19:31:04'),
(81, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.53218794', '2022-04-05', '22:31:04', '2022-04-05 19:31:04', '2022-04-05 19:31:04'),
(82, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.55605602', '2022-04-05', '22:31:05', '2022-04-05 19:31:05', '2022-04-05 19:31:05'),
(83, 2, '127.0.0.1', 'Dashboard &raquo; Services', 'unknown', 'http://127.0.0.1:8000/admin/2/topics', '0.38352013', '2022-04-05', '22:31:17', '2022-04-05 19:31:17', '2022-04-05 19:31:17'),
(84, 2, '127.0.0.1', 'Dashboard &raquo; Site pages', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.34512115', '2022-04-05', '22:31:20', '2022-04-05 19:31:20', '2022-04-05 19:31:20'),
(85, 2, '127.0.0.1', 'Dashboard &raquo; Home Welcome', 'unknown', 'http://127.0.0.1:8000/admin/1/topics/5/edit', '0.41822791', '2022-04-05', '22:31:23', '2022-04-05 19:31:23', '2022-04-05 19:31:23'),
(86, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.js', '0.57803798', '2022-04-05', '22:31:24', '2022-04-05 19:31:24', '2022-04-05 19:31:24'),
(87, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.css', '0.41449618', '2022-04-05', '22:31:25', '2022-04-05 19:31:25', '2022-04-05 19:31:25'),
(88, 2, '127.0.0.1', 'Dashboard &raquo; Site Sections', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.486866', '2022-04-05', '22:31:47', '2022-04-05 19:31:47', '2022-04-05 19:31:47'),
(89, 2, '127.0.0.1', 'Dashboard &raquo; Products', 'unknown', 'http://127.0.0.1:8000/admin/8/topics', '0.37957788', '2022-04-05', '22:31:57', '2022-04-05 19:31:57', '2022-04-05 19:31:57'),
(90, 2, '127.0.0.1', 'Dashboard &raquo; News', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.39808893', '2022-04-05', '22:32:00', '2022-04-05 19:32:00', '2022-04-05 19:32:00'),
(91, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.473423', '2022-04-05', '22:32:01', '2022-04-05 19:32:01', '2022-04-05 19:32:01'),
(92, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.39048314', '2022-04-05', '22:32:02', '2022-04-05 19:32:02', '2022-04-05 19:32:02'),
(93, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.51148796', '2022-04-05', '22:32:03', '2022-04-05 19:32:03', '2022-04-05 19:32:03'),
(94, 2, '127.0.0.1', 'Dashboard &raquo; Photos', 'unknown', 'http://127.0.0.1:8000/admin/4/topics', '0.39423418', '2022-04-05', '22:32:03', '2022-04-05 19:32:03', '2022-04-05 19:32:03'),
(95, 2, '127.0.0.1', 'Dashboard &raquo; File Manager', 'unknown', 'http://127.0.0.1:8000/admin/file-manager', '0.35007501', '2022-04-05', '22:32:10', '2022-04-05 19:32:10', '2022-04-05 19:32:10'),
(96, 2, '127.0.0.1', 'Dashboard &raquo; Users &amp; Permissions', 'unknown', 'http://127.0.0.1:8000/admin/users', '0.416466', '2022-04-05', '22:32:34', '2022-04-05 19:32:34', '2022-04-05 19:32:34'),
(97, 2, '127.0.0.1', 'Dashboard &raquo; Site Menus', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.35804915', '2022-04-05', '22:32:40', '2022-04-05 19:32:40', '2022-04-05 19:32:40'),
(98, 2, '127.0.0.1', 'Dashboard &raquo; Ad. Banners', 'unknown', 'http://127.0.0.1:8000/admin/banners', '0.40072107', '2022-04-05', '22:32:56', '2022-04-05 19:32:56', '2022-04-05 19:32:56'),
(99, 2, '127.0.0.1', 'Dashboard &raquo; Contacts', 'unknown', 'http://127.0.0.1:8000/admin/contacts', '0.39931107', '2022-04-05', '22:33:09', '2022-04-05 19:33:09', '2022-04-05 19:33:09'),
(100, 2, '127.0.0.1', 'Dashboard &raquo; Contacts', 'unknown', 'http://127.0.0.1:8000/admin/contacts/1', '0.36778593', '2022-04-05', '22:33:13', '2022-04-05 19:33:13', '2022-04-05 19:33:13'),
(101, 2, '127.0.0.1', 'Dashboard &raquo; Inbox', 'unknown', 'http://127.0.0.1:8000/admin/webmails', '0.54152513', '2022-04-05', '22:33:15', '2022-04-05 19:33:15', '2022-04-05 19:33:15'),
(102, 2, '127.0.0.1', 'Dashboard &raquo; Inbox', 'unknown', 'http://127.0.0.1:8000/admin/webmails/create', '0.35959601', '2022-04-05', '22:33:19', '2022-04-05 19:33:19', '2022-04-05 19:33:19'),
(103, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/summernote/dist/summernote.css', '0.54359508', '2022-04-05', '22:33:20', '2022-04-05 19:33:20', '2022-04-05 19:33:20'),
(104, 2, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/summernote/dist/summernote.js', '0.41715789', '2022-04-05', '22:33:21', '2022-04-05 19:33:21', '2022-04-05 19:33:21'),
(105, 2, '127.0.0.1', 'Dashboard &raquo; Calendar', 'unknown', 'http://127.0.0.1:8000/admin/calendar', '0.34345317', '2022-04-05', '22:33:43', '2022-04-05 19:33:43', '2022-04-05 19:33:43'),
(106, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/', '5.22668505', '2022-04-27', '15:18:06', '2022-04-27 12:18:06', '2022-04-27 12:18:06'),
(107, 3, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/install', '0.5766921', '2022-04-27', '15:18:07', '2022-04-27 12:18:07', '2022-04-27 12:18:07'),
(108, 3, '127.0.0.1', 'Dashboard &raquo; Sign in to CONTROL', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.29207397', '2022-04-27', '15:29:02', '2022-04-27 12:29:02', '2022-04-27 12:29:02'),
(109, 3, '127.0.0.1', 'Dashboard &raquo; Site Title', 'unknown', 'http://127.0.0.1:8000/admin', '1.34627199', '2022-04-27', '15:29:06', '2022-04-27 12:29:06', '2022-04-27 12:29:06'),
(110, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.39909506', '2022-04-27', '15:29:07', '2022-04-27 12:29:07', '2022-04-27 12:29:07'),
(111, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.28756213', '2022-04-27', '15:29:07', '2022-04-27 12:29:07', '2022-04-27 12:29:07'),
(112, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.33974886', '2022-04-27', '15:29:08', '2022-04-27 12:29:08', '2022-04-27 12:29:08'),
(113, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.30821109', '2022-04-27', '15:29:09', '2022-04-27 12:29:09', '2022-04-27 12:29:09'),
(114, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.28302097', '2022-04-27', '15:29:09', '2022-04-27 12:29:09', '2022-04-27 12:29:09'),
(115, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.31859016', '2022-04-27', '15:29:10', '2022-04-27 12:29:10', '2022-04-27 12:29:10'),
(116, 3, '127.0.0.1', 'Dashboard &raquo; General Settings', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '1.7139442', '2022-04-27', '15:31:31', '2022-04-27 12:31:31', '2022-04-27 12:31:31'),
(117, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.43267798', '2022-04-27', '15:31:32', '2022-04-27 12:31:32', '2022-04-27 12:31:32'),
(118, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.49728894', '2022-04-27', '15:31:32', '2022-04-27 12:31:32', '2022-04-27 12:31:32'),
(119, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.56013989', '2022-04-27', '15:31:33', '2022-04-27 12:31:33', '2022-04-27 12:31:33'),
(120, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.48532414', '2022-04-27', '15:31:33', '2022-04-27 12:31:33', '2022-04-27 12:31:33'),
(121, 3, '127.0.0.1', 'Dashboard &raquo; Site pages', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.54867911', '2022-04-27', '15:32:28', '2022-04-27 12:32:28', '2022-04-27 12:32:28'),
(122, 3, '127.0.0.1', 'Dashboard &raquo; Home Welcome', 'unknown', 'http://127.0.0.1:8000/admin/1/topics/5/edit', '1.85538292', '2022-04-27', '15:32:33', '2022-04-27 12:32:33', '2022-04-27 12:32:33'),
(123, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.js', '0.41500711', '2022-04-27', '15:32:33', '2022-04-27 12:32:33', '2022-04-27 12:32:33'),
(124, 3, '127.0.0.1', 'Site Title', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.css', '0.38157392', '2022-04-27', '15:32:34', '2022-04-27 12:32:34', '2022-04-27 12:32:34'),
(125, 3, '127.0.0.1', 'Dashboard &raquo; البريد الوارد', 'unknown', 'http://127.0.0.1:8000/admin/webmails', '0.46126199', '2022-04-27', '15:34:03', '2022-04-27 12:34:03', '2022-04-27 12:34:03'),
(126, 3, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.43733692', '2022-04-27', '15:35:46', '2022-04-27 12:35:46', '2022-04-27 12:35:46'),
(127, 3, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/3/edit/1', '0.37713718', '2022-04-27', '15:36:11', '2022-04-27 12:36:11', '2022-04-27 12:36:11'),
(128, 3, '127.0.0.1', 'المدونة', 'unknown', 'http://127.0.0.1:8000/blog', '0.45230699', '2022-04-27', '15:36:24', '2022-04-27 12:36:24', '2022-04-27 12:36:24'),
(129, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/', '2.27320004', '2022-04-28', '23:09:59', '2022-04-28 20:09:59', '2022-04-28 20:09:59'),
(130, 4, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.50995398', '2022-04-28', '23:10:03', '2022-04-28 20:10:03', '2022-04-28 20:10:03'),
(131, 4, '127.0.0.1', 'Dashboard &raquo; اسم الموقع', 'unknown', 'http://127.0.0.1:8000/admin', '1.40117097', '2022-04-28', '23:10:08', '2022-04-28 20:10:08', '2022-04-28 20:10:08'),
(132, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.44095397', '2022-04-28', '23:10:08', '2022-04-28 20:10:08', '2022-04-28 20:10:08'),
(133, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.34873199', '2022-04-28', '23:10:09', '2022-04-28 20:10:09', '2022-04-28 20:10:09'),
(134, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.33417201', '2022-04-28', '23:10:10', '2022-04-28 20:10:10', '2022-04-28 20:10:10'),
(135, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.33171606', '2022-04-28', '23:10:11', '2022-04-28 20:10:11', '2022-04-28 20:10:11'),
(136, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.33259511', '2022-04-28', '23:10:11', '2022-04-28 20:10:11', '2022-04-28 20:10:11'),
(137, 4, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.32003903', '2022-04-28', '23:10:12', '2022-04-28 20:10:12', '2022-04-28 20:10:12'),
(138, 4, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.55161405', '2022-04-28', '23:10:28', '2022-04-28 20:10:28', '2022-04-28 20:10:28'),
(139, 4, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/8/edit', '0.84968686', '2022-04-28', '23:10:33', '2022-04-28 20:10:33', '2022-04-28 20:10:33'),
(140, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/', '1.38785791', '2022-05-04', '22:13:37', '2022-05-04 19:13:37', '2022-05-04 19:13:37'),
(141, 5, '127.0.0.1', 'من نحن', 'unknown', 'http://127.0.0.1:8000/topic/about', '0.69846916', '2022-05-04', '22:14:19', '2022-05-04 19:14:19', '2022-05-04 19:14:19'),
(142, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/home', '0.26348901', '2022-05-04', '22:14:22', '2022-05-04 19:14:22', '2022-05-04 19:14:22'),
(143, 5, '127.0.0.1', 'المدونة', 'unknown', 'http://127.0.0.1:8000/blog', '0.39693999', '2022-05-04', '22:14:26', '2022-05-04 19:14:26', '2022-05-04 19:14:26'),
(144, 5, '127.0.0.1', 'اتصل بنا', 'unknown', 'http://127.0.0.1:8000/contact', '0.46578002', '2022-05-04', '22:14:28', '2022-05-04 19:14:28', '2022-05-04 19:14:28'),
(145, 5, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.21724296', '2022-05-04', '22:20:45', '2022-05-04 19:20:45', '2022-05-04 19:20:45'),
(146, 5, '127.0.0.1', 'Dashboard &raquo; اسم الموقع', 'unknown', 'http://127.0.0.1:8000/admin', '0.33585501', '2022-05-04', '22:20:47', '2022-05-04 19:20:47', '2022-05-04 19:20:47'),
(147, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.292696', '2022-05-04', '22:20:47', '2022-05-04 19:20:47', '2022-05-04 19:20:47'),
(148, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.28730607', '2022-05-04', '22:20:48', '2022-05-04 19:20:48', '2022-05-04 19:20:48'),
(149, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.33039689', '2022-05-04', '22:20:49', '2022-05-04 19:20:49', '2022-05-04 19:20:49'),
(150, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.29859495', '2022-05-04', '22:20:49', '2022-05-04 19:20:49', '2022-05-04 19:20:49'),
(151, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.25246501', '2022-05-04', '22:20:50', '2022-05-04 19:20:50', '2022-05-04 19:20:50'),
(152, 5, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.27194595', '2022-05-04', '22:20:50', '2022-05-04 19:20:50', '2022-05-04 19:20:50'),
(153, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.51038098', '2022-05-06', '21:21:41', '2022-05-06 18:21:41', '2022-05-06 18:21:41'),
(154, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/svg/chevron-double-down.svg', '0.28425598', '2022-05-06', '21:21:41', '2022-05-06 18:21:41', '2022-05-06 18:21:41'),
(155, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.31691098', '2022-05-06', '21:21:42', '2022-05-06 18:21:42', '2022-05-06 18:21:42'),
(156, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.24753499', '2022-05-06', '21:21:57', '2022-05-06 18:21:57', '2022-05-06 18:21:57'),
(157, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.22735286', '2022-05-06', '21:21:58', '2022-05-06 18:21:58', '2022-05-06 18:21:58'),
(158, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.20683002', '2022-05-06', '21:21:58', '2022-05-06 18:21:58', '2022-05-06 18:21:58'),
(159, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.22563601', '2022-05-06', '21:21:58', '2022-05-06 18:21:58', '2022-05-06 18:21:58'),
(160, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.21545291', '2022-05-06', '21:21:58', '2022-05-06 18:21:58', '2022-05-06 18:21:58'),
(161, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.18908787', '2022-05-06', '21:21:59', '2022-05-06 18:21:59', '2022-05-06 18:21:59'),
(162, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.23389792', '2022-05-06', '21:21:59', '2022-05-06 18:21:59', '2022-05-06 18:21:59'),
(163, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.23237395', '2022-05-06', '21:21:59', '2022-05-06 18:21:59', '2022-05-06 18:21:59'),
(164, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.23986006', '2022-05-06', '21:21:59', '2022-05-06 18:21:59', '2022-05-06 18:21:59'),
(165, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.20508599', '2022-05-06', '21:22:00', '2022-05-06 18:22:00', '2022-05-06 18:22:00'),
(166, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.20029402', '2022-05-06', '21:22:00', '2022-05-06 18:22:00', '2022-05-06 18:22:00'),
(167, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.22291303', '2022-05-06', '21:22:00', '2022-05-06 18:22:00', '2022-05-06 18:22:00'),
(168, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.24140596', '2022-05-06', '21:22:00', '2022-05-06 18:22:00', '2022-05-06 18:22:00'),
(169, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.31387496', '2022-05-06', '21:22:01', '2022-05-06 18:22:01', '2022-05-06 18:22:01'),
(170, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.2138412', '2022-05-06', '21:22:01', '2022-05-06 18:22:01', '2022-05-06 18:22:01'),
(171, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.18414807', '2022-05-06', '21:22:01', '2022-05-06 18:22:01', '2022-05-06 18:22:01'),
(172, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.18469501', '2022-05-06', '21:22:01', '2022-05-06 18:22:01', '2022-05-06 18:22:01'),
(173, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.19215703', '2022-05-06', '21:22:02', '2022-05-06 18:22:02', '2022-05-06 18:22:02'),
(174, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.24950695', '2022-05-06', '21:24:18', '2022-05-06 18:24:18', '2022-05-06 18:24:18'),
(175, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.28369498', '2022-05-06', '21:24:18', '2022-05-06 18:24:18', '2022-05-06 18:24:18'),
(176, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.22563601', '2022-05-06', '21:24:19', '2022-05-06 18:24:19', '2022-05-06 18:24:19'),
(177, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.21101809', '2022-05-06', '21:24:19', '2022-05-06 18:24:19', '2022-05-06 18:24:19'),
(178, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.31500506', '2022-05-06', '21:26:27', '2022-05-06 18:26:27', '2022-05-06 18:26:27'),
(179, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/', '0.56357789', '2022-05-06', '21:30:28', '2022-05-06 18:30:28', '2022-05-06 18:30:28'),
(180, 6, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/icon_slider_arrow.294984c9.svg', '0.37338686', '2022-05-06', '21:33:38', '2022-05-06 18:33:38', '2022-05-06 18:33:38'),
(181, 6, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.362885', '2022-05-06', '22:09:43', '2022-05-06 19:09:43', '2022-05-06 19:09:43'),
(182, 6, '127.0.0.1', 'Dashboard &raquo; اسم الموقع', 'unknown', 'http://127.0.0.1:8000/admin', '1.1330359', '2022-05-06', '22:09:47', '2022-05-06 19:09:47', '2022-05-06 19:09:47'),
(183, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.28210998', '2022-05-06', '22:09:48', '2022-05-06 19:09:48', '2022-05-06 19:09:48'),
(184, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.25658107', '2022-05-06', '22:09:49', '2022-05-06 19:09:49', '2022-05-06 19:09:49'),
(185, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.276963', '2022-05-06', '22:09:49', '2022-05-06 19:09:49', '2022-05-06 19:09:49'),
(186, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.24583697', '2022-05-06', '22:09:50', '2022-05-06 19:09:50', '2022-05-06 19:09:50'),
(187, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.23743796', '2022-05-06', '22:09:51', '2022-05-06 19:09:51', '2022-05-06 19:09:51'),
(188, 6, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.38364196', '2022-05-06', '22:09:54', '2022-05-06 19:09:54', '2022-05-06 19:09:54'),
(189, 6, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.52269793', '2022-05-06', '22:10:18', '2022-05-06 19:10:18', '2022-05-06 19:10:18'),
(190, 6, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/4/edit/1', '0.37678194', '2022-05-06', '22:10:34', '2022-05-06 19:10:34', '2022-05-06 19:10:34'),
(191, 6, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/3/edit/1', '0.26711893', '2022-05-06', '22:10:40', '2022-05-06 19:10:40', '2022-05-06 19:10:40'),
(192, 6, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/6/edit/1', '0.242836', '2022-05-06', '22:10:44', '2022-05-06 19:10:44', '2022-05-06 19:10:44'),
(193, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/', '0.90664196', '2022-05-07', '20:30:10', '2022-05-07 17:30:10', '2022-05-07 17:30:10'),
(194, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.37697291', '2022-05-07', '20:30:11', '2022-05-07 17:30:11', '2022-05-07 17:30:11'),
(195, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.36556506', '2022-05-07', '20:30:11', '2022-05-07 17:30:11', '2022-05-07 17:30:11'),
(196, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.57151794', '2022-05-07', '20:30:12', '2022-05-07 17:30:12', '2022-05-07 17:30:12'),
(197, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.28778696', '2022-05-07', '20:30:13', '2022-05-07 17:30:13', '2022-05-07 17:30:13'),
(198, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.28815603', '2022-05-07', '20:30:13', '2022-05-07 17:30:13', '2022-05-07 17:30:13'),
(199, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.23251987', '2022-05-07', '20:30:13', '2022-05-07 17:30:13', '2022-05-07 17:30:13'),
(200, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.26525784', '2022-05-07', '20:30:14', '2022-05-07 17:30:14', '2022-05-07 17:30:14'),
(201, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.23440099', '2022-05-07', '20:30:14', '2022-05-07 17:30:14', '2022-05-07 17:30:14'),
(202, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.23281503', '2022-05-07', '20:30:15', '2022-05-07 17:30:15', '2022-05-07 17:30:15'),
(203, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.20003414', '2022-05-07', '20:30:15', '2022-05-07 17:30:15', '2022-05-07 17:30:15'),
(204, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.24779892', '2022-05-07', '20:30:15', '2022-05-07 17:30:15', '2022-05-07 17:30:15'),
(205, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.29598689', '2022-05-07', '20:30:16', '2022-05-07 17:30:16', '2022-05-07 17:30:16'),
(206, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.22636104', '2022-05-07', '20:30:16', '2022-05-07 17:30:16', '2022-05-07 17:30:16'),
(207, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.21996498', '2022-05-07', '20:30:16', '2022-05-07 17:30:16', '2022-05-07 17:30:16'),
(208, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.22533798', '2022-05-07', '20:30:16', '2022-05-07 17:30:16', '2022-05-07 17:30:16'),
(209, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.23177004', '2022-05-07', '20:30:17', '2022-05-07 17:30:17', '2022-05-07 17:30:17'),
(210, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.19887996', '2022-05-07', '20:30:17', '2022-05-07 17:30:17', '2022-05-07 17:30:17'),
(211, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.20031381', '2022-05-07', '20:30:17', '2022-05-07 17:30:17', '2022-05-07 17:30:17'),
(212, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.20414805', '2022-05-07', '20:30:17', '2022-05-07 17:30:17', '2022-05-07 17:30:17'),
(213, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.1982789', '2022-05-07', '20:30:18', '2022-05-07 17:30:18', '2022-05-07 17:30:18'),
(214, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.1996572', '2022-05-07', '20:30:18', '2022-05-07 17:30:18', '2022-05-07 17:30:18'),
(215, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.20748782', '2022-05-07', '20:30:18', '2022-05-07 17:30:18', '2022-05-07 17:30:18'),
(216, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.20724201', '2022-05-07', '20:30:18', '2022-05-07 17:30:18', '2022-05-07 17:30:18'),
(217, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.24559689', '2022-05-07', '20:30:18', '2022-05-07 17:30:18', '2022-05-07 17:30:18'),
(218, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.2202611', '2022-05-07', '20:30:19', '2022-05-07 17:30:19', '2022-05-07 17:30:19'),
(219, 7, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.29897308', '2022-05-07', '20:30:43', '2022-05-07 17:30:43', '2022-05-07 17:30:43'),
(220, 7, '127.0.0.1', 'Dashboard &raquo; اسم الموقع', 'unknown', 'http://127.0.0.1:8000/admin', '0.62781787', '2022-05-07', '20:30:45', '2022-05-07 17:30:45', '2022-05-07 17:30:45'),
(221, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.27287698', '2022-05-07', '20:30:46', '2022-05-07 17:30:46', '2022-05-07 17:30:46'),
(222, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.26507521', '2022-05-07', '20:30:46', '2022-05-07 17:30:46', '2022-05-07 17:30:46'),
(223, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.26123309', '2022-05-07', '20:30:47', '2022-05-07 17:30:47', '2022-05-07 17:30:47'),
(224, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.264256', '2022-05-07', '20:30:47', '2022-05-07 17:30:47', '2022-05-07 17:30:47'),
(225, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.27024102', '2022-05-07', '20:30:48', '2022-05-07 17:30:48', '2022-05-07 17:30:48'),
(226, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.25084209', '2022-05-07', '20:30:49', '2022-05-07 17:30:49', '2022-05-07 17:30:49'),
(227, 7, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.27403092', '2022-05-07', '20:31:02', '2022-05-07 17:31:02', '2022-05-07 17:31:02'),
(228, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.26550794', '2022-05-07', '20:31:03', '2022-05-07 17:31:03', '2022-05-07 17:31:03'),
(229, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.27358508', '2022-05-07', '20:31:04', '2022-05-07 17:31:04', '2022-05-07 17:31:04');
INSERT INTO `iziengdb_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(230, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.24641204', '2022-05-07', '20:31:04', '2022-05-07 17:31:04', '2022-05-07 17:31:04'),
(231, 7, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics/create', '0.58601403', '2022-05-07', '20:31:18', '2022-05-07 17:31:18', '2022-05-07 17:31:18'),
(232, 7, '127.0.0.1', 'Dashboard &raquo; العملاء', 'unknown', 'http://127.0.0.1:8000/admin/9/topics', '0.24794912', '2022-05-07', '20:34:48', '2022-05-07 17:34:48', '2022-05-07 17:34:48'),
(233, 7, '127.0.0.1', 'Dashboard &raquo; العملاء', 'unknown', 'http://127.0.0.1:8000/admin/9/topics/create', '0.26226711', '2022-05-07', '20:34:51', '2022-05-07 17:34:51', '2022-05-07 17:34:51'),
(234, 7, '127.0.0.1', 'Dashboard &raquo; الخدمات', 'unknown', 'http://127.0.0.1:8000/admin/2/topics', '0.31355', '2022-05-07', '20:35:06', '2022-05-07 17:35:06', '2022-05-07 17:35:06'),
(235, 7, '127.0.0.1', 'Dashboard &raquo; الخدمات', 'unknown', 'http://127.0.0.1:8000/admin/2/topics/create', '0.25616097', '2022-05-07', '20:35:09', '2022-05-07 17:35:09', '2022-05-07 17:35:09'),
(236, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.28552294', '2022-05-07', '20:35:25', '2022-05-07 17:35:25', '2022-05-07 17:35:25'),
(237, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/create', '0.45121098', '2022-05-07', '20:35:41', '2022-05-07 17:35:41', '2022-05-07 17:35:41'),
(238, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/10/edit', '0.27018309', '2022-05-07', '20:37:47', '2022-05-07 17:37:47', '2022-05-07 17:37:47'),
(239, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.29892898', '2022-05-07', '20:37:51', '2022-05-07 17:37:51', '2022-05-07 17:37:51'),
(240, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.2368741', '2022-05-07', '20:37:52', '2022-05-07 17:37:52', '2022-05-07 17:37:52'),
(241, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.24913597', '2022-05-07', '20:37:52', '2022-05-07 17:37:52', '2022-05-07 17:37:52'),
(242, 7, '127.0.0.1', 'اسم الموقع', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.25124097', '2022-05-07', '20:37:52', '2022-05-07 17:37:52', '2022-05-07 17:37:52'),
(243, 7, '127.0.0.1', 'Dashboard &raquo; المزيد عن IZIENG', 'unknown', 'http://127.0.0.1:8000/admin/10/topics', '0.29089403', '2022-05-07', '20:39:21', '2022-05-07 17:39:21', '2022-05-07 17:39:21'),
(244, 7, '127.0.0.1', 'Dashboard &raquo; المزيد عن IZIENG', 'unknown', 'http://127.0.0.1:8000/admin/10/topics/create', '0.23826003', '2022-05-07', '20:39:23', '2022-05-07 17:39:23', '2022-05-07 17:39:23'),
(245, 7, '127.0.0.1', 'Dashboard &raquo; منظومة IZIENG', 'unknown', 'http://127.0.0.1:8000/admin/10/topics/6/edit', '0.26785707', '2022-05-07', '20:49:03', '2022-05-07 17:49:03', '2022-05-07 17:49:03'),
(246, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات البنرات', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/banners', '0.34240603', '2022-05-07', '20:56:57', '2022-05-07 17:56:57', '2022-05-07 17:56:57'),
(247, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات البنرات', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/banners/2/edit', '0.41691685', '2022-05-07', '20:57:00', '2022-05-07 17:57:00', '2022-05-07 17:57:00'),
(248, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات البنرات', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/banners/create', '0.32186389', '2022-05-07', '20:57:38', '2022-05-07 17:57:38', '2022-05-07 17:57:38'),
(249, 7, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners', '0.41683507', '2022-05-07', '20:57:56', '2022-05-07 17:57:56', '2022-05-07 17:57:56'),
(250, 7, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners/1/edit', '0.38486195', '2022-05-07', '20:57:59', '2022-05-07 17:57:59', '2022-05-07 17:57:59'),
(251, 7, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners/6/edit', '1.40546799', '2022-05-07', '20:58:08', '2022-05-07 17:58:08', '2022-05-07 17:58:08'),
(252, 7, '127.0.0.1', 'Dashboard &raquo; إعدادات البنرات', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/banners/4/edit', '0.24188685', '2022-05-07', '20:59:36', '2022-05-07 17:59:36', '2022-05-07 17:59:36'),
(253, 7, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners/create/4', '0.35421014', '2022-05-07', '21:01:32', '2022-05-07 18:01:33', '2022-05-07 18:01:33'),
(254, 7, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.814044', '2022-05-07', '21:19:06', '2022-05-07 18:19:06', '2022-05-07 18:19:06'),
(255, 7, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/icon_slider_arrow.294984c9.svg', '0.22376204', '2022-05-07', '21:23:16', '2022-05-07 18:23:16', '2022-05-07 18:23:16'),
(256, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.30042791', '2022-05-07', '21:25:34', '2022-05-07 18:25:34', '2022-05-07 18:25:34'),
(257, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/12/edit/1', '0.22867894', '2022-05-07', '21:25:39', '2022-05-07 18:25:39', '2022-05-07 18:25:39'),
(258, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/4/edit/1', '0.23734498', '2022-05-07', '21:25:46', '2022-05-07 18:25:46', '2022-05-07 18:25:46'),
(259, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/3/edit/1', '0.277385', '2022-05-07', '21:26:00', '2022-05-07 18:26:00', '2022-05-07 18:26:00'),
(260, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/1', '0.31195998', '2022-05-07', '21:29:46', '2022-05-07 18:29:46', '2022-05-07 18:29:46'),
(261, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/create/1', '0.4131701', '2022-05-07', '21:30:46', '2022-05-07 18:30:46', '2022-05-07 18:30:46'),
(262, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/19/edit/1', '0.272789', '2022-05-07', '21:31:03', '2022-05-07 18:31:03', '2022-05-07 18:31:03'),
(263, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/7/edit/1', '0.28118992', '2022-05-07', '21:31:16', '2022-05-07 18:31:16', '2022-05-07 18:31:16'),
(264, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/20/edit/1', '0.24810505', '2022-05-07', '21:32:12', '2022-05-07 18:32:12', '2022-05-07 18:32:12'),
(265, 7, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/5/edit/1', '0.30647802', '2022-05-07', '21:32:45', '2022-05-07 18:32:45', '2022-05-07 18:32:45'),
(266, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '0.53968692', '2022-05-08', '19:31:42', '2022-05-08 16:31:42', '2022-05-08 16:31:42'),
(267, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.33858895', '2022-05-08', '19:31:42', '2022-05-08 16:31:42', '2022-05-08 16:31:42'),
(268, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.41997409', '2022-05-08', '19:31:43', '2022-05-08 16:31:43', '2022-05-08 16:31:43'),
(269, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.45653391', '2022-05-08', '19:31:43', '2022-05-08 16:31:43', '2022-05-08 16:31:43'),
(270, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.38067794', '2022-05-08', '19:31:44', '2022-05-08 16:31:44', '2022-05-08 16:31:44'),
(271, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.26848292', '2022-05-08', '19:31:44', '2022-05-08 16:31:44', '2022-05-08 16:31:44'),
(272, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.29595208', '2022-05-08', '19:31:45', '2022-05-08 16:31:45', '2022-05-08 16:31:45'),
(273, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.24484897', '2022-05-08', '19:31:46', '2022-05-08 16:31:46', '2022-05-08 16:31:46'),
(274, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.25683212', '2022-05-08', '19:31:46', '2022-05-08 16:31:46', '2022-05-08 16:31:46'),
(275, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.23665094', '2022-05-08', '19:31:46', '2022-05-08 16:31:46', '2022-05-08 16:31:46'),
(276, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.22728896', '2022-05-08', '19:31:47', '2022-05-08 16:31:47', '2022-05-08 16:31:47'),
(277, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.22445297', '2022-05-08', '19:31:47', '2022-05-08 16:31:47', '2022-05-08 16:31:47'),
(278, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.24783397', '2022-05-08', '19:31:47', '2022-05-08 16:31:47', '2022-05-08 16:31:47'),
(279, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.21487999', '2022-05-08', '19:31:47', '2022-05-08 16:31:47', '2022-05-08 16:31:47'),
(280, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.22081804', '2022-05-08', '19:31:48', '2022-05-08 16:31:48', '2022-05-08 16:31:48'),
(281, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.22033405', '2022-05-08', '19:31:48', '2022-05-08 16:31:48', '2022-05-08 16:31:48'),
(282, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.23027396', '2022-05-08', '19:31:48', '2022-05-08 16:31:48', '2022-05-08 16:31:48'),
(283, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.24807501', '2022-05-08', '19:31:48', '2022-05-08 16:31:48', '2022-05-08 16:31:48'),
(284, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.20748687', '2022-05-08', '19:31:49', '2022-05-08 16:31:49', '2022-05-08 16:31:49'),
(285, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.25129986', '2022-05-08', '19:31:49', '2022-05-08 16:31:49', '2022-05-08 16:31:49'),
(286, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.19249392', '2022-05-08', '19:31:49', '2022-05-08 16:31:49', '2022-05-08 16:31:49'),
(287, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.16993117', '2022-05-08', '19:31:49', '2022-05-08 16:31:49', '2022-05-08 16:31:49'),
(288, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.17160511', '2022-05-08', '19:31:50', '2022-05-08 16:31:50', '2022-05-08 16:31:50'),
(289, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.16759205', '2022-05-08', '19:31:50', '2022-05-08 16:31:50', '2022-05-08 16:31:50'),
(290, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.16954899', '2022-05-08', '19:31:50', '2022-05-08 16:31:50', '2022-05-08 16:31:50'),
(291, 8, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.17368698', '2022-05-08', '19:31:50', '2022-05-08 16:31:50', '2022-05-08 16:31:50'),
(292, 8, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.45903182', '2022-05-08', '19:35:55', '2022-05-08 16:35:55', '2022-05-08 16:35:55'),
(293, 8, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin', '1.14499712', '2022-05-08', '19:36:00', '2022-05-08 16:36:00', '2022-05-08 16:36:00'),
(294, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.30034304', '2022-05-08', '19:36:01', '2022-05-08 16:36:01', '2022-05-08 16:36:01'),
(295, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.25981808', '2022-05-08', '19:36:01', '2022-05-08 16:36:01', '2022-05-08 16:36:01'),
(296, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.24559999', '2022-05-08', '19:36:02', '2022-05-08 16:36:02', '2022-05-08 16:36:02'),
(297, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.24150109', '2022-05-08', '19:36:02', '2022-05-08 16:36:02', '2022-05-08 16:36:02'),
(298, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.24351406', '2022-05-08', '19:36:03', '2022-05-08 16:36:03', '2022-05-08 16:36:03'),
(299, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.26117396', '2022-05-08', '19:36:03', '2022-05-08 16:36:03', '2022-05-08 16:36:03'),
(300, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.47904086', '2022-05-08', '19:36:23', '2022-05-08 16:36:23', '2022-05-08 16:36:23'),
(301, 8, '127.0.0.1', 'Dashboard &raquo; العملاء', 'unknown', 'http://127.0.0.1:8000/admin/9/topics', '0.56656098', '2022-05-08', '19:40:41', '2022-05-08 16:40:41', '2022-05-08 16:40:41'),
(302, 8, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.41277099', '2022-05-08', '19:40:49', '2022-05-08 16:40:49', '2022-05-08 16:40:49'),
(303, 8, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/9/edit', '0.76635909', '2022-05-08', '19:40:53', '2022-05-08 16:40:53', '2022-05-08 16:40:53'),
(304, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/create/1', '0.37867808', '2022-05-08', '19:52:09', '2022-05-08 16:52:09', '2022-05-08 16:52:09'),
(305, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/21/edit/1', '0.37536407', '2022-05-08', '19:52:34', '2022-05-08 16:52:34', '2022-05-08 16:52:34'),
(306, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/1', '0.2816062', '2022-05-08', '19:52:38', '2022-05-08 16:52:38', '2022-05-08 16:52:38'),
(307, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/22/edit/1', '0.23989701', '2022-05-08', '19:53:41', '2022-05-08 16:53:41', '2022-05-08 16:53:41'),
(308, 8, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/7/edit', '0.26114392', '2022-05-08', '19:54:21', '2022-05-08 16:54:21', '2022-05-08 16:54:21'),
(309, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/5/edit/1', '0.23673606', '2022-05-08', '19:56:50', '2022-05-08 16:56:50', '2022-05-08 16:56:50'),
(310, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/6/edit/1', '0.28079796', '2022-05-08', '19:57:59', '2022-05-08 16:57:59', '2022-05-08 16:57:59'),
(311, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/8/edit/1', '0.35428095', '2022-05-08', '19:58:42', '2022-05-08 16:58:42', '2022-05-08 16:58:42'),
(312, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/23/edit/1', '0.25739789', '2022-05-08', '19:59:44', '2022-05-08 16:59:44', '2022-05-08 16:59:44'),
(313, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/24/edit/1', '0.31273699', '2022-05-08', '20:01:02', '2022-05-08 17:01:02', '2022-05-08 17:01:02'),
(314, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/25/edit/1', '0.35439301', '2022-05-08', '20:01:21', '2022-05-08 17:01:21', '2022-05-08 17:01:21'),
(315, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/26/edit/1', '0.34978414', '2022-05-08', '20:01:39', '2022-05-08 17:01:39', '2022-05-08 17:01:39'),
(316, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/27/edit/1', '0.51057005', '2022-05-08', '20:01:54', '2022-05-08 17:01:54', '2022-05-08 17:01:54'),
(317, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/28/edit/1', '0.29093003', '2022-05-08', '20:02:09', '2022-05-08 17:02:09', '2022-05-08 17:02:09'),
(318, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/29/edit/1', '0.33888984', '2022-05-08', '20:02:23', '2022-05-08 17:02:23', '2022-05-08 17:02:23'),
(319, 8, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/30/edit/1', '0.24781084', '2022-05-08', '20:07:42', '2022-05-08 17:07:42', '2022-05-08 17:07:42'),
(320, 8, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/referrer', '0.481848', '2022-05-08', '20:24:17', '2022-05-08 17:24:17', '2022-05-08 17:24:17'),
(321, 8, '127.0.0.1', 'Dashboard &raquo; استعلام عن IP', 'unknown', 'http://127.0.0.1:8000/admin/ip', '0.4013412', '2022-05-08', '20:24:22', '2022-05-08 17:24:22', '2022-05-08 17:24:22'),
(322, 8, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/os', '0.24850988', '2022-05-08', '20:24:27', '2022-05-08 17:24:27', '2022-05-08 17:24:27'),
(323, 8, '127.0.0.1', 'Dashboard &raquo; جهات الاتصال', 'unknown', 'http://127.0.0.1:8000/admin/contacts', '0.50130582', '2022-05-08', '20:24:30', '2022-05-08 17:24:30', '2022-05-08 17:24:30'),
(324, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.25153804', '2022-05-08', '20:24:30', '2022-05-08 17:24:30', '2022-05-08 17:24:30'),
(325, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.34084892', '2022-05-08', '20:24:31', '2022-05-08 17:24:31', '2022-05-08 17:24:31'),
(326, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.25239706', '2022-05-08', '20:24:31', '2022-05-08 17:24:31', '2022-05-08 17:24:31'),
(327, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.22946405', '2022-05-08', '20:24:31', '2022-05-08 17:24:31', '2022-05-08 17:24:31'),
(328, 8, '127.0.0.1', 'Dashboard &raquo; البريد الوارد', 'unknown', 'http://127.0.0.1:8000/admin/webmails', '0.40462112', '2022-05-08', '20:24:32', '2022-05-08 17:24:32', '2022-05-08 17:24:32'),
(329, 8, '127.0.0.1', 'Dashboard &raquo; الأجندة والملاحظات', 'unknown', 'http://127.0.0.1:8000/admin/calendar', '0.51478887', '2022-05-08', '20:24:34', '2022-05-08 17:24:34', '2022-05-08 17:24:34'),
(330, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.25808501', '2022-05-08', '20:24:35', '2022-05-08 17:24:35', '2022-05-08 17:24:35'),
(331, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.27493596', '2022-05-08', '20:24:35', '2022-05-08 17:24:35', '2022-05-08 17:24:35'),
(332, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.23493314', '2022-05-08', '20:24:35', '2022-05-08 17:24:35', '2022-05-08 17:24:35'),
(333, 8, '127.0.0.1', 'Dashboard &raquo; صفحات الموقع', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.23387003', '2022-05-08', '20:24:36', '2022-05-08 17:24:36', '2022-05-08 17:24:36'),
(334, 8, '127.0.0.1', 'Dashboard &raquo; الصفحة الرئيسية', 'unknown', 'http://127.0.0.1:8000/admin/1/topics/5/edit', '1.46596313', '2022-05-08', '20:25:00', '2022-05-08 17:25:00', '2022-05-08 17:25:00'),
(335, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.js', '0.28148413', '2022-05-08', '20:25:01', '2022-05-08 17:25:01', '2022-05-08 17:25:01'),
(336, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.css', '0.24523592', '2022-05-08', '20:25:01', '2022-05-08 17:25:01', '2022-05-08 17:25:01'),
(337, 8, '127.0.0.1', 'Dashboard &raquo; الخدمات', 'unknown', 'http://127.0.0.1:8000/admin/2/topics', '0.31449413', '2022-05-08', '20:25:28', '2022-05-08 17:25:28', '2022-05-08 17:25:28'),
(338, 8, '127.0.0.1', 'Dashboard &raquo; الخدمات', 'unknown', 'http://127.0.0.1:8000/admin/2/topics/create', '0.53878903', '2022-05-08', '20:25:30', '2022-05-08 17:25:30', '2022-05-08 17:25:30'),
(339, 8, '127.0.0.1', 'Dashboard &raquo; مدير الملفات', 'unknown', 'http://127.0.0.1:8000/admin/file-manager', '0.27599406', '2022-05-08', '20:25:39', '2022-05-08 17:25:39', '2022-05-08 17:25:39'),
(340, 8, '127.0.0.1', 'Dashboard &raquo; المستخدمين والصلاحيات', 'unknown', 'http://127.0.0.1:8000/admin/users', '0.44752908', '2022-05-08', '20:26:04', '2022-05-08 17:26:04', '2022-05-08 17:26:04'),
(341, 8, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/create', '0.46279407', '2022-05-08', '20:26:35', '2022-05-08 17:26:35', '2022-05-08 17:26:35'),
(342, 8, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.78242898', '2022-05-08', '20:26:51', '2022-05-08 17:26:51', '2022-05-08 17:26:51'),
(343, 8, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '2.617805', '2022-05-08', '20:27:54', '2022-05-08 17:27:54', '2022-05-08 17:27:54'),
(344, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/summernote/dist/summernote.css', '0.2950511', '2022-05-08', '20:30:10', '2022-05-08 17:30:10', '2022-05-08 17:30:10'),
(345, 8, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/summernote/dist/summernote.js', '0.26859808', '2022-05-08', '20:30:10', '2022-05-08 17:30:10', '2022-05-08 17:30:10'),
(346, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '2.79031801', '2022-05-10', '19:12:03', '2022-05-10 16:12:03', '2022-05-10 16:12:03'),
(347, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.61486983', '2022-05-10', '19:12:05', '2022-05-10 16:12:05', '2022-05-10 16:12:05'),
(348, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.48902202', '2022-05-10', '19:12:05', '2022-05-10 16:12:05', '2022-05-10 16:12:05'),
(349, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.42257595', '2022-05-10', '19:12:06', '2022-05-10 16:12:06', '2022-05-10 16:12:06'),
(350, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.42992401', '2022-05-10', '19:12:06', '2022-05-10 16:12:06', '2022-05-10 16:12:06'),
(351, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.52530909', '2022-05-10', '19:12:07', '2022-05-10 16:12:07', '2022-05-10 16:12:07'),
(352, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.49378395', '2022-05-10', '19:12:07', '2022-05-10 16:12:07', '2022-05-10 16:12:07'),
(353, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.43837404', '2022-05-10', '19:12:09', '2022-05-10 16:12:09', '2022-05-10 16:12:09'),
(354, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.42025208', '2022-05-10', '19:12:10', '2022-05-10 16:12:10', '2022-05-10 16:12:10'),
(355, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.39539194', '2022-05-10', '19:12:10', '2022-05-10 16:12:10', '2022-05-10 16:12:10'),
(356, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.29600191', '2022-05-10', '19:12:10', '2022-05-10 16:12:10', '2022-05-10 16:12:10'),
(357, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.66641498', '2022-05-10', '19:12:11', '2022-05-10 16:12:11', '2022-05-10 16:12:11'),
(358, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.50763893', '2022-05-10', '19:12:12', '2022-05-10 16:12:12', '2022-05-10 16:12:12'),
(359, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.29904914', '2022-05-10', '19:12:12', '2022-05-10 16:12:12', '2022-05-10 16:12:12'),
(360, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.29931092', '2022-05-10', '19:12:12', '2022-05-10 16:12:12', '2022-05-10 16:12:12'),
(361, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.32686496', '2022-05-10', '19:12:13', '2022-05-10 16:12:13', '2022-05-10 16:12:13'),
(362, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.30721617', '2022-05-10', '19:12:13', '2022-05-10 16:12:13', '2022-05-10 16:12:13'),
(363, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.35501409', '2022-05-10', '19:12:13', '2022-05-10 16:12:13', '2022-05-10 16:12:13'),
(364, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.31963015', '2022-05-10', '19:12:14', '2022-05-10 16:12:14', '2022-05-10 16:12:14'),
(365, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.3477459', '2022-05-10', '19:12:14', '2022-05-10 16:12:14', '2022-05-10 16:12:14'),
(366, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.35724092', '2022-05-10', '19:12:15', '2022-05-10 16:12:15', '2022-05-10 16:12:15'),
(367, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.26585388', '2022-05-10', '19:12:15', '2022-05-10 16:12:15', '2022-05-10 16:12:15'),
(368, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.27099514', '2022-05-10', '19:12:15', '2022-05-10 16:12:15', '2022-05-10 16:12:15'),
(369, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.29960489', '2022-05-10', '19:12:15', '2022-05-10 16:12:15', '2022-05-10 16:12:15'),
(370, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.34026194', '2022-05-10', '19:12:16', '2022-05-10 16:12:16', '2022-05-10 16:12:16'),
(371, 9, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.32849503', '2022-05-10', '19:12:16', '2022-05-10 16:12:16', '2022-05-10 16:12:16'),
(372, 9, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.26377702', '2022-05-10', '19:14:11', '2022-05-10 16:14:11', '2022-05-10 16:14:11'),
(373, 9, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin', '0.3424499', '2022-05-10', '19:14:15', '2022-05-10 16:14:15', '2022-05-10 16:14:15'),
(374, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.26876211', '2022-05-10', '19:14:15', '2022-05-10 16:14:15', '2022-05-10 16:14:15'),
(375, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.27282405', '2022-05-10', '19:14:16', '2022-05-10 16:14:16', '2022-05-10 16:14:16'),
(376, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.254987', '2022-05-10', '19:14:17', '2022-05-10 16:14:17', '2022-05-10 16:14:17'),
(377, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.23648286', '2022-05-10', '19:14:17', '2022-05-10 16:14:17', '2022-05-10 16:14:17'),
(378, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.23471808', '2022-05-10', '19:14:18', '2022-05-10 16:14:18', '2022-05-10 16:14:18'),
(379, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.24191499', '2022-05-10', '19:14:18', '2022-05-10 16:14:18', '2022-05-10 16:14:18'),
(380, 9, '127.0.0.1', 'Dashboard &raquo; مدير الملفات', 'unknown', 'http://127.0.0.1:8000/admin/file-manager', '0.2460041', '2022-05-10', '19:14:49', '2022-05-10 16:14:49', '2022-05-10 16:14:49'),
(381, 9, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.40182996', '2022-05-10', '19:14:53', '2022-05-10 16:14:53', '2022-05-10 16:14:53'),
(382, 9, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners', '0.40918183', '2022-05-10', '19:17:42', '2022-05-10 16:17:42', '2022-05-10 16:17:42'),
(383, 9, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners/create/4', '0.2715528', '2022-05-10', '19:17:45', '2022-05-10 16:17:45', '2022-05-10 16:17:45'),
(384, 9, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.30999684', '2022-05-10', '19:19:35', '2022-05-10 16:19:35', '2022-05-10 16:19:35'),
(385, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.46017194', '2022-05-10', '19:19:36', '2022-05-10 16:19:36', '2022-05-10 16:19:36'),
(386, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.36190891', '2022-05-10', '19:19:37', '2022-05-10 16:19:37', '2022-05-10 16:19:37'),
(387, 9, '127.0.0.1', 'Dashboard &raquo; مقالات', 'unknown', 'http://127.0.0.1:8000/admin/7/topics', '0.38832307', '2022-05-10', '19:19:37', '2022-05-10 16:19:37', '2022-05-10 16:19:37'),
(388, 9, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.38351083', '2022-05-10', '19:19:37', '2022-05-10 16:19:37', '2022-05-10 16:19:37'),
(389, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '0.297436', '2022-05-12', '14:07:48', '2022-05-12 11:07:48', '2022-05-12 11:07:48'),
(390, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.02999496', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(391, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.02142692', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(392, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.0213089', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(393, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.02135205', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(394, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.0210731', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(395, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.02955079', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(396, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.03017402', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(397, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.0226481', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(398, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.02749085', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(399, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.02592301', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(400, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.02402592', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(401, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.02201319', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(402, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.02360702', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(403, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.02080703', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(404, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.02565718', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(405, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.02084398', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(406, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.02083492', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(407, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.02382803', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(408, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.03270483', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(409, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.02137589', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(410, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.02156496', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(411, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.02350688', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(412, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.02322316', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(413, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.02108312', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(414, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.02031398', '2022-05-12', '14:07:49', '2022-05-12 11:07:49', '2022-05-12 11:07:49'),
(415, 10, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/icon_slider_arrow.294984c9.svg', '0.03544497', '2022-05-12', '14:08:08', '2022-05-12 11:08:08', '2022-05-12 11:08:08'),
(416, 10, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.02491403', '2022-05-12', '14:08:41', '2022-05-12 11:08:41', '2022-05-12 11:08:41'),
(417, 10, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin', '0.05670691', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(418, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.04922199', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(419, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.03860497', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(420, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.03075695', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(421, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.03102303', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(422, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.02659512', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(423, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.02930593', '2022-05-12', '15:53:10', '2022-05-12 12:53:10', '2022-05-12 12:53:10'),
(424, 10, '127.0.0.1', 'Dashboard &raquo; صفحات الموقع', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.061584', '2022-05-12', '15:53:19', '2022-05-12 12:53:19', '2022-05-12 12:53:19'),
(425, 10, '127.0.0.1', 'Dashboard &raquo; الخدمات', 'unknown', 'http://127.0.0.1:8000/admin/2/topics', '0.04120803', '2022-05-12', '15:53:24', '2022-05-12 12:53:24', '2022-05-12 12:53:24'),
(426, 10, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.0298419', '2022-05-12', '15:53:26', '2022-05-12 12:53:26', '2022-05-12 12:53:26'),
(427, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.0393281', '2022-05-12', '15:53:26', '2022-05-12 12:53:26', '2022-05-12 12:53:26'),
(428, 10, '127.0.0.1', 'Dashboard &raquo; الصور', 'unknown', 'http://127.0.0.1:8000/admin/4/topics', '0.03012991', '2022-05-12', '15:53:29', '2022-05-12 12:53:29', '2022-05-12 12:53:29'),
(429, 10, '127.0.0.1', 'Dashboard &raquo; مقالات', 'unknown', 'http://127.0.0.1:8000/admin/7/topics', '0.02969503', '2022-05-12', '15:53:36', '2022-05-12 12:53:36', '2022-05-12 12:53:36'),
(430, 10, '127.0.0.1', 'Dashboard &raquo; الطاقم', 'unknown', 'http://127.0.0.1:8000/admin/9/topics', '0.03187585', '2022-05-12', '15:53:39', '2022-05-12 12:53:39', '2022-05-12 12:53:39'),
(431, 10, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners', '0.04232192', '2022-05-12', '15:53:42', '2022-05-12 12:53:42', '2022-05-12 12:53:42'),
(432, 10, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.03963089', '2022-05-12', '15:54:20', '2022-05-12 12:54:20', '2022-05-12 12:54:20'),
(433, 10, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '0.08600116', '2022-05-12', '15:54:26', '2022-05-12 12:54:26', '2022-05-12 12:54:26'),
(434, 10, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.04021096', '2022-05-12', '15:54:26', '2022-05-12 12:54:26', '2022-05-12 12:54:26'),
(435, 10, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.05074', '2022-05-12', '15:54:30', '2022-05-12 12:54:30', '2022-05-12 12:54:30'),
(436, 10, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.04696584', '2022-05-12', '15:54:31', '2022-05-12 12:54:31', '2022-05-12 12:54:31'),
(437, 10, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/24/edit/1', '0.04619002', '2022-05-12', '15:54:40', '2022-05-12 12:54:40', '2022-05-12 12:54:40'),
(438, 10, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/create', '0.038131', '2022-05-12', '15:55:02', '2022-05-12 12:55:02', '2022-05-12 12:55:02'),
(439, 10, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/23/edit/1', '0.04792094', '2022-05-12', '15:56:49', '2022-05-12 12:56:49', '2022-05-12 12:56:49'),
(440, 10, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/11/edit', '0.0472188', '2022-05-12', '15:57:26', '2022-05-12 12:57:26', '2022-05-12 12:57:26'),
(441, 10, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/5/edit/1', '0.05004001', '2022-05-12', '15:57:51', '2022-05-12 12:57:51', '2022-05-12 12:57:51'),
(442, 10, '127.0.0.1', 'Dashboard &raquo; أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/admin/11/topics', '0.04963088', '2022-05-12', '15:58:20', '2022-05-12 12:58:20', '2022-05-12 12:58:20'),
(443, 10, '127.0.0.1', 'Dashboard &raquo; أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/admin/11/topics/create', '0.04869413', '2022-05-12', '15:58:24', '2022-05-12 12:58:24', '2022-05-12 12:58:24');
INSERT INTO `iziengdb_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(444, 10, '127.0.0.1', 'Dashboard &raquo; שדגכדש', 'unknown', 'http://127.0.0.1:8000/admin/11/topics/7/edit', '0.05617619', '2022-05-12', '15:58:37', '2022-05-12 12:58:37', '2022-05-12 12:58:37'),
(445, 10, '127.0.0.1', 'שדגכדש', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr/topic/7', '0.06106901', '2022-05-12', '16:03:40', '2022-05-12 13:03:40', '2022-05-12 13:03:40'),
(446, 10, '127.0.0.1', 'الأخبار', 'unknown', 'http://127.0.0.1:8000/news', '0.05516601', '2022-05-12', '16:05:32', '2022-05-12 13:05:32', '2022-05-12 13:05:32'),
(447, 10, '127.0.0.1', 'أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr', '0.05697203', '2022-05-12', '16:09:40', '2022-05-12 13:09:40', '2022-05-12 13:09:40'),
(448, 10, '127.0.0.1', 'Dashboard &raquo; هل يوجد أولوية للقبول في مؤسسات التعليم العالي العالميّة حسب تاريخ التقديم ؟', 'unknown', 'http://127.0.0.1:8000/admin/11/topics/8/edit', '0.03569293', '2022-05-12', '16:14:06', '2022-05-12 13:14:06', '2022-05-12 13:14:06'),
(449, 10, '127.0.0.1', 'هل يوجد أولوية للقبول في مؤسسات التعليم العالي العالميّة حسب تاريخ التقديم ؟', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr/topic/8', '0.03392911', '2022-05-12', '16:14:21', '2022-05-12 13:14:21', '2022-05-12 13:14:21'),
(450, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '0.03470802', '2022-05-13', '11:13:21', '2022-05-13 08:13:21', '2022-05-13 08:13:21'),
(451, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.02688408', '2022-05-13', '11:13:21', '2022-05-13 08:13:21', '2022-05-13 08:13:21'),
(452, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.02202606', '2022-05-13', '11:13:21', '2022-05-13 08:13:21', '2022-05-13 08:13:21'),
(453, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.04213405', '2022-05-13', '11:13:21', '2022-05-13 08:13:21', '2022-05-13 08:13:21'),
(454, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.04042888', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(455, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.0208149', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(456, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.02371311', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(457, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.02460289', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(458, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.02429605', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(459, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.02564597', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(460, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.02570701', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(461, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.02522612', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(462, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.02180386', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(463, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.0258491', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(464, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.0257659', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(465, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.02292585', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(466, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.02285504', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(467, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.02124906', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(468, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.02537084', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(469, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.02579093', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(470, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.024158', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(471, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.022259', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(472, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.02484608', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(473, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.02353597', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(474, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.02080584', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(475, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.02463698', '2022-05-13', '11:13:22', '2022-05-13 08:13:22', '2022-05-13 08:13:22'),
(476, 11, '127.0.0.1', 'الأخبار', 'unknown', 'http://127.0.0.1:8000/news', '0.09157491', '2022-05-13', '12:14:07', '2022-05-13 09:14:07', '2022-05-13 09:14:07'),
(477, 11, '127.0.0.1', 'أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr', '0.09149814', '2022-05-13', '12:15:39', '2022-05-13 09:15:39', '2022-05-13 09:15:39'),
(478, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/icon_slider_arrow.294984c9.svg', '0.06339908', '2022-05-13', '12:16:23', '2022-05-13 09:16:23', '2022-05-13 09:16:23'),
(479, 11, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.03027701', '2022-05-13', '12:17:56', '2022-05-13 09:17:56', '2022-05-13 09:17:56'),
(480, 11, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin', '0.06500196', '2022-05-13', '12:18:26', '2022-05-13 09:18:26', '2022-05-13 09:18:26'),
(481, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.06617594', '2022-05-13', '12:18:27', '2022-05-13 09:18:27', '2022-05-13 09:18:27'),
(482, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.06855702', '2022-05-13', '12:18:27', '2022-05-13 09:18:27', '2022-05-13 09:18:27'),
(483, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.04830909', '2022-05-13', '12:18:27', '2022-05-13 09:18:27', '2022-05-13 09:18:27'),
(484, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.05436182', '2022-05-13', '12:18:27', '2022-05-13 09:18:27', '2022-05-13 09:18:27'),
(485, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.053123', '2022-05-13', '12:18:28', '2022-05-13 09:18:28', '2022-05-13 09:18:28'),
(486, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.06926298', '2022-05-13', '12:18:28', '2022-05-13 09:18:28', '2022-05-13 09:18:28'),
(487, 11, '127.0.0.1', 'Dashboard &raquo; صفحات الموقع', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.08947802', '2022-05-13', '12:18:48', '2022-05-13 09:18:48', '2022-05-13 09:18:48'),
(488, 11, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.05449796', '2022-05-13', '12:18:59', '2022-05-13 09:18:59', '2022-05-13 09:18:59'),
(489, 11, '127.0.0.1', 'שדגכדש', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr/topic/7', '0.06170797', '2022-05-13', '19:07:10', '2022-05-13 16:07:10', '2022-05-13 16:07:10'),
(490, 11, '127.0.0.1', 'Dashboard &raquo; 403', 'unknown', 'http://127.0.0.1:8000/admin/403', '0.02530789', '2022-05-13', '19:18:17', '2022-05-13 16:18:17', '2022-05-13 16:18:17'),
(491, 11, '127.0.0.1', 'Dashboard &raquo; أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/admin/11/topics', '0.03594398', '2022-05-13', '19:56:09', '2022-05-13 16:56:09', '2022-05-13 16:56:09'),
(492, 11, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin/', '0.36030102', '2022-05-13', '20:00:35', '2022-05-13 17:00:35', '2022-05-13 17:00:35'),
(493, 11, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.12512112', '2022-05-13', '20:24:04', '2022-05-13 17:24:04', '2022-05-13 17:24:04'),
(494, 11, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/11/edit', '0.09531498', '2022-05-13', '20:24:07', '2022-05-13 17:24:07', '2022-05-13 17:24:07'),
(495, 11, '127.0.0.1', 'Dashboard &raquo; تصنيفات  أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/admin/11/categories', '0.09268093', '2022-05-13', '20:24:39', '2022-05-13 17:24:39', '2022-05-13 17:24:39'),
(496, 11, '127.0.0.1', 'Dashboard &raquo; تصنيفات  أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/admin/11/categories/create', '0.05497503', '2022-05-13', '20:24:51', '2022-05-13 17:24:51', '2022-05-13 17:24:51'),
(497, 11, '127.0.0.1', 'Dashboard &raquo; هل يوجد أولوية للقبول في مؤسسات التعليم العالي العالميّة حسب تاريخ التقديم ؟', 'unknown', 'http://127.0.0.1:8000/admin/11/topics/8/edit', '0.13217616', '2022-05-13', '20:25:42', '2022-05-13 17:25:42', '2022-05-13 17:25:42'),
(498, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.06504107', '2022-05-13', '20:25:43', '2022-05-13 17:25:43', '2022-05-13 17:25:43'),
(499, 11, '127.0.0.1', 'Dashboard &raquo; ماهو نظام القبول لمؤسسات التعليم العالي العالميّة؟', 'unknown', 'http://127.0.0.1:8000/admin/11/topics/7/edit', '0.10574007', '2022-05-13', '20:26:12', '2022-05-13 17:26:12', '2022-05-13 17:26:12'),
(500, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/', '0.39422107', '2022-05-13', '20:33:37', '2022-05-13 17:33:37', '2022-05-13 17:33:37'),
(501, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.26300097', '2022-05-13', '20:33:38', '2022-05-13 17:33:38', '2022-05-13 17:33:38'),
(502, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.25196886', '2022-05-13', '20:33:38', '2022-05-13 17:33:38', '2022-05-13 17:33:38'),
(503, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.24914503', '2022-05-13', '20:33:39', '2022-05-13 17:33:39', '2022-05-13 17:33:39'),
(504, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.25964808', '2022-05-13', '20:33:39', '2022-05-13 17:33:39', '2022-05-13 17:33:39'),
(505, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.24611497', '2022-05-13', '20:33:39', '2022-05-13 17:33:39', '2022-05-13 17:33:39'),
(506, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.32034588', '2022-05-13', '20:33:40', '2022-05-13 17:33:40', '2022-05-13 17:33:40'),
(507, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/assets/lib/lazysizes/lazysizes.min.js', '0.21008301', '2022-05-13', '20:33:41', '2022-05-13 17:33:41', '2022-05-13 17:33:41'),
(508, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.19945002', '2022-05-13', '20:33:41', '2022-05-13 17:33:41', '2022-05-13 17:33:41'),
(509, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.20993209', '2022-05-13', '20:33:41', '2022-05-13 17:33:41', '2022-05-13 17:33:41'),
(510, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.19136596', '2022-05-13', '20:33:42', '2022-05-13 17:33:42', '2022-05-13 17:33:42'),
(511, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.20440793', '2022-05-13', '20:33:42', '2022-05-13 17:33:42', '2022-05-13 17:33:42'),
(512, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.22653389', '2022-05-13', '20:33:42', '2022-05-13 17:33:42', '2022-05-13 17:33:42'),
(513, 11, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/admin', '0.27288699', '2022-05-13', '20:33:42', '2022-05-13 17:33:42', '2022-05-13 17:33:42'),
(514, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.20371413', '2022-05-13', '20:33:43', '2022-05-13 17:33:43', '2022-05-13 17:33:43'),
(515, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.18239188', '2022-05-13', '20:33:43', '2022-05-13 17:33:43', '2022-05-13 17:33:43'),
(516, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.19426489', '2022-05-13', '20:33:43', '2022-05-13 17:33:43', '2022-05-13 17:33:43'),
(517, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.18547916', '2022-05-13', '20:33:43', '2022-05-13 17:33:43', '2022-05-13 17:33:43'),
(518, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.18738198', '2022-05-13', '20:33:43', '2022-05-13 17:33:43', '2022-05-13 17:33:43'),
(519, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.20243216', '2022-05-13', '20:33:44', '2022-05-13 17:33:44', '2022-05-13 17:33:44'),
(520, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.18818688', '2022-05-13', '20:33:45', '2022-05-13 17:33:45', '2022-05-13 17:33:45'),
(521, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.194556', '2022-05-13', '20:33:45', '2022-05-13 17:33:45', '2022-05-13 17:33:45'),
(522, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.18779492', '2022-05-13', '20:33:45', '2022-05-13 17:33:45', '2022-05-13 17:33:45'),
(523, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.18661714', '2022-05-13', '20:33:46', '2022-05-13 17:33:46', '2022-05-13 17:33:46'),
(524, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.1902492', '2022-05-13', '20:33:46', '2022-05-13 17:33:46', '2022-05-13 17:33:46'),
(525, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.21128702', '2022-05-13', '20:33:46', '2022-05-13 17:33:46', '2022-05-13 17:33:46'),
(526, 11, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://localhost:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.20316982', '2022-05-13', '20:33:47', '2022-05-13 17:33:47', '2022-05-13 17:33:47'),
(527, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.js', '0.29278183', '2022-05-13', '20:33:49', '2022-05-13 17:33:49', '2022-05-13 17:33:49'),
(528, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.25043702', '2022-05-13', '20:33:49', '2022-05-13 17:33:49', '2022-05-13 17:33:49'),
(529, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.30091691', '2022-05-13', '20:33:50', '2022-05-13 17:33:50', '2022-05-13 17:33:50'),
(530, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.26352119', '2022-05-13', '20:33:50', '2022-05-13 17:33:50', '2022-05-13 17:33:50'),
(531, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.26551986', '2022-05-13', '20:33:51', '2022-05-13 17:33:51', '2022-05-13 17:33:51'),
(532, 11, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.26464915', '2022-05-13', '20:33:51', '2022-05-13 17:33:51', '2022-05-13 17:33:51'),
(533, 11, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://localhost:8000/admin/banners', '0.28909111', '2022-05-13', '20:33:54', '2022-05-13 17:33:54', '2022-05-13 17:33:54'),
(534, 11, '127.0.0.1', 'Dashboard &raquo; إعدادات البنرات', 'unknown', 'http://localhost:8000/admin/webmaster/banners', '0.35820985', '2022-05-13', '20:34:11', '2022-05-13 17:34:11', '2022-05-13 17:34:11'),
(535, 11, '127.0.0.1', 'Dashboard &raquo; إعدادات البنرات', 'unknown', 'http://localhost:8000/admin/webmaster/banners/1/edit', '0.36232209', '2022-05-13', '20:34:16', '2022-05-13 17:34:16', '2022-05-13 17:34:16'),
(536, 11, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://localhost:8000/admin/banners/1/edit', '0.41167212', '2022-05-13', '20:34:44', '2022-05-13 17:34:44', '2022-05-13 17:34:44'),
(537, 11, '127.0.0.1', 'Dashboard &raquo; تصنيفات  أسئلة متكررة', 'unknown', 'http://localhost:8000/admin/11/categories', '0.56687212', '2022-05-13', '21:56:34', '2022-05-13 18:56:34', '2022-05-13 18:56:34'),
(538, 11, '127.0.0.1', 'Dashboard &raquo; تصنيفات  أسئلة متكررة', 'unknown', 'http://localhost:8000/admin/11/categories/1/edit', '0.49152112', '2022-05-13', '21:56:38', '2022-05-13 18:56:38', '2022-05-13 18:56:38'),
(539, 11, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://localhost:8000/admin/webmaster/sections', '0.41624308', '2022-05-13', '21:57:41', '2022-05-13 18:57:41', '2022-05-13 18:57:41'),
(540, 11, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://localhost:8000/admin/webmaster/sections/8/edit', '0.74982619', '2022-05-13', '21:57:50', '2022-05-13 18:57:50', '2022-05-13 18:57:50'),
(541, 11, '127.0.0.1', 'Dashboard &raquo; تصنيفات  الدورات', 'unknown', 'http://localhost:8000/admin/8/categories', '1.85420799', '2022-05-13', '21:58:39', '2022-05-13 18:58:39', '2022-05-13 18:58:39'),
(542, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '0.63315606', '2022-05-14', '12:07:14', '2022-05-14 09:07:14', '2022-05-14 09:07:14'),
(543, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.31782317', '2022-05-14', '12:07:15', '2022-05-14 09:07:15', '2022-05-14 09:07:15'),
(544, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.45718002', '2022-05-14', '12:07:15', '2022-05-14 09:07:15', '2022-05-14 09:07:15'),
(545, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.47966003', '2022-05-14', '12:07:16', '2022-05-14 09:07:16', '2022-05-14 09:07:16'),
(546, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.49989104', '2022-05-14', '12:07:17', '2022-05-14 09:07:17', '2022-05-14 09:07:17'),
(547, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.35847807', '2022-05-14', '12:07:17', '2022-05-14 09:07:17', '2022-05-14 09:07:17'),
(548, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.47981215', '2022-05-14', '12:07:17', '2022-05-14 09:07:17', '2022-05-14 09:07:17'),
(549, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/lib/lazysizes/lazysizes.min.js', '0.30041194', '2022-05-14', '12:07:18', '2022-05-14 09:07:18', '2022-05-14 09:07:18'),
(550, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.23512387', '2022-05-14', '12:07:18', '2022-05-14 09:07:18', '2022-05-14 09:07:18'),
(551, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.25117493', '2022-05-14', '12:07:18', '2022-05-14 09:07:18', '2022-05-14 09:07:18'),
(552, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.22752285', '2022-05-14', '12:07:19', '2022-05-14 09:07:19', '2022-05-14 09:07:19'),
(553, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.25643301', '2022-05-14', '12:07:19', '2022-05-14 09:07:19', '2022-05-14 09:07:19'),
(554, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.2204771', '2022-05-14', '12:07:19', '2022-05-14 09:07:19', '2022-05-14 09:07:19'),
(555, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.20771408', '2022-05-14', '12:07:19', '2022-05-14 09:07:19', '2022-05-14 09:07:19'),
(556, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.23771691', '2022-05-14', '12:07:20', '2022-05-14 09:07:20', '2022-05-14 09:07:20'),
(557, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.35241103', '2022-05-14', '12:07:20', '2022-05-14 09:07:20', '2022-05-14 09:07:20'),
(558, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.26515794', '2022-05-14', '12:07:20', '2022-05-14 09:07:20', '2022-05-14 09:07:20'),
(559, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.24314904', '2022-05-14', '12:07:21', '2022-05-14 09:07:21', '2022-05-14 09:07:21'),
(560, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.25133896', '2022-05-14', '12:07:21', '2022-05-14 09:07:21', '2022-05-14 09:07:21'),
(561, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.26250291', '2022-05-14', '12:07:21', '2022-05-14 09:07:21', '2022-05-14 09:07:21'),
(562, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.25373602', '2022-05-14', '12:07:22', '2022-05-14 09:07:22', '2022-05-14 09:07:22'),
(563, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.23456502', '2022-05-14', '12:07:22', '2022-05-14 09:07:22', '2022-05-14 09:07:22'),
(564, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.23468804', '2022-05-14', '12:07:22', '2022-05-14 09:07:22', '2022-05-14 09:07:22'),
(565, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.23670292', '2022-05-14', '12:07:22', '2022-05-14 09:07:22', '2022-05-14 09:07:22'),
(566, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.24267316', '2022-05-14', '12:07:23', '2022-05-14 09:07:23', '2022-05-14 09:07:23'),
(567, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.29670405', '2022-05-14', '12:07:23', '2022-05-14 09:07:23', '2022-05-14 09:07:23'),
(568, 12, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://localhost:8000/admin/login', '0.23404002', '2022-05-14', '12:07:36', '2022-05-14 09:07:36', '2022-05-14 09:07:36'),
(569, 12, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/admin', '0.36794305', '2022-05-14', '12:07:41', '2022-05-14 09:07:41', '2022-05-14 09:07:41'),
(570, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.js', '0.30288291', '2022-05-14', '12:07:44', '2022-05-14 09:07:44', '2022-05-14 09:07:44'),
(571, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.28143191', '2022-05-14', '12:07:45', '2022-05-14 09:07:45', '2022-05-14 09:07:45'),
(572, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.26310992', '2022-05-14', '12:07:46', '2022-05-14 09:07:46', '2022-05-14 09:07:46'),
(573, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.26618409', '2022-05-14', '12:07:46', '2022-05-14 09:07:46', '2022-05-14 09:07:46'),
(574, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.27425289', '2022-05-14', '12:07:47', '2022-05-14 09:07:47', '2022-05-14 09:07:47'),
(575, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.28179789', '2022-05-14', '12:07:48', '2022-05-14 09:07:48', '2022-05-14 09:07:48'),
(576, 12, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://localhost:8000/admin/3/topics', '0.38484192', '2022-05-14', '12:08:53', '2022-05-14 09:08:53', '2022-05-14 09:08:53'),
(577, 12, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://localhost:8000/admin/3/topics/create', '0.57491183', '2022-05-14', '12:08:57', '2022-05-14 09:08:57', '2022-05-14 09:08:57'),
(578, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.31526804', '2022-05-14', '12:09:00', '2022-05-14 09:09:00', '2022-05-14 09:09:00'),
(579, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.36982393', '2022-05-14', '12:09:00', '2022-05-14 09:09:00', '2022-05-14 09:09:00'),
(580, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.28403807', '2022-05-14', '12:09:01', '2022-05-14 09:09:01', '2022-05-14 09:09:01'),
(581, 12, '127.0.0.1', 'Dashboard &raquo; نتعلم في الناصرة, ونتخرج من بريطانيا', 'unknown', 'http://localhost:8000/admin/3/topics/9/edit', '0.31386089', '2022-05-14', '12:10:14', '2022-05-14 09:10:14', '2022-05-14 09:10:14'),
(582, 12, '127.0.0.1', 'Dashboard &raquo; لقب أول من بريطانيا', 'unknown', 'http://localhost:8000/admin/3/topics/10/edit', '0.29109001', '2022-05-14', '12:11:02', '2022-05-14 09:11:02', '2022-05-14 09:11:02'),
(583, 12, '127.0.0.1', 'Dashboard &raquo; لقب ثالث عن بعد في الناصرة', 'unknown', 'http://localhost:8000/admin/3/topics/11/edit', '0.27315998', '2022-05-14', '12:11:41', '2022-05-14 09:11:41', '2022-05-14 09:11:41'),
(584, 12, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://localhost:8000/admin/webmaster/sections', '0.27351999', '2022-05-14', '12:12:11', '2022-05-14 09:12:11', '2022-05-14 09:12:11'),
(585, 12, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://localhost:8000/admin/settings', '0.27098703', '2022-05-14', '12:12:17', '2022-05-14 09:12:17', '2022-05-14 09:12:17'),
(586, 12, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://localhost:8000/admin/webmaster', '1.24995303', '2022-05-14', '12:12:26', '2022-05-14 09:12:26', '2022-05-14 09:12:26'),
(587, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.30329704', '2022-05-14', '12:12:30', '2022-05-14 09:12:30', '2022-05-14 09:12:30'),
(588, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.27890992', '2022-05-14', '12:12:30', '2022-05-14 09:12:30', '2022-05-14 09:12:30'),
(589, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.36086893', '2022-05-14', '12:12:31', '2022-05-14 09:12:31', '2022-05-14 09:12:31'),
(590, 12, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://localhost:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.27703118', '2022-05-14', '12:12:31', '2022-05-14 09:12:31', '2022-05-14 09:12:31'),
(591, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/dr_rohan01.jpg', '0.39428592', '2022-05-14', '12:38:49', '2022-05-14 09:38:49', '2022-05-14 09:38:49'),
(592, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/new1.jpg', '0.25714898', '2022-05-14', '12:38:49', '2022-05-14 09:38:49', '2022-05-14 09:38:49'),
(593, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/1.jpg', '0.2300539', '2022-05-14', '12:38:50', '2022-05-14 09:38:50', '2022-05-14 09:38:50'),
(594, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/3.jpg', '0.20726395', '2022-05-14', '12:38:50', '2022-05-14 09:38:50', '2022-05-14 09:38:50'),
(595, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/4.jpg', '0.28962588', '2022-05-14', '12:38:50', '2022-05-14 09:38:50', '2022-05-14 09:38:50'),
(596, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/P6fNDjdF.jpeg?h=c4835a12&itok=dUkMC9wg', '0.24095607', '2022-05-14', '12:47:21', '2022-05-14 09:47:21', '2022-05-14 09:47:21'),
(597, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-08/img_homepage_president3.jpg?h=df9d1b89&itok=LMbSYYEv', '0.22895694', '2022-05-14', '12:47:21', '2022-05-14 09:47:21', '2022-05-14 09:47:21'),
(598, 12, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-08/img_homepage_president2.jpg?h=31fee8cc&itok=E8R6jViz', '0.22667599', '2022-05-14', '12:47:21', '2022-05-14 09:47:21', '2022-05-14 09:47:21'),
(599, 12, '127.0.0.1', 'Dashboard &raquo; تخرج الفوق الحادي عشر', 'unknown', 'http://localhost:8000/admin/3/topics/12/edit', '0.35584712', '2022-05-14', '12:50:28', '2022-05-14 09:50:28', '2022-05-14 09:50:28'),
(600, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '5.10383701', '2022-05-22', '15:31:37', '2022-05-22 12:31:37', '2022-05-22 12:31:37'),
(601, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '1.0270021', '2022-05-22', '15:31:38', '2022-05-22 12:31:38', '2022-05-22 12:31:38'),
(602, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.82899094', '2022-05-22', '15:31:40', '2022-05-22 12:31:40', '2022-05-22 12:31:40'),
(603, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.77866197', '2022-05-22', '15:31:41', '2022-05-22 12:31:41', '2022-05-22 12:31:41'),
(604, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.48353601', '2022-05-22', '15:31:41', '2022-05-22 12:31:41', '2022-05-22 12:31:41'),
(605, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/hkust_logo_small.svg', '0.43755913', '2022-05-22', '15:31:42', '2022-05-22 12:31:42', '2022-05-22 12:31:42'),
(606, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.46284986', '2022-05-22', '15:31:42', '2022-05-22 12:31:42', '2022-05-22 12:31:42'),
(607, 13, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.60688114', '2022-05-22', '15:31:48', '2022-05-22 12:31:48', '2022-05-22 12:31:48'),
(608, 13, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin', '1.41271281', '2022-05-22', '15:31:52', '2022-05-22 12:31:52', '2022-05-22 12:31:52'),
(609, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.65033197', '2022-05-22', '15:31:54', '2022-05-22 12:31:54', '2022-05-22 12:31:54'),
(610, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.68978691', '2022-05-22', '15:31:55', '2022-05-22 12:31:55', '2022-05-22 12:31:55'),
(611, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.4380312', '2022-05-22', '15:31:55', '2022-05-22 12:31:55', '2022-05-22 12:31:55'),
(612, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.38782406', '2022-05-22', '15:31:57', '2022-05-22 12:31:57', '2022-05-22 12:31:57'),
(613, 13, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.99277997', '2022-05-22', '15:31:58', '2022-05-22 12:31:58', '2022-05-22 12:31:58'),
(614, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.46273899', '2022-05-22', '15:31:58', '2022-05-22 12:31:58', '2022-05-22 12:31:58'),
(615, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.43180299', '2022-05-22', '15:32:39', '2022-05-22 12:32:39', '2022-05-22 12:32:39'),
(616, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.404145', '2022-05-22', '15:32:40', '2022-05-22 12:32:40', '2022-05-22 12:32:40'),
(617, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.49133801', '2022-05-22', '15:32:40', '2022-05-22 12:32:40', '2022-05-22 12:32:40'),
(618, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.49702001', '2022-05-22', '15:32:41', '2022-05-22 12:32:41', '2022-05-22 12:32:41'),
(619, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.40546179', '2022-05-22', '15:32:41', '2022-05-22 12:32:41', '2022-05-22 12:32:41'),
(620, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.429847', '2022-05-22', '15:32:42', '2022-05-22 12:32:42', '2022-05-22 12:32:42'),
(621, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.33402491', '2022-05-22', '15:32:42', '2022-05-22 12:32:42', '2022-05-22 12:32:42'),
(622, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.39460111', '2022-05-22', '15:32:42', '2022-05-22 12:32:42', '2022-05-22 12:32:42'),
(623, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.59538698', '2022-05-22', '15:32:43', '2022-05-22 12:32:43', '2022-05-22 12:32:43'),
(624, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.85443783', '2022-05-22', '15:32:44', '2022-05-22 12:32:44', '2022-05-22 12:32:44'),
(625, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.54100108', '2022-05-22', '15:32:44', '2022-05-22 12:32:44', '2022-05-22 12:32:44'),
(626, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.44492412', '2022-05-22', '15:32:45', '2022-05-22 12:32:45', '2022-05-22 12:32:45'),
(627, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.45945597', '2022-05-22', '15:32:45', '2022-05-22 12:32:45', '2022-05-22 12:32:45'),
(628, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.445364', '2022-05-22', '15:32:46', '2022-05-22 12:32:46', '2022-05-22 12:32:46'),
(629, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.46464896', '2022-05-22', '15:32:46', '2022-05-22 12:32:46', '2022-05-22 12:32:46'),
(630, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/new1.jpg', '0.41336179', '2022-05-22', '15:32:47', '2022-05-22 12:32:47', '2022-05-22 12:32:47'),
(631, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/dr_rohan01.jpg', '0.4093399', '2022-05-22', '15:32:47', '2022-05-22 12:32:47', '2022-05-22 12:32:47'),
(632, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/1.jpg', '0.46617103', '2022-05-22', '15:32:48', '2022-05-22 12:32:48', '2022-05-22 12:32:48'),
(633, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.45784283', '2022-05-22', '15:32:48', '2022-05-22 12:32:48', '2022-05-22 12:32:48'),
(634, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.45195508', '2022-05-22', '15:32:49', '2022-05-22 12:32:49', '2022-05-22 12:32:49'),
(635, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.49609184', '2022-05-22', '15:32:49', '2022-05-22 12:32:49', '2022-05-22 12:32:49'),
(636, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/P6fNDjdF.jpeg?h=c4835a12&itok=dUkMC9wg', '0.55671906', '2022-05-22', '15:32:58', '2022-05-22 12:32:58', '2022-05-22 12:32:58'),
(637, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-08/img_homepage_president3.jpg?h=df9d1b89&itok=LMbSYYEv', '0.60412693', '2022-05-22', '15:32:58', '2022-05-22 12:32:58', '2022-05-22 12:32:58'),
(638, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-08/img_homepage_president2.jpg?h=31fee8cc&itok=E8R6jViz', '0.79688692', '2022-05-22', '15:32:59', '2022-05-22 12:32:59', '2022-05-22 12:32:59'),
(639, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/%C3%89cole%20polytechnique%20f%C3%A9d%C3%A9rale%20de%20Lausanne.jpg?h=2c135db9&itok=vqlAOy-_', '0.64075708', '2022-05-22', '15:33:00', '2022-05-22 12:33:00', '2022-05-22 12:33:00'),
(640, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/National%20University%20of%20Singapore.jpg?h=d868a9d8&itok=Hhw39upp', '0.85184216', '2022-05-22', '15:33:01', '2022-05-22 12:33:01', '2022-05-22 12:33:01'),
(641, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/Shanghai%20Jiao%20Tong%20University.jpg?h=071e36d1&itok=HAm6Cwk-', '0.73723602', '2022-05-22', '15:33:02', '2022-05-22 12:33:02', '2022-05-22 12:33:02'),
(642, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-12/1210_impact_v2.png?itok=0JhJC34k', '1.18059301', '2022-05-22', '15:33:03', '2022-05-22 12:33:03', '2022-05-22 12:33:03'),
(643, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/2021-11/W3307_21_001%20-%20%E8%A4%87%E8%A3%BD_0.jpg?h=2a9dc24d&itok=ZlDkz7J2', '0.96176791', '2022-05-22', '15:33:04', '2022-05-22 12:33:04', '2022-05-22 12:33:04'),
(644, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/2021-10/3_edited_0.png?h=4cf2a6d8&itok=T71uL4UZ', '0.89473891', '2022-05-22', '15:33:05', '2022-05-22 12:33:05', '2022-05-22 12:33:05');
INSERT INTO `iziengdb_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(645, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-09/01_0.jpg?h=55b3a44c&itok=NOBZTbSa', '0.67779803', '2022-05-22', '15:33:06', '2022-05-22 12:33:06', '2022-05-22 12:33:06'),
(646, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-09/11.jpg?h=55b3a44c&itok=5menT4ON', '0.81313705', '2022-05-22', '15:33:07', '2022-05-22 12:33:07', '2022-05-22 12:33:07'),
(647, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-09/15.jpg?h=55b3a44c&itok=jxhMmhaF', '0.56790495', '2022-05-22', '15:33:07', '2022-05-22 12:33:07', '2022-05-22 12:33:07'),
(648, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/icon_slider_arrow.294984c9.svg', '0.37626195', '2022-05-22', '15:33:08', '2022-05-22 12:33:08', '2022-05-22 12:33:08'),
(649, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.29127598', '2022-05-22', '15:38:31', '2022-05-22 12:38:31', '2022-05-22 12:38:31'),
(650, 13, '127.0.0.1', 'Dashboard &raquo; سجل الزوار', 'unknown', 'http://127.0.0.1:8000/admin/visitors', '0.40055609', '2022-05-22', '15:38:43', '2022-05-22 12:38:43', '2022-05-22 12:38:43'),
(651, 13, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/date', '0.40047908', '2022-05-22', '15:38:47', '2022-05-22 12:38:47', '2022-05-22 12:38:47'),
(652, 13, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/country', '0.227108', '2022-05-22', '15:38:50', '2022-05-22 12:38:50', '2022-05-22 12:38:50'),
(653, 13, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/browser', '0.26534986', '2022-05-22', '15:38:51', '2022-05-22 12:38:51', '2022-05-22 12:38:51'),
(654, 13, '127.0.0.1', 'Dashboard &raquo; تحليلات الزوار', 'unknown', 'http://127.0.0.1:8000/admin/analytics/referrer', '0.29057407', '2022-05-22', '15:38:52', '2022-05-22 12:38:52', '2022-05-22 12:38:52'),
(655, 13, '127.0.0.1', 'Dashboard &raquo; البريد الوارد', 'unknown', 'http://127.0.0.1:8000/admin/webmails', '0.50220609', '2022-05-22', '15:38:58', '2022-05-22 12:38:58', '2022-05-22 12:38:58'),
(656, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/Groundbreaking%2520ceremony%2520of%2520Shaw%2520Auditorium_24.jpg?h=cf2a5780&itok=VVaF2SQs', '0.31669497', '2022-05-22', '15:40:31', '2022-05-22 12:40:31', '2022-05-22 12:40:31'),
(657, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/header_image_1920_400/public/2021-09/img_banner_4.jpg?h=bc1925a2&itok=Uz6cVsGD', '0.20131707', '2022-05-22', '15:40:37', '2022-05-22 12:40:37', '2022-05-22 12:40:37'),
(658, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9889/DSC05141_0.JPG?itok=jTu0pVNS', '0.20771503', '2022-05-22', '15:40:37', '2022-05-22 12:40:37', '2022-05-22 12:40:37'),
(659, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-05/photo_2_resized.jpg?itok=Sp-qETB8', '0.208848', '2022-05-22', '15:40:37', '2022-05-22 12:40:37', '2022-05-22 12:40:37'),
(660, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.223454', '2022-05-22', '15:40:37', '2022-05-22 12:40:37', '2022-05-22 12:40:37'),
(661, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9840/Group%20Photo.jpg?itok=1CAuk9qa', '0.24431801', '2022-05-22', '15:40:41', '2022-05-22 12:40:41', '2022-05-22 12:40:41'),
(662, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-03/%5BPhoto%201%5D%20MSHK%20x%20HKUST%20MoU%20Signing%20Ceremony_Group%20Shot_1.jpg?itok=X-wYBU-k', '0.25346494', '2022-05-22', '15:40:41', '2022-05-22 12:40:41', '2022-05-22 12:40:41'),
(663, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-03/group%20photo_v2.png?itok=yMnhVEw-', '0.19631505', '2022-05-22', '15:40:41', '2022-05-22 12:40:41', '2022-05-22 12:40:41'),
(664, 13, '127.0.0.1', 'الأخبار', 'unknown', 'http://127.0.0.1:8000/news', '0.34748888', '2022-05-22', '15:41:11', '2022-05-22 12:41:11', '2022-05-22 12:41:11'),
(665, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.2151711', '2022-05-22', '15:41:12', '2022-05-22 12:41:12', '2022-05-22 12:41:12'),
(666, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-03/UST_Banner_500pxX300px_0307-01%5B1%5D.png?itok=ESQmogCy', '0.22934604', '2022-05-22', '15:41:13', '2022-05-22 12:41:13', '2022-05-22 12:41:13'),
(667, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-03/minnie1.jpg?itok=CrqT94MI', '0.22143888', '2022-05-22', '15:41:14', '2022-05-22 12:41:14', '2022-05-22 12:41:14'),
(668, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-02/MoU-Dr%20Denis%20Yip%20%26%20Prof%20Nancy%20Ip_0.jpg?itok=BWYvqxwd', '0.22390199', '2022-05-22', '15:41:14', '2022-05-22 12:41:14', '2022-05-22 12:41:14'),
(669, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9704/DSC05300-1.jpg?itok=7JUOtn1u', '0.19056582', '2022-05-22', '15:41:14', '2022-05-22 12:41:14', '2022-05-22 12:41:14'),
(670, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-01/Stock_331028057.jpeg?itok=f6A9ky1b', '0.19364309', '2022-05-22', '15:41:14', '2022-05-22 12:41:14', '2022-05-22 12:41:14'),
(671, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9574/Team%20photo%20-%20%E8%A4%87%E8%A3%BD_0.jpg?itok=dR013eT5', '0.20616603', '2022-05-22', '15:41:15', '2022-05-22 12:41:15', '2022-05-22 12:41:15'),
(672, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2022-01/Photo%203.jpg?itok=76wjZI5d', '0.18904114', '2022-05-22', '15:41:15', '2022-05-22 12:41:15', '2022-05-22 12:41:15'),
(673, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9572/01_M67A1364_0.jpg?itok=qQNBzHMi', '0.18439913', '2022-05-22', '15:41:15', '2022-05-22 12:41:15', '2022-05-22 12:41:15'),
(674, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9447/Prof.%20YAN%20Lianke_0.jpg?itok=ur_HigC3', '0.18107319', '2022-05-22', '15:41:15', '2022-05-22 12:41:15', '2022-05-22 12:41:15'),
(675, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.22340703', '2022-05-22', '15:43:36', '2022-05-22 12:43:36', '2022-05-22 12:43:36'),
(676, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9469/image%201%20%282%29_0_1.jpg?itok=bAPaT_8Z', '0.21799111', '2022-05-22', '15:43:39', '2022-05-22 12:43:39', '2022-05-22 12:43:39'),
(677, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9471/UST-HKEX%20Image_2.jpg?itok=hw5t3IIG', '0.19227695', '2022-05-22', '15:43:39', '2022-05-22 12:43:39', '2022-05-22 12:43:39'),
(678, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9382/%2Bpopulation.jpg?itok=aTe3S4A5', '0.16953897', '2022-05-22', '15:43:40', '2022-05-22 12:43:40', '2022-05-22 12:43:40'),
(679, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9388/dayta_cofounders%20%281%29.jpg?itok=d22aJwX5', '0.17702699', '2022-05-22', '15:43:40', '2022-05-22 12:43:40', '2022-05-22 12:43:40'),
(680, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9692/king%20and%20george.jpg?itok=rVuF9DoN', '0.19891715', '2022-05-22', '15:43:40', '2022-05-22 12:43:40', '2022-05-22 12:43:40'),
(681, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9693/research%20group%20photo.jpg?itok=Pe5zTAr7', '0.21558714', '2022-05-22', '15:43:40', '2022-05-22 12:43:40', '2022-05-22 12:43:40'),
(682, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9378/HKUST-09-8051_R2_Final.jpg?itok=2e2FtlUk', '0.19733906', '2022-05-22', '15:43:41', '2022-05-22 12:43:41', '2022-05-22 12:43:41'),
(683, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9691/_MAX4810%20%281%29.jpg?itok=58qvU2Ha', '0.18892503', '2022-05-22', '15:43:41', '2022-05-22 12:43:41', '2022-05-22 12:43:41'),
(684, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9342/JER00410.JPG?itok=uPd2UATI', '0.18494606', '2022-05-22', '15:43:41', '2022-05-22 12:43:41', '2022-05-22 12:43:41'),
(685, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9327/2.jpg?itok=BVyi4cSb', '0.19511008', '2022-05-22', '15:43:41', '2022-05-22 12:43:41', '2022-05-22 12:43:41'),
(686, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9322/shaw%20article%20thumbnail.jpg?itok=NBC0zKZ1', '0.21783614', '2022-05-22', '15:43:41', '2022-05-22 12:43:41', '2022-05-22 12:43:41'),
(687, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9321/cover%202.jpg?itok=yMqMS14l', '0.18778205', '2022-05-22', '15:43:42', '2022-05-22 12:43:42', '2022-05-22 12:43:42'),
(688, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/2021-10/1_3.jpg?itok=nwe8MeMg', '0.17785001', '2022-05-22', '15:43:42', '2022-05-22 12:43:42', '2022-05-22 12:43:42'),
(689, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9401/20200320_eekmlau_firstbufferl_.jpg?itok=o3RjvZB1', '0.16688013', '2022-05-22', '15:43:42', '2022-05-22 12:43:42', '2022-05-22 12:43:42'),
(690, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9694/Handshake_11zon.jpg?itok=YHPKQaAp', '0.20986009', '2022-05-22', '15:43:42', '2022-05-22 12:43:42', '2022-05-22 12:43:42'),
(691, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.29741597', '2022-05-22', '15:44:12', '2022-05-22 12:44:12', '2022-05-22 12:44:12'),
(692, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.2302928', '2022-05-22', '15:47:01', '2022-05-22 12:47:01', '2022-05-22 12:47:01'),
(693, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.24651003', '2022-05-22', '15:49:32', '2022-05-22 12:49:32', '2022-05-22 12:49:32'),
(694, 13, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners', '0.47194505', '2022-05-22', '15:54:11', '2022-05-22 12:54:11', '2022-05-22 12:54:11'),
(695, 13, '127.0.0.1', 'Dashboard &raquo; البنرات الإعلانية', 'unknown', 'http://127.0.0.1:8000/admin/banners/1/edit', '0.39600396', '2022-05-22', '15:54:16', '2022-05-22 12:54:16', '2022-05-22 12:54:16'),
(696, 13, '127.0.0.1', 'Dashboard &raquo; الأجندة والملاحظات', 'unknown', 'http://127.0.0.1:8000/admin/calendar', '0.60408616', '2022-05-22', '16:09:33', '2022-05-22 13:09:33', '2022-05-22 13:09:33'),
(697, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.36365294', '2022-05-22', '16:09:34', '2022-05-22 13:09:34', '2022-05-22 13:09:34'),
(698, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.34116101', '2022-05-22', '16:09:34', '2022-05-22 13:09:34', '2022-05-22 13:09:34'),
(699, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.31633687', '2022-05-22', '16:09:34', '2022-05-22 13:09:34', '2022-05-22 13:09:34'),
(700, 13, '127.0.0.1', 'Dashboard &raquo; صفحات الموقع', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.498101', '2022-05-22', '16:09:39', '2022-05-22 13:09:39', '2022-05-22 13:09:39'),
(701, 13, '127.0.0.1', 'Dashboard &raquo; اتصل بنا', 'unknown', 'http://127.0.0.1:8000/admin/1/topics/2/edit', '3.3284831', '2022-05-22', '16:10:04', '2022-05-22 13:10:04', '2022-05-22 13:10:04'),
(702, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.js', '0.39998007', '2022-05-22', '16:10:06', '2022-05-22 13:10:06', '2022-05-22 13:10:06'),
(703, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/dropzone/dist/min/dropzone.min.css', '0.38998199', '2022-05-22', '16:10:06', '2022-05-22 13:10:06', '2022-05-22 13:10:06'),
(704, 13, '127.0.0.1', 'Dashboard &raquo; الخدمات', 'unknown', 'http://127.0.0.1:8000/admin/2/topics', '0.53666997', '2022-05-22', '16:10:16', '2022-05-22 13:10:16', '2022-05-22 13:10:16'),
(705, 13, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.25396419', '2022-05-22', '16:10:17', '2022-05-22 13:10:17', '2022-05-22 13:10:17'),
(706, 13, '127.0.0.1', 'Dashboard &raquo; تخرج الفوق الحادي عشر', 'unknown', 'http://127.0.0.1:8000/admin/3/topics/12/edit', '0.31246591', '2022-05-22', '16:10:20', '2022-05-22 13:10:20', '2022-05-22 13:10:20'),
(707, 13, '127.0.0.1', 'Dashboard &raquo; الصور', 'unknown', 'http://127.0.0.1:8000/admin/4/topics', '0.2652998', '2022-05-22', '16:12:16', '2022-05-22 13:12:16', '2022-05-22 13:12:16'),
(708, 13, '127.0.0.1', 'Dashboard &raquo; مقالات', 'unknown', 'http://127.0.0.1:8000/admin/7/topics', '0.26259804', '2022-05-22', '16:12:20', '2022-05-22 13:12:20', '2022-05-22 13:12:20'),
(709, 13, '127.0.0.1', 'Dashboard &raquo; الطاقم', 'unknown', 'http://127.0.0.1:8000/admin/9/topics', '0.35421205', '2022-05-22', '16:21:00', '2022-05-22 13:21:00', '2022-05-22 13:21:00'),
(710, 13, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.6315701', '2022-05-22', '16:26:27', '2022-05-22 13:26:27', '2022-05-22 13:26:27'),
(711, 13, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.43922496', '2022-05-22', '16:26:48', '2022-05-22 13:26:48', '2022-05-22 13:26:48'),
(712, 13, '127.0.0.1', 'Dashboard &raquo; مدير الملفات', 'unknown', 'http://127.0.0.1:8000/admin/file-manager', '0.30851388', '2022-05-22', '16:28:05', '2022-05-22 13:28:05', '2022-05-22 13:28:05'),
(713, 13, '127.0.0.1', 'Dashboard &raquo; المستخدمين والصلاحيات', 'unknown', 'http://127.0.0.1:8000/admin/users', '0.51096702', '2022-05-22', '16:28:19', '2022-05-22 13:28:19', '2022-05-22 13:28:19'),
(714, 13, '127.0.0.1', 'Dashboard &raquo; المستخدمين والصلاحيات', 'unknown', 'http://127.0.0.1:8000/admin/users/create', '0.37474108', '2022-05-22', '16:28:43', '2022-05-22 13:28:43', '2022-05-22 13:28:43'),
(715, 13, '127.0.0.1', 'Dashboard &raquo; إضافة صلاحية جديدة', 'unknown', 'http://127.0.0.1:8000/admin/users/permissions/create', '0.43579388', '2022-05-22', '16:28:57', '2022-05-22 13:28:57', '2022-05-22 13:28:57'),
(716, 13, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '1.69513607', '2022-05-22', '16:29:23', '2022-05-22 13:29:23', '2022-05-22 13:29:23'),
(717, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.39102101', '2022-05-22', '16:29:24', '2022-05-22 13:29:24', '2022-05-22 13:29:24'),
(718, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.35976601', '2022-05-22', '16:29:25', '2022-05-22 13:29:25', '2022-05-22 13:29:25'),
(719, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.28906393', '2022-05-22', '16:29:25', '2022-05-22 13:29:25', '2022-05-22 13:29:25'),
(720, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.30616498', '2022-05-22', '16:29:25', '2022-05-22 13:29:25', '2022-05-22 13:29:25'),
(721, 13, '127.0.0.1', 'Dashboard &raquo; Site Menus', 'unknown', 'http://127.0.0.1:8000/admin/menus/4/edit/1', '0.51076388', '2022-05-22', '16:32:00', '2022-05-22 13:32:00', '2022-05-22 13:32:00'),
(722, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.213166', '2022-05-22', '20:15:20', '2022-05-22 17:15:20', '2022-05-22 17:15:20'),
(723, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.2375679', '2022-05-22', '20:22:34', '2022-05-22 17:22:34', '2022-05-22 17:22:34'),
(724, 13, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/home', '0.48570013', '2022-05-22', '20:27:27', '2022-05-22 17:27:27', '2022-05-22 17:27:27'),
(725, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.23816299', '2022-05-22', '20:27:33', '2022-05-22 17:27:33', '2022-05-22 17:27:33'),
(726, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.2376852', '2022-05-22', '20:28:31', '2022-05-22 17:28:31', '2022-05-22 17:28:31'),
(727, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.21934605', '2022-05-22', '20:34:59', '2022-05-22 17:34:59', '2022-05-22 17:34:59'),
(728, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.27908707', '2022-05-22', '20:39:05', '2022-05-22 17:39:05', '2022-05-22 17:39:05'),
(729, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.20381594', '2022-05-22', '20:39:36', '2022-05-22 17:39:36', '2022-05-22 17:39:36'),
(730, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.21633101', '2022-05-22', '20:39:59', '2022-05-22 17:39:59', '2022-05-22 17:39:59'),
(731, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.24714303', '2022-05-22', '20:40:25', '2022-05-22 17:40:25', '2022-05-22 17:40:25'),
(732, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.213346', '2022-05-22', '20:41:53', '2022-05-22 17:41:53', '2022-05-22 17:41:53'),
(733, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/header_image_768_200/public/2021-09/img_banner_4.jpg?h=bc1925a2&itok=8bILWfOX', '0.20042109', '2022-05-22', '20:41:56', '2022-05-22 17:41:56', '2022-05-22 17:41:56'),
(734, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.22740698', '2022-05-22', '20:42:35', '2022-05-22 17:42:35', '2022-05-22 17:42:35'),
(735, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.26248908', '2022-05-22', '20:46:23', '2022-05-22 17:46:23', '2022-05-22 17:46:23'),
(736, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/.assets/Front/images/img/education-banner.webp', '0.27828598', '2022-05-22', '20:52:33', '2022-05-22 17:52:33', '2022-05-22 17:52:33'),
(737, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.19456911', '2022-05-22', '20:52:34', '2022-05-22 17:52:34', '2022-05-22 17:52:34'),
(738, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.23814607', '2022-05-22', '20:52:42', '2022-05-22 17:52:42', '2022-05-22 17:52:42'),
(739, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.24880505', '2022-05-22', '20:53:27', '2022-05-22 17:53:27', '2022-05-22 17:53:27'),
(740, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.25095105', '2022-05-22', '21:01:09', '2022-05-22 18:01:09', '2022-05-22 18:01:09'),
(741, 13, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9871/Prof%20Tam%20%28Left%2C%20HKUST%29%20and%20Mr%20Freeman%20Tsang%20%28Right%2C%20PAM%29_photo%202.jpg?', '0.22175217', '2022-05-22', '21:03:12', '2022-05-22 18:03:12', '2022-05-22 18:03:12'),
(742, 13, '127.0.0.1', 'أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr', '0.2808919', '2022-05-22', '21:06:05', '2022-05-22 18:06:05', '2022-05-22 18:06:05'),
(743, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/', '0.67848206', '2022-05-23', '17:07:06', '2022-05-23 14:07:06', '2022-05-23 14:07:06'),
(744, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/resource/images/svg/milestone_arrow.svg', '0.47857308', '2022-05-23', '17:07:07', '2022-05-23 14:07:07', '2022-05-23 14:07:07'),
(745, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Medium.89ba5470.ttf', '0.74668407', '2022-05-23', '17:07:07', '2022-05-23 14:07:07', '2022-05-23 14:07:07'),
(746, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.ttf?nz072t=', '0.82204485', '2022-05-23', '17:07:08', '2022-05-23 14:07:08', '2022-05-23 14:07:08'),
(747, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/images/white/ust.svg', '0.56194901', '2022-05-23', '17:07:09', '2022-05-23 14:07:09', '2022-05-23 14:07:09'),
(748, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust_style_a/css/fonts/icomoon/fonts/icomoon.woff?nz072t=', '0.5453608', '2022-05-23', '17:07:09', '2022-05-23 14:07:09', '2022-05-23 14:07:09'),
(749, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17930047957933992_1450332543.jpg?itok=3O1_7t3x', '0.49785709', '2022-05-23', '17:07:11', '2022-05-23 14:07:11', '2022-05-23 14:07:11'),
(750, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17958123145515881_1450332543.jpg?itok=YtC8fb80', '0.50893617', '2022-05-23', '17:07:11', '2022-05-23 14:07:11', '2022-05-23 14:07:11'),
(751, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17929161481911036_1450332543.jpg?itok=ad6sgaSB', '0.40708303', '2022-05-23', '17:07:12', '2022-05-23 14:07:12', '2022-05-23 14:07:12'),
(752, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17872456283614034_1450332543.jpg?itok=qesumMWo', '0.48265886', '2022-05-23', '17:07:12', '2022-05-23 14:07:12', '2022-05-23 14:07:12'),
(753, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17931571483767109_1450332543.jpg?itok=T0oyJJal', '0.66206288', '2022-05-23', '17:07:13', '2022-05-23 14:07:13', '2022-05-23 14:07:13'),
(754, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17906474042336358_1450332543.jpg?itok=3-d4HKNJ', '0.41830206', '2022-05-23', '17:07:13', '2022-05-23 14:07:13', '2022-05-23 14:07:13'),
(755, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17925058129869280_1450332543.jpg?itok=7SOxsu6I', '0.49764299', '2022-05-23', '17:07:14', '2022-05-23 14:07:14', '2022-05-23 14:07:14'),
(756, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18138471682222338_1450332543.jpg?itok=m5D8ZUfA', '0.50838518', '2022-05-23', '17:07:15', '2022-05-23 14:07:15', '2022-05-23 14:07:15'),
(757, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17914772015141648_1450332543.jpg?itok=C8rWOzDf', '0.59398913', '2022-05-23', '17:07:15', '2022-05-23 14:07:15', '2022-05-23 14:07:15'),
(758, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17866573940632318_1450332543.jpg?itok=sE4T0dhC', '0.43445206', '2022-05-23', '17:07:16', '2022-05-23 14:07:16', '2022-05-23 14:07:16'),
(759, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_18263031028002313_1450332543.jpg?itok=V13w-js9', '0.43524194', '2022-05-23', '17:07:16', '2022-05-23 14:07:16', '2022-05-23 14:07:16'),
(760, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17891911901394211_1450332543.jpg?itok=xdiQgDME', '0.47855091', '2022-05-23', '17:07:17', '2022-05-23 14:07:17', '2022-05-23 14:07:17'),
(761, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17898128993419391_1450332543.jpg?itok=wIXoAFOM', '0.39060807', '2022-05-23', '17:07:17', '2022-05-23 14:07:17', '2022-05-23 14:07:17'),
(762, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17909825534207995_1450332543.jpg?itok=OUQMbnco', '0.45146203', '2022-05-23', '17:07:18', '2022-05-23 14:07:18', '2022-05-23 14:07:18'),
(763, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17902854371487039_1450332543.jpg?itok=KxGyf2H0', '0.52225304', '2022-05-23', '17:07:18', '2022-05-23 14:07:18', '2022-05-23 14:07:18'),
(764, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17882833649201777_1450332543.jpg?itok=fb3-E10z', '0.51960588', '2022-05-23', '17:07:19', '2022-05-23 14:07:19', '2022-05-23 14:07:19'),
(765, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17903519209822835_1450332543_0.jpg?itok=Gw60Lwtf', '0.39811397', '2022-05-23', '17:07:19', '2022-05-23 14:07:19', '2022-05-23 14:07:19'),
(766, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/social-media/instagram_17895518167935464_1450332543.jpg?itok=3CwTTLuM', '0.45805597', '2022-05-23', '17:07:20', '2022-05-23 14:07:20', '2022-05-23 14:07:20'),
(767, 14, '127.0.0.1', 'الأخبار', 'unknown', 'http://127.0.0.1:8000/news', '0.94869685', '2022-05-23', '17:07:30', '2022-05-23 14:07:30', '2022-05-23 14:07:30'),
(768, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/press_release_thumbnail/public/news/9889/DSC05141_0.JPG?itok=jTu0pVNS', '0.46820903', '2022-05-23', '17:07:32', '2022-05-23 14:07:32', '2022-05-23 14:07:32'),
(769, 14, '127.0.0.1', 'اتصل بنا', 'unknown', 'http://127.0.0.1:8000/contact', '0.31423616', '2022-05-23', '17:40:29', '2022-05-23 14:40:29', '2022-05-23 14:40:29'),
(770, 14, '127.0.0.1', 'أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/faq', '0.32293892', '2022-05-23', '17:42:19', '2022-05-23 14:42:19', '2022-05-23 14:42:19'),
(771, 14, '127.0.0.1', 'أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/asyl-mtkrr', '0.27558088', '2022-05-23', '17:44:25', '2022-05-23 14:44:25', '2022-05-23 14:44:25'),
(772, 14, '127.0.0.1', 'نتعلم في الناصرة, ونتخرج من بريطانيا', 'unknown', 'http://127.0.0.1:8000/news/topic/9', '0.32494497', '2022-05-23', '17:55:48', '2022-05-23 14:55:48', '2022-05-23 14:55:48'),
(773, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/dr_rohan01.jpg', '0.22425318', '2022-05-23', '18:06:00', '2022-05-23 15:06:00', '2022-05-23 15:06:00'),
(774, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/new1.jpg', '0.23394203', '2022-05-23', '18:06:01', '2022-05-23 15:06:01', '2022-05-23 15:06:01'),
(775, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/images/img/1.jpg', '0.22702813', '2022-05-23', '18:06:01', '2022-05-23 15:06:01', '2022-05-23 15:06:01'),
(776, 14, '127.0.0.1', 'الصور', 'unknown', 'http://127.0.0.1:8000/photos', '0.30950594', '2022-05-23', '18:49:51', '2022-05-23 15:49:51', '2022-05-23 15:49:51'),
(777, 14, '127.0.0.1', 'Dashboard &raquo; تسجيل الدخول إلى لوحة التحكم', 'unknown', 'http://127.0.0.1:8000/admin/login', '0.2194972', '2022-05-23', '18:56:02', '2022-05-23 15:56:02', '2022-05-23 15:56:02'),
(778, 14, '127.0.0.1', 'Dashboard &raquo; المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/admin', '0.78414202', '2022-05-23', '18:56:05', '2022-05-23 15:56:05', '2022-05-23 15:56:05'),
(779, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.js', '0.34708095', '2022-05-23', '18:56:06', '2022-05-23 15:56:06', '2022-05-23 15:56:06'),
(780, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.resize.js', '0.3420918', '2022-05-23', '18:56:06', '2022-05-23 15:56:06', '2022-05-23 15:56:06'),
(781, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot/jquery.flot.pie.js', '0.39786482', '2022-05-23', '18:56:07', '2022-05-23 15:56:07', '2022-05-23 15:56:07'),
(782, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.tooltip/js/jquery.flot.tooltip.min.js', '0.39827418', '2022-05-23', '18:56:08', '2022-05-23 15:56:08', '2022-05-23 15:56:08'),
(783, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot-spline/js/jquery.flot.spline.min.js', '0.3355279', '2022-05-23', '18:56:08', '2022-05-23 15:56:08', '2022-05-23 15:56:08'),
(784, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/flot.orderbars/js/jquery.flot.orderBars.js', '0.32681704', '2022-05-23', '18:56:09', '2022-05-23 15:56:09', '2022-05-23 15:56:09'),
(785, 14, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections', '0.27931094', '2022-05-23', '18:56:14', '2022-05-23 15:56:14', '2022-05-23 15:56:14'),
(786, 14, '127.0.0.1', 'Dashboard &raquo; صفحات الموقع', 'unknown', 'http://127.0.0.1:8000/admin/1/topics', '0.33379316', '2022-05-23', '18:56:24', '2022-05-23 15:56:24', '2022-05-23 15:56:24'),
(787, 14, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/1/edit', '0.28408003', '2022-05-23', '18:56:33', '2022-05-23 15:56:33', '2022-05-23 15:56:33'),
(788, 14, '127.0.0.1', 'Dashboard &raquo; الطاقم', 'unknown', 'http://127.0.0.1:8000/admin/9/topics', '0.323632', '2022-05-23', '18:57:06', '2022-05-23 15:57:06', '2022-05-23 15:57:06'),
(789, 14, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics', '0.263412', '2022-05-23', '18:57:10', '2022-05-23 15:57:10', '2022-05-23 15:57:10'),
(790, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', '0.42796993', '2022-05-23', '18:57:11', '2022-05-23 15:57:11', '2022-05-23 15:57:11'),
(791, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css', '0.36485696', '2022-05-23', '18:57:11', '2022-05-23 15:57:11', '2022-05-23 15:57:11'),
(792, 14, '127.0.0.1', 'Dashboard &raquo; الأخبار', 'unknown', 'http://127.0.0.1:8000/admin/3/topics/create', '0.27506495', '2022-05-23', '18:57:12', '2022-05-23 15:57:12', '2022-05-23 15:57:12'),
(793, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', '0.35092592', '2022-05-23', '18:57:12', '2022-05-23 15:57:12', '2022-05-23 15:57:12'),
(794, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-08/img_homepage_president3.jpg?h=df9d1b89&itok=LMbSYYEv', '0.24723983', '2022-05-23', '18:57:40', '2022-05-23 15:57:40', '2022-05-23 15:57:40'),
(795, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/P6fNDjdF.jpeg?h=c4835a12&itok=dUkMC9wg', '0.28741193', '2022-05-23', '18:57:41', '2022-05-23 15:57:41', '2022-05-23 15:57:41'),
(796, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-08/img_homepage_president2.jpg?h=31fee8cc&itok=E8R6jViz', '0.23390007', '2022-05-23', '18:57:41', '2022-05-23 15:57:41', '2022-05-23 15:57:41'),
(797, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/%C3%89cole%20polytechnique%20f%C3%A9d%C3%A9rale%20de%20Lausanne.jpg?h=2c135db9&itok=vqlAOy-_', '0.24837399', '2022-05-23', '18:57:45', '2022-05-23 15:57:45', '2022-05-23 15:57:45'),
(798, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/National%20University%20of%20Singapore.jpg?h=d868a9d8&itok=Hhw39upp', '0.23038793', '2022-05-23', '18:57:45', '2022-05-23 15:57:45', '2022-05-23 15:57:45'),
(799, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-10/Shanghai%20Jiao%20Tong%20University.jpg?h=071e36d1&itok=HAm6Cwk-', '0.25271392', '2022-05-23', '18:57:46', '2022-05-23 15:57:46', '2022-05-23 15:57:46'),
(800, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-12/1210_impact_v2.png?itok=0JhJC34k', '0.34434009', '2022-05-23', '18:58:05', '2022-05-23 15:58:05', '2022-05-23 15:58:05'),
(801, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/2021-11/W3307_21_001%20-%20%E8%A4%87%E8%A3%BD_0.jpg?h=2a9dc24d&itok=ZlDkz7J2', '0.2491982', '2022-05-23', '18:58:06', '2022-05-23 15:58:06', '2022-05-23 15:58:06'),
(802, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/2021-10/3_edited_0.png?h=4cf2a6d8&itok=T71uL4UZ', '0.25328088', '2022-05-23', '18:58:06', '2022-05-23 15:58:06', '2022-05-23 15:58:06'),
(803, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-09/01_0.jpg?h=55b3a44c&itok=NOBZTbSa', '0.28775096', '2022-05-23', '18:58:07', '2022-05-23 15:58:07', '2022-05-23 15:58:07'),
(804, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-09/11.jpg?h=55b3a44c&itok=5menT4ON', '0.2775991', '2022-05-23', '18:58:07', '2022-05-23 15:58:07', '2022-05-23 15:58:07'),
(805, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/ust30abox_1_1/public/2021-09/15.jpg?h=55b3a44c&itok=jxhMmhaF', '0.25616813', '2022-05-23', '18:58:08', '2022-05-23 15:58:08', '2022-05-23 15:58:08'),
(806, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/icon_slider_arrow.294984c9.svg', '0.26423597', '2022-05-23', '18:58:08', '2022-05-23 15:58:08', '2022-05-23 15:58:08'),
(807, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/Groundbreaking%2520ceremony%2520of%2520Shaw%2520Auditorium_24.jpg?h=cf2a5780&itok=VVaF2SQs', '0.33072305', '2022-05-23', '18:58:15', '2022-05-23 15:58:15', '2022-05-23 15:58:15'),
(808, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/sites/default/files/styles/crop_image_1_1/public/Opening%2520ceremony%2520of%2520Tsang%2520Shiu%2520Tim%2520Sports%2520Center_24.jpg?h=40228a8d&itok=jmp6re7y', '0.39933896', '2022-05-23', '18:59:04', '2022-05-23 15:59:04', '2022-05-23 15:59:04'),
(809, 14, '127.0.0.1', 'Dashboard &raquo; تصنيفات  الدورات', 'unknown', 'http://127.0.0.1:8000/admin/8/categories', '0.37242913', '2022-05-23', '19:02:27', '2022-05-23 16:02:27', '2022-05-23 16:02:27'),
(810, 14, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus', '0.37599707', '2022-05-23', '20:40:34', '2022-05-23 17:40:34', '2022-05-23 17:40:34'),
(811, 14, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/23/edit/1', '0.27725101', '2022-05-23', '20:40:39', '2022-05-23 17:40:39', '2022-05-23 17:40:39'),
(812, 14, '127.0.0.1', 'Dashboard &raquo; أسئلة متكررة', 'unknown', 'http://127.0.0.1:8000/admin/11/topics', '0.25964403', '2022-05-23', '20:40:52', '2022-05-23 17:40:52', '2022-05-23 17:40:52'),
(813, 14, '127.0.0.1', 'Dashboard &raquo; إعدادات أقسام الموقع', 'unknown', 'http://127.0.0.1:8000/admin/webmaster/sections/11/edit', '0.29751706', '2022-05-23', '20:41:03', '2022-05-23 17:41:03', '2022-05-23 17:41:03'),
(814, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/.assets/Front/images/img/menu_icons/s-icon-fb_0.svg\'', '0.33305287', '2022-05-23', '20:58:42', '2022-05-23 17:58:42', '2022-05-23 17:58:42'),
(815, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/.assets/Front/images/img/menu_icons/s-icon-fb_0.svg', '0.27308512', '2022-05-23', '20:59:10', '2022-05-23 17:59:10', '2022-05-23 17:59:10'),
(816, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/Front/images/img/menu_icons/s-icon-fb_0.svg', '0.34697795', '2022-05-23', '20:59:24', '2022-05-23 17:59:24', '2022-05-23 17:59:24'),
(817, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/Front/images/s-icon-youtube_0.svg', '0.40577602', '2022-05-23', '21:05:31', '2022-05-23 18:05:31', '2022-05-23 18:05:31'),
(818, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/assets/Front/images/twitter-brands.svg', '0.29256988', '2022-05-23', '21:05:31', '2022-05-23 18:05:31', '2022-05-23 18:05:31'),
(819, 14, '127.0.0.1', 'News', 'unknown', 'http://127.0.0.1:8000/he/news', '0.45803404', '2022-05-23', '21:25:26', '2022-05-23 18:25:26', '2022-05-23 18:25:26'),
(820, 14, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/settings', '0.30289888', '2022-05-23', '21:26:10', '2022-05-23 18:26:10', '2022-05-23 18:26:10'),
(821, 14, '127.0.0.1', 'Dashboard &raquo; الإعدادات العامة', 'unknown', 'http://127.0.0.1:8000/admin/webmaster', '0.62649989', '2022-05-23', '21:28:55', '2022-05-23 18:28:55', '2022-05-23 18:28:55'),
(822, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/css/select2.min.css', '0.35760283', '2022-05-23', '21:28:56', '2022-05-23 18:28:56', '2022-05-23 18:28:56'),
(823, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '0.30748177', '2022-05-23', '21:28:57', '2022-05-23 18:28:57', '2022-05-23 18:28:57'),
(824, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2-bootstrap-theme/dist/select2-bootstrap.4.css', '0.31051207', '2022-05-23', '21:28:57', '2022-05-23 18:28:57', '2022-05-23 18:28:57'),
(825, 14, '127.0.0.1', 'المركز التعليمي العالمي iziEng', 'unknown', 'http://127.0.0.1:8000/assets/dashboard/js/select2/dist/js/select2.min.js', '0.35632205', '2022-05-23', '21:28:57', '2022-05-23 18:28:57', '2022-05-23 18:28:57'),
(826, 14, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/2', '0.28319407', '2022-05-23', '21:29:05', '2022-05-23 18:29:05', '2022-05-23 18:29:05'),
(827, 14, '127.0.0.1', 'Dashboard &raquo; قوائم الموقع', 'unknown', 'http://127.0.0.1:8000/admin/menus/16/edit/2', '0.25697303', '2022-05-23', '21:29:39', '2022-05-23 18:29:39', '2022-05-23 18:29:39'),
(828, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/themes/custom/ust30a/dist/prod/Muli-Regular.069bf750.ttf', '0.28834677', '2022-05-23', '21:35:17', '2022-05-23 18:35:17', '2022-05-23 18:35:17'),
(829, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust/css/fonts/muli/fonts/muli-v12-latin-ext_latin-regular.woff2', '0.54194689', '2022-05-23', '21:35:18', '2022-05-23 18:35:18', '2022-05-23 18:35:18'),
(830, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust/css/fonts/muli/fonts/muli-v12-latin-ext_latin-regular.woff', '0.37141514', '2022-05-23', '21:35:18', '2022-05-23 18:35:18', '2022-05-23 18:35:18'),
(831, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/themes/custom/hkust/css/fonts/muli/fonts/muli-v12-latin-ext_latin-regular.ttf', '0.31130004', '2022-05-23', '21:35:18', '2022-05-23 18:35:18', '2022-05-23 18:35:18'),
(832, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/fonts/muli-v11-latin-regular.woff2', '0.28241086', '2022-05-23', '21:35:19', '2022-05-23 18:35:19', '2022-05-23 18:35:19'),
(833, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/fonts/muli-v11-latin-regular.woff', '0.22519684', '2022-05-23', '21:35:19', '2022-05-23 18:35:19', '2022-05-23 18:35:19'),
(834, 14, '127.0.0.1', 'Dashboard &raquo; 404', 'unknown', 'http://127.0.0.1:8000/profiles/ust/modules/custom/hkust_signature_affiliate/assets/fonts/muli-v11-latin-regular.ttf', '0.20794988', '2022-05-23', '21:35:19', '2022-05-23 18:35:19', '2022-05-23 18:35:19'),
(835, 14, '127.0.0.1', 'الخصوصية', 'unknown', 'http://127.0.0.1:8000/topic/privacy', '0.28984189', '2022-05-23', '21:35:21', '2022-05-23 18:35:21', '2022-05-23 18:35:21'),
(836, 14, '127.0.0.1', 'الشروط والأحكام', 'unknown', 'http://127.0.0.1:8000/topic/terms', '0.30648613', '2022-05-23', '21:35:24', '2022-05-23 18:35:24', '2022-05-23 18:35:24'),
(837, 14, '127.0.0.1', 'تخرج الفوق الحادي عشر', 'unknown', 'http://127.0.0.1:8000/news/topic/12', '0.81654', '2022-05-23', '21:52:06', '2022-05-23 18:52:06', '2022-05-23 18:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_analytics_visitors`
--

DROP TABLE IF EXISTS `iziengdb_analytics_visitors`;
CREATE TABLE IF NOT EXISTS `iziengdb_analytics_visitors` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referrer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_analytics_visitors`
--

INSERT INTO `iziengdb_analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'http://127.0.0.1:8000/install/final', 'NA', 'America/New_York', '2022-04-03', '19:51:24', '2022-04-03 16:51:24', '2022-04-03 16:51:24'),
(2, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-04-05', '22:30:43', '2022-04-05 19:30:43', '2022-04-05 19:30:43'),
(3, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-04-27', '15:18:06', '2022-04-27 12:18:06', '2022-04-27 12:18:06'),
(4, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-04-28', '23:09:59', '2022-04-28 20:09:59', '2022-04-28 20:09:59'),
(5, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-04', '22:13:37', '2022-05-04 19:13:37', '2022-05-04 19:13:37'),
(6, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'http://127.0.0.1:8000/', 'NA', 'America/New_York', '2022-05-06', '21:21:41', '2022-05-06 18:21:41', '2022-05-06 18:21:41'),
(7, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-07', '20:30:10', '2022-05-07 17:30:10', '2022-05-07 17:30:10'),
(8, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-08', '19:31:42', '2022-05-08 16:31:42', '2022-05-08 16:31:42'),
(9, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-10', '19:12:03', '2022-05-10 16:12:03', '2022-05-10 16:12:03'),
(10, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Linux', 'Firefox', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-12', '14:07:48', '2022-05-12 11:07:48', '2022-05-12 11:07:48'),
(11, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Linux', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-13', '11:13:21', '2022-05-13 08:13:21', '2022-05-13 08:13:21'),
(12, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-14', '12:07:14', '2022-05-14 09:07:14', '2022-05-14 09:07:14'),
(13, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-22', '15:31:37', '2022-05-22 12:31:37', '2022-05-22 12:31:37'),
(14, '127.0.0.1', 'unknown', 'US', 'unknown', 'Connecticut', NULL, '41.31', '-72.92', 'Windows 10', 'Chrome', 'unknown', 'unknown', 'NA', 'America/New_York', '2022-05-23', '17:07:06', '2022-05-23 14:07:06', '2022-05-23 14:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_attach_files`
--

DROP TABLE IF EXISTS `iziengdb_attach_files`;
CREATE TABLE IF NOT EXISTS `iziengdb_attach_files` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_banners`
--

DROP TABLE IF EXISTS `iziengdb_banners`;
CREATE TABLE IF NOT EXISTS `iziengdb_banners` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci,
  `file_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_he` text COLLATE utf8mb4_unicode_ci,
  `file_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_banners`
--

INSERT INTO `iziengdb_banners` (`id`, `section_id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `code`, `file_ar`, `file_en`, `video_type`, `youtube_link`, `link_url`, `icon`, `status`, `visits`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`, `title_he`, `details_he`, `file_he`) VALUES
(1, 1, 'بنر رئيسي', 'main banner', 'المركز التعليميّ العالميّ iziEng الوحيد المخول والمعتمد عالميًا من العديد من مؤسسات التعليم العالي العالميّة -خاصة البريطانية – ليكون منصة تحضيرية، تعليميّة وثقافية لدورات اللغة الإنجليزية وغيرها من أجل تحضير أي طالب باللغة الإنجليزية وغيرها ممن يرغبون بالدراسة أكاديميًا عن بعد، ما يمكّنهم ويهيأهم للدراسة بمفردهم ومباشرة مقابل الجامعة المختارة من قِبلهم و/أو بمساعدة محاضرين أجانب -عرب موكلين ومعتمدين من الجامعة ذاتها، ومن ثمّ التخرّج واستلام الشهادة خارج البلاد من تلك الجامعة في العديد من المواضيع المطلوبة حاليًا في سوق العمل..', 'iziEng is the only global educational center authorized and globally accredited by many international higher education institutions - especially the British - to be a preparatory, educational and cultural platform for English language courses and others in order to prepare any student in English and others who wish to study academically remotely, which enables and prepares them to study on their own and directly Against the university chosen by them and/or with the help of foreign-Arab lecturers accredited and accredited by the same university, and then graduating and receiving a certificate abroad from that university in many of the subjects currently required in the labor market..', NULL, 'noimg.png', 'noimg.png', 1, 'https://www.youtube.com/watch?v=dXpUPZHbezo', 'https://google.com', NULL, 1, 0, 1, 1, 1, '2022-04-03 16:51:07', '2022-05-13 18:19:37', 'באנר ראשי', 'iziEng הוא המרכז החינוכי הגלובלי היחיד המורשה והמוסמך ברחבי העולם על ידי מוסדות להשכלה גבוהה בינלאומיים רבים - במיוחד הבריטים - להוות פלטפורמה הכנה, חינוכית ותרבותית לקורסים בשפה האנגלית ואחרים על מנת להכין כל סטודנט באנגלית ואחרים שרוצים לימודים אקדמיים מרחוק, המאפשרים ומכינים אותם ללמוד בעצמם ובאופן ישיר מול האוניברסיטה שנבחרה על ידם ו/או בעזרת מרצים זרים-ערבים המוסמכים ומוסמכים על ידי אותה אוניברסיטה, ולאחר מכן מסיימים ומקבלים תעודה בחו\"ל מבית הספר. אותה אוניברסיטה ברבים מהמקצועות הנדרשים כיום בשוק העבודה..', NULL),
(3, 2, 'تصميم ريسبونسف', 'Responsive Design', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-object-group', 1, 0, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Responsive Design', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL),
(4, 2, ' احدث التقنيات', 'HTML5 & CSS3', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-html5', 1, 0, 2, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'HTML5 & CSS3', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL),
(5, 2, 'باستخدام بوتستراب', 'Bootstrap Used', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-code', 1, 0, 3, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bootstrap Used', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL),
(6, 2, 'تصميم كلاسيكي', 'Classic Design', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-laptop', 1, 0, 4, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Classic Design', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL),
(7, 4, 'منظومة IZIENG', 'IZIENG CRM', NULL, NULL, NULL, '', '', NULL, NULL, 'https://crm.rohan.world/loginstudent.aspx', NULL, 1, 0, 5, 1, NULL, '2022-05-07 18:01:49', '2022-05-07 18:01:49', 'מערכת IZIENG', NULL, ''),
(8, 4, 'test link 1', 'test link 1', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 1, 0, 6, 1, NULL, '2022-05-07 18:12:53', '2022-05-07 18:12:53', 'test link 1', NULL, ''),
(9, 4, 'test link 2', 'test link 2', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 1, 0, 7, 1, NULL, '2022-05-07 18:13:04', '2022-05-07 18:13:04', 'test link 2', NULL, ''),
(10, 4, 'test link 3', 'test link 3', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 1, 0, 8, 1, NULL, '2022-05-07 18:13:13', '2022-05-07 18:13:13', 'test link 3', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_comments`
--

DROP TABLE IF EXISTS `iziengdb_comments`;
CREATE TABLE IF NOT EXISTS `iziengdb_comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_contacts`
--

DROP TABLE IF EXISTS `iziengdb_contacts`;
CREATE TABLE IF NOT EXISTS `iziengdb_contacts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_contacts_groups`
--

DROP TABLE IF EXISTS `iziengdb_contacts_groups`;
CREATE TABLE IF NOT EXISTS `iziengdb_contacts_groups` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_contacts_groups`
--

INSERT INTO `iziengdb_contacts_groups` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Newsletter Emails', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_countries`
--

DROP TABLE IF EXISTS `iziengdb_countries`;
CREATE TABLE IF NOT EXISTS `iziengdb_countries` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_countries`
--

INSERT INTO `iziengdb_countries` (`id`, `code`, `title_ar`, `title_en`, `tel`, `created_at`, `updated_at`, `title_he`) VALUES
(1, 'AL', 'ألبانيا', 'Albania', '355', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Albania'),
(2, 'DZ', 'الجزائر', 'Algeria', '213', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Algeria'),
(3, 'AS', 'ساموا الأمريكية', 'American Samoa', '1-684', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'American Samoa'),
(4, 'AD', 'أندورا', 'Andorra', '376', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Andorra'),
(5, 'AO', 'أنغولا', 'Angola', '244', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Angola'),
(6, 'AI', 'أنغيلا', 'Anguilla', '1-264', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Anguilla'),
(7, 'AR', 'الأرجنتين', 'Argentina', '54', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Argentina'),
(8, 'AM', 'أرمينيا', 'Armenia', '374', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Armenia'),
(9, 'AW', 'أروبا', 'Aruba', '297', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Aruba'),
(10, 'AU', 'أستراليا', 'Australia', '61', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Australia'),
(11, 'AT', 'النمسا', 'Austria', '43', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Austria'),
(12, 'AZ', 'أذربيجان', 'Azerbaijan', '994', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Azerbaijan'),
(13, 'BS', 'جزر البهاما', 'Bahamas', '1-242', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bahamas'),
(14, 'BH', 'البحرين', 'Bahrain', '973', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bahrain'),
(15, 'BD', 'بنغلاديش', 'Bangladesh', '880', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bangladesh'),
(16, 'BB', 'بربادوس', 'Barbados', '1-246', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Barbados'),
(17, 'BY', 'روسيا البيضاء', 'Belarus', '375', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Belarus'),
(18, 'BE', 'بلجيكا', 'Belgium', '32', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Belgium'),
(19, 'BZ', 'بليز', 'Belize', '501', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Belize'),
(20, 'BJ', 'بنين', 'Benin', '229', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Benin'),
(21, 'BM', 'برمودا', 'Bermuda', '1-441', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bermuda'),
(22, 'BT', 'بوتان', 'Bhutan', '975', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bhutan'),
(23, 'BO', 'بوليفيا', 'Bolivia', '591', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bolivia'),
(24, 'BA', 'البوسنة والهرسك', 'Bosnia and Herzegovina', '387', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bosnia and Herzegovina'),
(25, 'BW', 'بوتسوانا', 'Botswana', '267', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Botswana'),
(26, 'BR', 'البرازيل', 'Brazil', '55', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Brazil'),
(27, 'VG', 'جزر فيرجن البريطانية', 'British Virgin Islands', '1-284', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'British Virgin Islands'),
(28, 'IO', 'إقليم المحيط الهندي البريطاني', 'British Indian Ocean Territory', '246', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'British Indian Ocean Territory'),
(29, 'BN', 'بروناي دار السلام', 'Brunei Darussalam', '673', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Brunei Darussalam'),
(30, 'BG', 'بلغاريا', 'Bulgaria', '359', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Bulgaria'),
(31, 'BF', 'بوركينا فاسو', 'Burkina Faso', '226', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Burkina Faso'),
(32, 'BI', 'بوروندي', 'Burundi', '257', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Burundi'),
(33, 'KH', 'كمبوديا', 'Cambodia', '855', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cambodia'),
(34, 'CM', 'الكاميرون', 'Cameroon', '237', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cameroon'),
(35, 'CA', 'كندا', 'Canada', '1', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Canada'),
(36, 'CV', 'الرأس الأخضر', 'Cape Verde', '238', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cape Verde'),
(37, 'KY', 'جزر كايمان', 'Cayman Islands', '1-345', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cayman Islands'),
(38, 'CF', 'افريقيا الوسطى', 'Central African Republic', '236', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Central African Republic'),
(39, 'TD', 'تشاد', 'Chad', '235', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Chad'),
(40, 'CL', 'تشيلي', 'Chile', '56', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Chile'),
(41, 'CN', 'الصين', 'China', '86', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'China'),
(42, 'HK', 'هونغ كونغ', 'Hong Kong', '852', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Hong Kong'),
(43, 'MO', 'ماكاو', 'Macao', '853', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Macao'),
(44, 'CX', 'جزيرة الكريسماس', 'Christmas Island', '61', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Christmas Island'),
(45, 'CC', 'جزر كوكوس (كيلينغ)', 'Cocos (Keeling) Islands', '61', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cocos (Keeling) Islands'),
(46, 'CO', 'كولومبيا', 'Colombia', '57', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Colombia'),
(47, 'KM', 'جزر القمر', 'Comoros', '269', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Comoros'),
(48, 'CK', 'جزر كوك', 'Cook Islands', '682', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cook Islands'),
(49, 'CR', 'كوستا ريكا', 'Costa Rica', '506', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Costa Rica'),
(50, 'HR', 'كرواتيا', 'Croatia', '385', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Croatia'),
(51, 'CU', 'كوبا', 'Cuba', '53', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cuba'),
(52, 'CY', 'قبرص', 'Cyprus', '357', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Cyprus'),
(53, 'CZ', 'الجمهورية التشيكية', 'Czech Republic', '420', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Czech Republic'),
(54, 'DK', 'الدنمارك', 'Denmark', '45', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Denmark'),
(55, 'DJ', 'جيبوتي', 'Djibouti', '253', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Djibouti'),
(56, 'DM', 'دومينيكا', 'Dominica', '1-767', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Dominica'),
(57, 'DO', 'جمهورية الدومينيكان', 'Dominican Republic', '1-809', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Dominican Republic'),
(58, 'EC', 'الاكوادور', 'Ecuador', '593', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Ecuador'),
(59, 'EG', 'مصر', 'Egypt', '20', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Egypt'),
(60, 'SV', 'السلفادور', 'El Salvador', '503', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'El Salvador'),
(61, 'GQ', 'غينيا الاستوائية', 'Equatorial Guinea', '240', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Equatorial Guinea'),
(62, 'ER', 'إريتريا', 'Eritrea', '291', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Eritrea'),
(63, 'EE', 'استونيا', 'Estonia', '372', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Estonia'),
(64, 'ET', 'أثيوبيا', 'Ethiopia', '251', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Ethiopia'),
(65, 'FO', 'جزر فارو', 'Faroe Islands', '298', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Faroe Islands'),
(66, 'FJ', 'فيجي', 'Fiji', '679', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Fiji'),
(67, 'FI', 'فنلندا', 'Finland', '358', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Finland'),
(68, 'FR', 'فرنسا', 'France', '33', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'France'),
(69, 'GF', 'جيانا الفرنسية', 'French Guiana', '689', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'French Guiana'),
(70, 'GA', 'الغابون', 'Gabon', '241', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Gabon'),
(71, 'GM', 'غامبيا', 'Gambia', '220', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Gambia'),
(72, 'GE', 'جورجيا', 'Georgia', '995', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Georgia'),
(73, 'DE', 'ألمانيا', 'Germany', '49', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Germany'),
(74, 'GH', 'غانا', 'Ghana', '233', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Ghana'),
(75, 'GI', 'جبل طارق', 'Gibraltar', '350', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Gibraltar'),
(76, 'GR', 'يونان', 'Greece', '30', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Greece'),
(77, 'GL', 'غرينلاند', 'Greenland', '299', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Greenland'),
(78, 'GD', 'غرينادا', 'Grenada', '1-473', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Grenada'),
(79, 'GU', 'غوام', 'Guam', '1-671', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Guam'),
(80, 'GT', 'غواتيمالا', 'Guatemala', '502', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Guatemala'),
(81, 'GN', 'غينيا', 'Guinea', '224', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Guinea'),
(82, 'GW', 'غينيا-بيساو', 'Guinea-Bissau', '245', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Guinea-Bissau'),
(83, 'GY', 'غيانا', 'Guyana', '592', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Guyana'),
(84, 'HT', 'هايتي', 'Haiti', '509', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Haiti'),
(85, 'HN', 'هندوراس', 'Honduras', '504', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Honduras'),
(86, 'HU', 'هنغاريا', 'Hungary', '36', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Hungary'),
(87, 'IS', 'أيسلندا', 'Iceland', '354', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Iceland'),
(88, 'IN', 'الهند', 'India', '91', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'India'),
(89, 'ID', 'أندونيسيا', 'Indonesia', '62', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Indonesia'),
(90, 'IR', 'جمهورية إيران الإسلامية', 'Iran, Islamic Republic of', '98', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Iran, Islamic Republic of'),
(91, 'IQ', 'العراق', 'Iraq', '964', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Iraq'),
(92, 'IE', 'أيرلندا', 'Ireland', '353', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Ireland'),
(93, 'IM', 'جزيرة مان', 'Isle of Man', '44-1624', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Isle of Man'),
(94, 'IL', 'إسرائيل', 'Israel', '972', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Israel'),
(95, 'IT', 'إيطاليا', 'Italy', '39', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Italy'),
(96, 'JM', 'جامايكا', 'Jamaica', '1-876', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Jamaica'),
(97, 'JP', 'اليابان', 'Japan', '81', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Japan'),
(98, 'JE', 'جيرسي', 'Jersey', '44-1534', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Jersey'),
(99, 'JO', 'الأردن', 'Jordan', '962', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Jordan'),
(100, 'KZ', 'كازاخستان', 'Kazakhstan', '7', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Kazakhstan'),
(101, 'KE', 'كينيا', 'Kenya', '254', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Kenya'),
(102, 'KI', 'كيريباس', 'Kiribati', '686', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Kiribati'),
(103, 'KW', 'الكويت', 'Kuwait', '965', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Kuwait'),
(104, 'KG', 'قيرغيزستان', 'Kyrgyzstan', '996', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Kyrgyzstan'),
(105, 'LV', 'لاتفيا', 'Latvia', '371', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Latvia'),
(106, 'LB', 'لبنان', 'Lebanon', '961', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Lebanon'),
(107, 'LS', 'ليسوتو', 'Lesotho', '266', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Lesotho'),
(108, 'LR', 'ليبيريا', 'Liberia', '231', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Liberia'),
(109, 'LY', 'ليبيا', 'Libya', '218', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Libya'),
(110, 'LI', 'ليشتنشتاين', 'Liechtenstein', '423', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Liechtenstein'),
(111, 'LT', 'ليتوانيا', 'Lithuania', '370', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Lithuania'),
(112, 'LU', 'لوكسمبورغ', 'Luxembourg', '352', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Luxembourg'),
(113, 'MK', 'مقدونيا، جمهورية', 'Macedonia', '389', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Macedonia'),
(114, 'MG', 'مدغشقر', 'Madagascar', '261', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Madagascar'),
(115, 'MW', 'ملاوي', 'Malawi', '265', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Malawi'),
(116, 'MY', 'ماليزيا', 'Malaysia', '60', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Malaysia'),
(117, 'MV', 'جزر المالديف', 'Maldives', '960', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Maldives'),
(118, 'ML', 'مالي', 'Mali', '223', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mali'),
(119, 'MT', 'مالطا', 'Malta', '356', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Malta'),
(120, 'MH', 'جزر مارشال', 'Marshall Islands', '692', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Marshall Islands'),
(121, 'MR', 'موريتانيا', 'Mauritania', '222', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mauritania'),
(122, 'MU', 'موريشيوس', 'Mauritius', '230', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mauritius'),
(123, 'YT', 'مايوت', 'Mayotte', '262', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mayotte'),
(124, 'MX', 'المكسيك', 'Mexico', '52', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mexico'),
(125, 'FM', 'ولايات ميكرونيزيا الموحدة', 'Micronesia', '691', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Micronesia'),
(126, 'MD', 'مولدوفا', 'Moldova', '373', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Moldova'),
(127, 'MC', 'موناكو', 'Monaco', '377', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Monaco'),
(128, 'MN', 'منغوليا', 'Mongolia', '976', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mongolia'),
(129, 'ME', 'الجبل الأسود', 'Montenegro', '382', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Montenegro'),
(130, 'MS', 'مونتسيرات', 'Montserrat', '1-664', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Montserrat'),
(131, 'MA', 'المغرب', 'Morocco', '212', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Morocco'),
(132, 'MZ', 'موزمبيق', 'Mozambique', '258', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Mozambique'),
(133, 'MM', 'ميانمار', 'Myanmar', '95', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Myanmar'),
(134, 'NA', 'ناميبيا', 'Namibia', '264', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Namibia'),
(135, 'NR', 'ناورو', 'Nauru', '674', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Nauru'),
(136, 'NP', 'نيبال', 'Nepal', '977', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Nepal'),
(137, 'NL', 'هولندا', 'Netherlands', '31', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Netherlands'),
(138, 'AN', 'جزر الأنتيل الهولندية', 'Netherlands Antilles', '599', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Netherlands Antilles'),
(139, 'NC', 'كاليدونيا الجديدة', 'New Caledonia', '687', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'New Caledonia'),
(140, 'NZ', 'نيوزيلندا', 'New Zealand', '64', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'New Zealand'),
(141, 'NI', 'نيكاراغوا', 'Nicaragua', '505', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Nicaragua'),
(142, 'NE', 'النيجر', 'Niger', '227', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Niger'),
(143, 'NG', 'نيجيريا', 'Nigeria', '234', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Nigeria'),
(144, 'NU', 'نيوي', 'Niue', '683', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Niue'),
(145, 'NO', 'النرويج', 'Norway', '47', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Norway'),
(146, 'OM', 'عمان', 'Oman', '968', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Oman'),
(147, 'PK', 'باكستان', 'Pakistan', '92', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Pakistan'),
(148, 'PW', 'بالاو', 'Palau', '680', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Palau'),
(149, 'PS', 'فلسطين', 'Palestinian', '972', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Palestinian'),
(150, 'PA', 'بناما', 'Panama', '507', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Panama'),
(151, 'PY', 'باراغواي', 'Paraguay', '595', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Paraguay'),
(152, 'PE', 'بيرو', 'Peru', '51', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Peru'),
(153, 'PH', 'الفلبين', 'Philippines', '63', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Philippines'),
(154, 'PN', 'بيتكيرن', 'Pitcairn', '870', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Pitcairn'),
(155, 'PL', 'بولندا', 'Poland', '48', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Poland'),
(156, 'PT', 'البرتغال', 'Portugal', '351', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Portugal'),
(157, 'PR', 'بويرتو ريكو', 'Puerto Rico', '1-787', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Puerto Rico'),
(158, 'QA', 'قطر', 'Qatar', '974', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Qatar'),
(159, 'RO', 'رومانيا', 'Romania', '40', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Romania'),
(160, 'RU', 'الفيدرالية الروسية', 'Russian Federation', '7', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Russian Federation'),
(161, 'RW', 'رواندا', 'Rwanda', '250', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Rwanda'),
(162, 'SH', 'سانت هيلينا', 'Saint Helena', '290', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Saint Helena'),
(163, 'KN', 'سانت كيتس ونيفيس', 'Saint Kitts and Nevis', '1-869', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Saint Kitts and Nevis'),
(164, 'LC', 'سانت لوسيا', 'Saint Lucia', '1-758', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Saint Lucia'),
(165, 'PM', 'سان بيار وميكلون', 'Saint Pierre and Miquelon', '508', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Saint Pierre and Miquelon'),
(166, 'VC', 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and Grenadines', '1-784', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Saint Vincent and Grenadines'),
(167, 'WS', 'ساموا', 'Samoa', '685', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Samoa'),
(168, 'SM', 'سان مارينو', 'San Marino', '378', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'San Marino'),
(169, 'ST', 'ساو تومي وبرينسيبي', 'Sao Tome and Principe', '239', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Sao Tome and Principe'),
(170, 'SA', 'المملكة العربية السعودية', 'Saudi Arabia', '966', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Saudi Arabia'),
(171, 'SN', 'السنغال', 'Senegal', '221', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Senegal'),
(172, 'RS', 'صربيا', 'Serbia', '381', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Serbia'),
(173, 'SC', 'سيشيل', 'Seychelles', '248', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Seychelles'),
(174, 'SL', 'سيرا ليون', 'Sierra Leone', '232', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Sierra Leone'),
(175, 'SG', 'سنغافورة', 'Singapore', '65', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Singapore'),
(176, 'SK', 'سلوفاكيا', 'Slovakia', '421', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Slovakia'),
(177, 'SI', 'سلوفينيا', 'Slovenia', '386', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Slovenia'),
(178, 'SB', 'جزر سليمان', 'Solomon Islands', '677', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Solomon Islands'),
(179, 'SO', 'الصومال', 'Somalia', '252', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Somalia'),
(180, 'ZA', 'جنوب أفريقيا', 'South Africa', '27', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'South Africa'),
(181, 'ES', 'إسبانيا', 'Spain', '34', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Spain'),
(182, 'LK', 'سيريلانكا', 'Sri Lanka', '94', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Sri Lanka'),
(183, 'SD', 'السودان', 'Sudan', '249', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Sudan'),
(184, 'SR', 'سورينام', 'Suriname', '597', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Suriname'),
(185, 'SJ', 'جزر سفالبارد وجان ماين', 'Svalbard and Jan Mayen Islands', '47', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Svalbard and Jan Mayen Islands'),
(186, 'SZ', 'سوازيلاند', 'Swaziland', '268', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Swaziland'),
(187, 'SE', 'السويد', 'Sweden', '46', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Sweden'),
(188, 'CH', 'سويسرا', 'Switzerland', '41', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Switzerland'),
(189, 'SY', 'سوريا', 'Syrian Arab Republic', '963', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Syrian Arab Republic'),
(190, 'TW', 'تايوان، جمهورية الصين', 'Taiwan, Republic of China', '886', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Taiwan, Republic of China'),
(191, 'TJ', 'طاجيكستان', 'Tajikistan', '992', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Tajikistan'),
(192, 'TZ', 'تنزانيا', 'Tanzania', '255', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Tanzania'),
(193, 'TH', 'تايلاند', 'Thailand', '66', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Thailand'),
(194, 'TG', 'توغو', 'Togo', '228', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Togo'),
(195, 'TK', 'توكيلاو', 'Tokelau', '690', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Tokelau'),
(196, 'TO', 'تونغا', 'Tonga', '676', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Tonga'),
(197, 'TT', 'ترينداد وتوباغو', 'Trinidad and Tobago', '1-868', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Trinidad and Tobago'),
(198, 'TN', 'تونس', 'Tunisia', '216', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Tunisia'),
(199, 'TR', 'تركيا', 'Turkey', '90', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Turkey'),
(200, 'TM', 'تركمانستان', 'Turkmenistan', '993', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Turkmenistan'),
(201, 'TC', 'جزر تركس وكايكوس', 'Turks and Caicos Islands', '1-649', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Turks and Caicos Islands'),
(202, 'TV', 'توفالو', 'Tuvalu', '688', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Tuvalu'),
(203, 'UG', 'أوغندا', 'Uganda', '256', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Uganda'),
(204, 'UA', 'أوكرانيا', 'Ukraine', '380', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Ukraine'),
(205, 'AE', 'الإمارات العربية المتحدة', 'United Arab Emirates', '971', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'United Arab Emirates'),
(206, 'GB', 'المملكة المتحدة', 'United Kingdom', '44', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'United Kingdom'),
(207, 'US', 'الولايات المتحدة الأمريكية', 'United States of America', '1', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'United States of America'),
(208, 'UY', 'أوروغواي', 'Uruguay', '598', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Uruguay'),
(209, 'UZ', 'أوزبكستان', 'Uzbekistan', '998', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Uzbekistan'),
(210, 'VU', 'فانواتو', 'Vanuatu', '678', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Vanuatu'),
(211, 'VE', 'فنزويلا', 'Venezuela', '58', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Venezuela'),
(212, 'VN', 'فيتنام', 'Viet Nam', '84', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Viet Nam'),
(213, 'WF', 'واليس وفوتونا', 'Wallis and Futuna Islands', '681', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Wallis and Futuna Islands'),
(214, 'YE', 'اليمن', 'Yemen', '967', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Yemen'),
(215, 'ZM', 'زامبيا', 'Zambia', '260', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Zambia'),
(216, 'ZW', 'زيمبابوي', 'Zimbabwe', '263', '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_events`
--

DROP TABLE IF EXISTS `iziengdb_events`;
CREATE TABLE IF NOT EXISTS `iziengdb_events` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_failed_jobs`
--

DROP TABLE IF EXISTS `iziengdb_failed_jobs`;
CREATE TABLE IF NOT EXISTS `iziengdb_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iziengDB_failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_languages`
--

DROP TABLE IF EXISTS `iziengdb_languages`;
CREATE TABLE IF NOT EXISTS `iziengdb_languages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `left` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `right` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `box_status` tinyint(4) DEFAULT '1',
  `status` tinyint(4) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_languages`
--

INSERT INTO `iziengdb_languages` (`id`, `title`, `code`, `direction`, `left`, `right`, `icon`, `box_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 'ltr', 'left', 'right', 'us', 1, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07'),
(2, 'العربية', 'ar', 'rtl', 'right', 'left', 'sa', 1, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07'),
(11, 'עברית', 'he', 'rtl', 'right', 'left', 'il', 1, 1, 1, 1, '2022-04-03 16:53:19', '2022-04-03 16:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_ltm_translations`
--

DROP TABLE IF EXISTS `iziengdb_ltm_translations`;
CREATE TABLE IF NOT EXISTS `iziengdb_ltm_translations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `key` text COLLATE utf8mb4_bin NOT NULL,
  `value` text COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_maps`
--

DROP TABLE IF EXISTS `iziengdb_maps`;
CREATE TABLE IF NOT EXISTS `iziengdb_maps` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `icon` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_he` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_menus`
--

DROP TABLE IF EXISTS `iziengdb_menus`;
CREATE TABLE IF NOT EXISTS `iziengdb_menus` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `row_no` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_menus`
--

INSERT INTO `iziengdb_menus` (`id`, `row_no`, `father_id`, `title_ar`, `title_en`, `status`, `type`, `cat_id`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`, `title_he`) VALUES
(1, 1, 0, 'القائمة الرئيسية', 'Main Menu', 1, 0, 0, '', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Main Menu'),
(2, 2, 0, 'روابط سريعة', 'Quick Links', 1, 0, 0, '', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Quick Links'),
(3, 1, 1, 'عن المركز', 'about us', 1, 0, 1, 'home', 1, 1, '2022-04-03 16:51:07', '2022-05-07 18:29:38', 'עלינו'),
(4, 2, 3, 'عن المركز', 'about us', 1, 1, 1, 'topic/about', 1, 1, '2022-04-03 16:51:07', '2022-05-07 18:30:34', 'עלינו'),
(5, 5, 1, 'مجازين / مقالات', 'Articles', 1, 3, 7, NULL, 1, 1, '2022-04-03 16:51:07', '2022-05-08 16:57:52', 'מאמרים'),
(6, 4, 1, 'انجازات واخبار', 'achievements and news', 1, 2, 3, NULL, 1, 1, '2022-04-03 16:51:07', '2022-05-08 16:58:29', 'השגים וחדשות'),
(7, 4, 3, 'الصور', 'Photos', 1, 2, 4, NULL, 1, 1, '2022-04-03 16:51:07', '2022-05-07 18:31:51', 'Photos'),
(24, 1, 22, 'التعليم العالي عن بعد (DL)', 'التعليم العالي عن بعد (DL)', 1, 0, 1, NULL, 1, 1, '2022-05-08 17:01:02', '2022-05-08 17:07:13', 'التعليم العالي عن بعد (DL)'),
(12, 7, 1, 'اتصل بنا', 'Contact', 1, 1, 0, 'contact', 1, NULL, '2022-04-03 16:51:07', '2022-05-08 17:00:02', 'Contact'),
(16, 4, 2, 'الخصوصية', 'Privacy', 1, 1, 2, 'topic/privacy', 1, 1, '2022-04-03 16:51:07', '2022-05-23 18:29:57', 'הצהרת נגישות'),
(17, 5, 2, 'الشروط والأحكام', 'Terms & Conditions', 1, 1, 0, 'topic/terms', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Terms & Conditions'),
(23, 6, 1, 'أسئلة متكررة', 'FAQ', 1, 2, 11, NULL, 1, 1, '2022-05-08 16:59:44', '2022-05-12 12:58:06', 'שאלות'),
(19, 3, 3, 'جولة في المركز', 'جولة في المركز', 1, 1, 1, '#', 1, NULL, '2022-05-07 18:31:03', '2022-05-07 18:31:38', 'جولة في المركز'),
(20, 5, 3, 'مكتبة المركز', 'مكتبة المركز', 1, 1, 1, '#', 1, NULL, '2022-05-07 18:32:12', '2022-05-07 18:32:25', 'مكتبة المركز'),
(21, 2, 1, 'الطاقم', 'Team', 1, 2, 9, NULL, 1, NULL, '2022-05-08 16:52:34', '2022-05-08 16:52:46', 'צוות'),
(22, 3, 1, 'أقسام المركز', 'Center Department', 1, 0, 1, NULL, 1, NULL, '2022-05-08 16:53:40', '2022-05-08 16:53:50', 'מחלקות'),
(25, 2, 22, 'مركز لطلاب الجامعة', 'مركز لطلاب الجامعة', 1, 1, 1, NULL, 1, 1, '2022-05-08 17:01:21', '2022-05-08 17:02:53', 'مركز لطلاب الجامعة'),
(26, 3, 22, 'دورات تحضير أكاديميّ', 'دورات تحضير أكاديميّ', 1, 1, 1, NULL, 1, 1, '2022-05-08 17:01:38', '2022-05-08 17:03:06', 'دورات تحضير أكاديميّ'),
(27, 4, 22, 'مركز طلاب المدارس', 'مركز طلاب المدارس', 1, 1, 1, NULL, 1, 1, '2022-05-08 17:01:54', '2022-05-08 17:03:16', 'مركز طلاب المدارس'),
(28, 5, 22, 'مركز الموسيقى', 'مركز الموسيقى', 1, 1, 1, NULL, 1, 1, '2022-05-08 17:02:09', '2022-05-08 17:03:24', 'مركز الموسيقى'),
(29, 6, 22, 'مركز ابحاث', 'مركز ابحاث', 1, 1, 1, NULL, 1, 1, '2022-05-08 17:02:22', '2022-05-08 17:02:46', 'مركز ابحاث');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_migrations`
--

DROP TABLE IF EXISTS `iziengdb_migrations`;
CREATE TABLE IF NOT EXISTS `iziengdb_migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_migrations`
--

INSERT INTO `iziengdb_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_04_02_193005_create_translations_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(5, '2019_08_19_000000_create_failed_jobs_table', 1),
(6, '2020_09_11_160850_create_sessions_table', 1),
(7, '2020_09_11_190632_create_webmaster_settings_table', 1),
(8, '2020_09_11_190633_create_webmaster_sections_table', 1),
(9, '2020_09_11_190635_create_webmaster_banners_table', 1),
(10, '2020_09_11_190637_create_webmails_groups_table', 1),
(11, '2020_09_11_190638_create_webmails_files_table', 1),
(12, '2020_09_11_190640_create_webmails_table', 1),
(13, '2020_09_11_190641_create_topics_table', 1),
(14, '2020_09_11_190643_create_settings_table', 1),
(15, '2020_09_11_190645_create_sections_table', 1),
(16, '2020_09_11_190647_create_photos_table', 1),
(17, '2020_09_11_190648_create_permissions_table', 1),
(18, '2020_09_11_190650_create_menus_table', 1),
(19, '2020_09_11_190652_create_maps_table', 1),
(20, '2020_09_11_190654_create_events_table', 1),
(21, '2020_09_11_190656_create_countries_table', 1),
(22, '2020_09_11_190657_create_contacts_groups_table', 1),
(23, '2020_09_11_190659_create_contacts_table', 1),
(24, '2020_09_11_190701_create_comments_table', 1),
(25, '2020_09_11_190703_create_banners_table', 1),
(26, '2020_09_11_190704_create_attach_files_table', 1),
(27, '2020_09_11_190706_create_analytics_visitors_table', 1),
(28, '2020_09_11_190708_create_analytics_pages_table', 1),
(29, '2020_09_11_190912_create_related_topics_table', 1),
(30, '2020_09_11_190914_create_topic_categories_table', 1),
(31, '2020_09_11_190916_create_topic_fields_table', 1),
(32, '2020_09_11_190917_create_webmaster_section_fields_table', 1),
(33, '2020_09_11_201046_create_languages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_password_resets`
--

DROP TABLE IF EXISTS `iziengdb_password_resets`;
CREATE TABLE IF NOT EXISTS `iziengdb_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `iziengDB_password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_permissions`
--

DROP TABLE IF EXISTS `iziengdb_permissions`;
CREATE TABLE IF NOT EXISTS `iziengdb_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_status` tinyint(4) NOT NULL DEFAULT '0',
  `add_status` tinyint(4) NOT NULL DEFAULT '0',
  `edit_status` tinyint(4) NOT NULL DEFAULT '0',
  `delete_status` tinyint(4) NOT NULL DEFAULT '0',
  `active_status` tinyint(4) NOT NULL DEFAULT '0',
  `analytics_status` tinyint(4) NOT NULL DEFAULT '0',
  `inbox_status` tinyint(4) NOT NULL DEFAULT '0',
  `newsletter_status` tinyint(4) NOT NULL DEFAULT '0',
  `calendar_status` tinyint(4) NOT NULL DEFAULT '0',
  `banners_status` tinyint(4) NOT NULL DEFAULT '0',
  `settings_status` tinyint(4) NOT NULL DEFAULT '0',
  `webmaster_status` tinyint(4) NOT NULL DEFAULT '0',
  `data_sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_status` tinyint(4) NOT NULL DEFAULT '0',
  `home_links` text COLLATE utf8mb4_unicode_ci,
  `home_details_ar` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_en` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_ch` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_hi` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_es` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_ru` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_pt` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_fr` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_de` longtext COLLATE utf8mb4_unicode_ci,
  `home_details_th` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `home_details_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_permissions`
--

INSERT INTO `iziengdb_permissions` (`id`, `name`, `view_status`, `add_status`, `edit_status`, `delete_status`, `active_status`, `analytics_status`, `inbox_status`, `newsletter_status`, `calendar_status`, `banners_status`, `settings_status`, `webmaster_status`, `data_sections`, `home_status`, `home_links`, `home_details_ar`, `home_details_en`, `home_details_ch`, `home_details_hi`, `home_details_es`, `home_details_ru`, `home_details_pt`, `home_details_fr`, `home_details_de`, `home_details_th`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `home_details_he`) VALUES
(1, 'Webmaster', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4,5,6,7,8,9,10,11', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2022-04-03 16:51:07', '2022-05-12 12:56:28', NULL),
(2, 'Website Manager', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '1,2,3,4,5,6,7,8,9', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:19', NULL),
(3, 'Limited User', 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, '1,2,3,4,5,6,7,8,9', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_photos`
--

DROP TABLE IF EXISTS `iziengdb_photos`;
CREATE TABLE IF NOT EXISTS `iziengdb_photos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_related_topics`
--

DROP TABLE IF EXISTS `iziengdb_related_topics`;
CREATE TABLE IF NOT EXISTS `iziengdb_related_topics` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `topic2_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_sections`
--

DROP TABLE IF EXISTS `iziengdb_sections`;
CREATE TABLE IF NOT EXISTS `iziengdb_sections` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_sections`
--

INSERT INTO `iziengdb_sections` (`id`, `title_ar`, `title_en`, `photo`, `icon`, `status`, `visits`, `webmaster_id`, `father_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `created_by`, `updated_by`, `created_at`, `updated_at`, `title_he`, `seo_title_he`, `seo_description_he`, `seo_keywords_he`, `seo_url_slug_he`) VALUES
(1, 'أسئلة عامة', 'General Questions', NULL, NULL, 1, 0, 11, 0, 1, 'أسئلة عامة', 'General Questions', NULL, NULL, NULL, NULL, 'asyl-aaam', 'general-questions', 1, NULL, '2022-05-13 17:25:32', '2022-05-13 17:25:32', 'שאלות כלליות', 'שאלות כלליות', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_sessions`
--

DROP TABLE IF EXISTS `iziengdb_sessions`;
CREATE TABLE IF NOT EXISTS `iziengdb_sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iziengDB_sessions_user_id_index` (`user_id`),
  KEY `iziengDB_sessions_last_activity_index` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_settings`
--

DROP TABLE IF EXISTS `iziengdb_settings`;
CREATE TABLE IF NOT EXISTS `iziengdb_settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_desc_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_desc_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_keywords_ar` text COLLATE utf8mb4_unicode_ci,
  `site_keywords_en` text COLLATE utf8mb4_unicode_ci,
  `site_webmails` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_messages_status` tinyint(4) DEFAULT NULL,
  `notify_comments_status` tinyint(4) DEFAULT NULL,
  `notify_orders_status` tinyint(4) DEFAULT NULL,
  `notify_table_status` tinyint(4) DEFAULT NULL,
  `notify_private_status` tinyint(4) DEFAULT NULL,
  `site_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_status` tinyint(4) NOT NULL,
  `close_msg` text COLLATE utf8mb4_unicode_ci,
  `social_link1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_link10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t1_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t1_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t7_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t7_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_fav` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_apple` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_type` tinyint(4) DEFAULT NULL,
  `style_bg_type` tinyint(4) DEFAULT NULL,
  `style_bg_pattern` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_subscribe` tinyint(4) DEFAULT NULL,
  `style_footer` tinyint(4) DEFAULT NULL,
  `style_header` tinyint(4) DEFAULT NULL,
  `style_footer_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_preload` tinyint(4) DEFAULT NULL,
  `css` longtext COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_desc_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_keywords_he` text COLLATE utf8mb4_unicode_ci,
  `contact_t1_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t7_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_settings`
--

INSERT INTO `iziengdb_settings` (`id`, `site_title_ar`, `site_title_en`, `site_desc_ar`, `site_desc_en`, `site_keywords_ar`, `site_keywords_en`, `site_webmails`, `notify_messages_status`, `notify_comments_status`, `notify_orders_status`, `notify_table_status`, `notify_private_status`, `site_url`, `site_status`, `close_msg`, `social_link1`, `social_link2`, `social_link3`, `social_link4`, `social_link5`, `social_link6`, `social_link7`, `social_link8`, `social_link9`, `social_link10`, `contact_t1_ar`, `contact_t1_en`, `contact_t3`, `contact_t4`, `contact_t5`, `contact_t6`, `contact_t7_ar`, `contact_t7_en`, `style_logo_ar`, `style_logo_en`, `style_fav`, `style_apple`, `style_color1`, `style_color2`, `style_type`, `style_bg_type`, `style_bg_pattern`, `style_bg_color`, `style_bg_image`, `style_subscribe`, `style_footer`, `style_header`, `style_footer_bg`, `style_preload`, `css`, `created_by`, `updated_by`, `created_at`, `updated_at`, `site_title_he`, `site_desc_he`, `site_keywords_he`, `contact_t1_he`, `contact_t7_he`, `style_logo_he`) VALUES
(1, 'المركز التعليمي العالمي iziEng', 'iziEng - International Education Center', 'وصف الموقع الإلكتروني ونبذة قصيره عنه', 'Website description and some little information about it', 'كلمات، دلالية، موقع، موقع إلكتروني', 'key, words, website, web', 'info@sitename.com', 1, 1, 1, 0, 0, NULL, 1, 'Website under maintenance \r\n<h1>Comming SOON</h1>', 'https://facebook.com', 'https://twitter.com', NULL, '#', '#', '#', '#', '#', '#', '#', 'المبني - اسم الشارع - المدينة - الدولة', 'Building, Street name, City, Country', '048800177', '048800176', '*3783', 'info@izieng.com', 'من الأحد إلى الخميس 08:00 ص - 05:00 م', 'Sunday to Thursday 08:00 AM to 05:00 PM', '16532335492139.png', '16532335495273.png', NULL, NULL, '#0cbaa4', '#2e3e4e', 0, 0, NULL, NULL, NULL, 1, 1, 0, NULL, 0, NULL, 1, 1, '2022-04-03 16:51:07', '2022-05-23 18:26:29', 'iziEng מרכז חינוך בינלאומי', 'Website description and some little information about it', 'key, words, website, web', 'Building, Street name, City, Country', 'Sunday to Thursday 08:00 AM to 05:00 PM', '16532335499008.png');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_topics`
--

DROP TABLE IF EXISTS `iziengdb_topics`;
CREATE TABLE IF NOT EXISTS `iziengdb_topics` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` longtext COLLATE utf8mb4_unicode_ci,
  `details_en` longtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `photo_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_file` text COLLATE utf8mb4_unicode_ci,
  `audio_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `form_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_he` longtext COLLATE utf8mb4_unicode_ci,
  `seo_title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_topics`
--

INSERT INTO `iziengdb_topics` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `form_id`, `topic_id`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `created_by`, `updated_by`, `created_at`, `updated_at`, `title_he`, `details_he`, `seo_title_he`, `seo_description_he`, `seo_keywords_he`, `seo_url_slug_he`) VALUES
(1, 'من نحن', 'About Us', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', '2022-04-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-04-03 16:51:07', '2022-05-04 19:14:18', 'About Us', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, NULL, NULL, NULL),
(2, 'اتصل بنا', 'Contact Us', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', '2022-04-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 9, 1, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-04-03 16:51:07', '2022-05-23 14:40:29', 'Contact Us', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, NULL, NULL, NULL),
(3, 'الخصوصية', 'Privacy', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', '2022-04-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 1, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-04-03 16:51:07', '2022-05-23 18:35:21', 'Privacy', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, NULL, NULL, NULL),
(4, 'الشروط والأحكام', 'Terms & Conditions', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', '2022-04-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-04-03 16:51:07', '2022-05-23 18:35:24', 'Terms & Conditions', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, NULL, NULL, NULL),
(5, 'الصفحة الرئيسية', 'Home Welcome', '<div style=\'text-align: center\'><h2>مرحبا بكم في موقعنا</h2>\nهناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</div>', '<div style=\'text-align: center\'><h2>Welcome to our website</h2>It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.</div>', '2022-04-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-04-03 16:51:07', '2022-05-08 17:24:52', 'Home Welcome', '<div style=\'text-align: center\'><h2>Welcome to our website</h2>It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.</div>', NULL, NULL, NULL, NULL),
(7, 'ماهو نظام القبول لمؤسسات التعليم العالي العالميّة؟', 'דגכשד', 'يعني أن التقديم على القبول مرة واحدة في بداية العام الدراسي او في بدية الفصل الدراسي الثاني.', NULL, '2022-05-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8, 11, 0, 1, NULL, NULL, 'שדגכדש', 'דגכשד', '', '', NULL, NULL, '', '', 1, 1, '2022-05-12 12:58:37', '2022-05-22 12:31:43', 'שדגכדשגכ', NULL, 'שדגכדשגכ', '', NULL, ''),
(8, 'هل يوجد أولوية للقبول في مؤسسات التعليم العالي العالميّة حسب تاريخ التقديم ؟', 'ranking', 'ليس هناك أولوية في القبول بناء على تاريخ التقديم،وبالتالي لا يؤثر التقديم في أول يوم عن آخر يوم، طالما أنه خلال الفترة المحددة للتقديم.', NULL, '2022-05-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 11, 0, 2, NULL, NULL, 'هل يوجد أولوية للقبول في مؤسسات التعليم العالي العالميّة حسب تاريخ التقديم ؟', 'ranking', 'ليس هناك أولوية في القبول بناء على تاريخ التقديم،وبالتالي لا يؤثر التقديم في أول يوم عن آخر يوم، طالما أنه خلال الفترة المحددة للتقديم.', '', NULL, NULL, 'hl-yogd-aoloy-llkbol-fy-mossat-altaalym-alaaaly-alaaalmy-hsb-tarykh-altkdym', 'ranking', 1, 1, '2022-05-12 13:14:06', '2022-05-13 17:25:54', 'קדימות', NULL, 'קדימות', '', NULL, ''),
(9, 'نتعلم في الناصرة, ونتخرج من بريطانيا', 'نتعلم في الناصرة, ونتخرج من بريطانيا', '<div dir=\"rtl\">نتعلم في الناصرة, ونتخرج من بريطانيا</div>', '<div dir=\"ltr\">نتعلم في الناصرة, ونتخرج من بريطانيا</div>', '2022-05-14', NULL, NULL, '16525302138928.jpg', NULL, NULL, NULL, NULL, 1, 13, 3, 0, 1, NULL, NULL, 'نتعلم في الناصرة, ونتخرج من بريطانيا', 'نتعلم في الناصرة, ونتخرج من بريطانيا', 'نتعلم في الناصرة, ونتخرج من بريطانيا', 'نتعلم في الناصرة, ونتخرج من بريطانيا', NULL, NULL, 'ntaalm-fy-alnasr-ontkhrg-mn-brytanya', 'ntaalm-fy-alnasr-ontkhrg-mn-brytanya', 1, NULL, '2022-05-14 09:10:13', '2022-05-23 18:49:25', 'نتعلم في الناصرة, ونتخرج من بريطانيا', '<div dir=\"rtl\">نتعلم في الناصرة, ونتخرج من بريطانيا</div>', 'نتعلم في الناصرة, ونتخرج من بريطانيا', 'نتعلم في الناصرة, ونتخرج من بريطانيا', NULL, 'ntaalm-fy-alnasr-ontkhrg-mn-brytanya'),
(10, 'لقب أول من بريطانيا', 'لقب أول من بريطانيا', '<div dir=\"rtl\">لقب أول من بريطانيا</div>', '<div dir=\"ltr\">لقب أول من بريطانيا</div>', '2022-05-14', NULL, NULL, '16525302623615.jpg', NULL, NULL, NULL, NULL, 1, 0, 3, 0, 2, NULL, NULL, 'لقب أول من بريطانيا', 'لقب أول من بريطانيا', 'لقب أول من بريطانيا', 'لقب أول من بريطانيا', NULL, NULL, 'lkb-aol-mn-brytanya', 'lkb-aol-mn-brytanya', 1, NULL, '2022-05-14 09:11:02', '2022-05-14 09:11:02', 'لقب أول من بريطانيا', '<div dir=\"rtl\">لقب أول من بريطانيا</div>', 'لقب أول من بريطانيا', 'لقب أول من بريطانيا', NULL, 'lkb-aol-mn-brytanya'),
(11, 'لقب ثالث عن بعد في الناصرة', 'لقب ثالث عن بعد في الناصرة', '<div dir=\"rtl\">لقب ثالث عن بعد في الناصرة</div>', '<div dir=\"ltr\">لقب ثالث عن بعد في الناصرة</div>', '2022-05-14', NULL, NULL, '16525303015237.jpg', NULL, NULL, NULL, NULL, 1, 0, 3, 0, 3, NULL, NULL, 'لقب ثالث عن بعد في الناصرة', 'لقب ثالث عن بعد في الناصرة', 'لقب ثالث عن بعد في الناصرة', 'لقب ثالث عن بعد في الناصرة', NULL, NULL, 'lkb-thalth-aan-baad-fy-alnasr', 'lkb-thalth-aan-baad-fy-alnasr', 1, NULL, '2022-05-14 09:11:41', '2022-05-14 09:11:41', 'لقب ثالث عن بعد في الناصرة', '<div dir=\"rtl\">لقب ثالث عن بعد في الناصرة</div>', 'لقب ثالث عن بعد في الناصرة', 'لقب ثالث عن بعد في الناصرة', NULL, 'lkb-thalth-aan-baad-fy-alnasr'),
(12, 'تخرج الفوق الحادي عشر', 'تخرج الفوق الحادي عشر', '<div dir=\"rtl\">تخرج الفوق الحادي عشر</div>', '<div dir=\"ltr\">تخرج الفوق الحادي عشر</div>', '2022-05-14', NULL, NULL, '16525326559807.jpg', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 4, NULL, NULL, 'تخرج الفوق الحادي عشر', 'تخرج الفوق الحادي عشر', 'تخرج الفوق الحادي عشر', 'تخرج الفوق الحادي عشر', NULL, NULL, 'tkhrg-alfok-alhady-aashr', 'tkhrg-alfok-alhady-aashr', 1, 1, '2022-05-14 09:50:28', '2022-05-23 18:52:06', 'تخرج الفوق الحادي عشر', '<div dir=\"rtl\">تخرج الفوق الحادي عشر</div>', 'تخرج الفوق الحادي عشر', 'تخرج الفوق الحادي عشر', NULL, 'tkhrg-alfok-alhady-aashr');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_topic_categories`
--

DROP TABLE IF EXISTS `iziengdb_topic_categories`;
CREATE TABLE IF NOT EXISTS `iziengdb_topic_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_topic_categories`
--

INSERT INTO `iziengdb_topic_categories` (`id`, `topic_id`, `section_id`, `created_at`, `updated_at`) VALUES
(2, 8, 1, '2022-05-13 17:26:02', '2022-05-13 17:26:02'),
(3, 7, 1, '2022-05-13 17:26:16', '2022-05-13 17:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_topic_fields`
--

DROP TABLE IF EXISTS `iziengdb_topic_fields`;
CREATE TABLE IF NOT EXISTS `iziengdb_topic_fields` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_users`
--

DROP TABLE IF EXISTS `iziengdb_users`;
CREATE TABLE IF NOT EXISTS `iziengdb_users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `connect_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connect_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iziengDB_users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_users`
--

INSERT INTO `iziengdb_users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `photo`, `permissions_id`, `status`, `connect_email`, `connect_password`, `provider`, `provider_id`, `access_token`, `created_by`, `updated_by`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@site.com', NULL, '$2y$10$tLQe2N7EFgA2sKK6i3hVAOFDGGIhvLt8linKByXFLWhlRUxIvZtyW', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmails`
--

DROP TABLE IF EXISTS `iziengdb_webmails`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmails` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci,
  `date` datetime NOT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmails_files`
--

DROP TABLE IF EXISTS `iziengdb_webmails_files`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmails_files` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `webmail_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmails_groups`
--

DROP TABLE IF EXISTS `iziengdb_webmails_groups`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmails_groups` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_webmails_groups`
--

INSERT INTO `iziengdb_webmails_groups` (`id`, `name`, `color`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Support', '#00bcd4', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07'),
(2, 'Orders', '#f44336', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07'),
(3, 'Queries', '#8bc34a', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmaster_banners`
--

DROP TABLE IF EXISTS `iziengdb_webmaster_banners`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmaster_banners` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `row_no` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `desc_status` tinyint(4) NOT NULL,
  `link_status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_webmaster_banners`
--

INSERT INTO `iziengdb_webmaster_banners` (`id`, `row_no`, `title_ar`, `title_en`, `width`, `height`, `desc_status`, `link_status`, `type`, `icon_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `title_he`) VALUES
(1, 1, 'بنرات الرئيسية', 'Home Banners', 1600, 500, 1, 1, 2, 0, 1, 1, 1, '2022-04-03 16:51:07', '2022-05-13 18:41:02', 'Home Banners'),
(2, 2, 'بنرات نصية', 'Text Banners', 330, 330, 1, 1, 0, 1, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Text Banners'),
(3, 3, 'بنرات جانبية', 'Side Banners', 330, 330, 0, 1, 1, 0, 1, 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Side Banners'),
(4, 4, 'المزيد عن IZIENG', 'more about IZIENG', 512, 512, 0, 1, 0, 0, 1, 1, NULL, '2022-05-07 17:59:34', '2022-05-07 17:59:34', 'עוד על IZIENG');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmaster_sections`
--

DROP TABLE IF EXISTS `iziengdb_webmaster_sections`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmaster_sections` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `row_no` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `title_status` tinyint(4) NOT NULL DEFAULT '1',
  `photo_status` tinyint(4) NOT NULL DEFAULT '1',
  `case_status` tinyint(4) NOT NULL DEFAULT '1',
  `visits_status` tinyint(4) NOT NULL DEFAULT '1',
  `sections_status` tinyint(4) NOT NULL DEFAULT '0',
  `comments_status` tinyint(4) NOT NULL DEFAULT '0',
  `date_status` tinyint(4) NOT NULL DEFAULT '0',
  `expire_date_status` tinyint(4) NOT NULL DEFAULT '0',
  `longtext_status` tinyint(4) NOT NULL DEFAULT '0',
  `editor_status` tinyint(4) NOT NULL DEFAULT '0',
  `attach_file_status` tinyint(4) NOT NULL DEFAULT '0',
  `extra_attach_file_status` tinyint(4) NOT NULL DEFAULT '0',
  `multi_images_status` tinyint(4) NOT NULL DEFAULT '0',
  `section_icon_status` tinyint(4) NOT NULL DEFAULT '0',
  `icon_status` tinyint(4) NOT NULL DEFAULT '0',
  `maps_status` tinyint(4) NOT NULL DEFAULT '0',
  `order_status` tinyint(4) NOT NULL DEFAULT '0',
  `related_status` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_webmaster_sections`
--

INSERT INTO `iziengdb_webmaster_sections` (`id`, `row_no`, `title_ar`, `title_en`, `type`, `title_status`, `photo_status`, `case_status`, `visits_status`, `sections_status`, `comments_status`, `date_status`, `expire_date_status`, `longtext_status`, `editor_status`, `attach_file_status`, `extra_attach_file_status`, `multi_images_status`, `section_icon_status`, `icon_status`, `maps_status`, `order_status`, `related_status`, `status`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `created_by`, `updated_by`, `created_at`, `updated_at`, `title_he`, `seo_title_he`, `seo_description_he`, `seo_keywords_he`, `seo_url_slug_he`) VALUES
(1, 1, 'صفحات الموقع', 'Site pages', 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 'صفحات الموقع', 'Site pages', NULL, NULL, NULL, NULL, 'sitepages', 'sitepages', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Site pages', 'Site pages', NULL, NULL, NULL),
(2, 2, 'الخدمات', 'Services', 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 'الخدمات', 'Services', NULL, NULL, NULL, NULL, 'services', 'services', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Services', 'Services', NULL, NULL, NULL),
(3, 3, 'الأخبار', 'News', 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 'الأخبار', 'News', NULL, NULL, NULL, NULL, 'news', 'news', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'News', 'News', NULL, NULL, NULL),
(4, 4, 'الصور', 'Photos', 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 'الصور', 'Photos', NULL, NULL, NULL, NULL, 'photos', 'photos', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Photos', 'Photos', NULL, NULL, NULL),
(5, 5, 'الفيديو', 'Videos', 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 'الفيديو', 'Videos', NULL, NULL, NULL, NULL, 'videos', 'videos', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Videos', 'Videos', NULL, NULL, NULL),
(6, 6, 'الصوتيات', 'Audio', 3, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 'الصوتيات', 'Audio', NULL, NULL, NULL, NULL, 'audio', 'audio', 1, NULL, '2022-04-03 16:51:07', '2022-04-03 16:53:18', 'Audio', 'Audio', NULL, NULL, NULL),
(7, 7, 'مقالات', 'Articles', 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 'المدونة', 'Blog', NULL, NULL, NULL, NULL, 'blog', 'blog', 1, 1, '2022-04-03 16:51:07', '2022-05-08 17:38:16', 'מאמרים', 'Blog', NULL, NULL, NULL),
(8, 8, 'الدورات', 'Courses', 0, 1, 1, 1, 1, 2, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 'المنتجات', 'Products', NULL, NULL, NULL, NULL, 'products', 'products', 1, 1, '2022-04-03 16:51:07', '2022-04-28 20:11:01', 'Products', 'Products', NULL, NULL, NULL),
(9, 9, 'الطاقم', 'Team', 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'العملاء', 'Partners', NULL, NULL, NULL, NULL, 'partners', 'partners', 1, 1, '2022-04-03 16:51:07', '2022-05-08 16:41:31', 'צוות', 'Partners', NULL, NULL, NULL),
(11, 10, 'أسئلة متكررة', 'FAQ', 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'faq', 'faq', 1, 1, '2022-05-12 12:56:28', '2022-05-23 17:41:17', 'שאלות', NULL, NULL, NULL, 'faq');

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmaster_section_fields`
--

DROP TABLE IF EXISTS `iziengdb_webmaster_section_fields`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmaster_section_fields` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `webmaster_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `row_no` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `required` tinyint(4) NOT NULL,
  `in_table` tinyint(4) NOT NULL DEFAULT '0',
  `in_search` tinyint(4) NOT NULL DEFAULT '0',
  `in_listing` tinyint(4) NOT NULL DEFAULT '0',
  `in_page` tinyint(4) NOT NULL DEFAULT '0',
  `in_statics` tinyint(4) NOT NULL DEFAULT '0',
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_permission_groups` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_permission_groups` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edit_permission_groups` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_he` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_he` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iziengdb_webmaster_settings`
--

DROP TABLE IF EXISTS `iziengdb_webmaster_settings`;
CREATE TABLE IF NOT EXISTS `iziengdb_webmaster_settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `seo_status` tinyint(4) NOT NULL,
  `analytics_status` tinyint(4) NOT NULL,
  `banners_status` tinyint(4) NOT NULL,
  `inbox_status` tinyint(4) NOT NULL,
  `calendar_status` tinyint(4) NOT NULL,
  `settings_status` tinyint(4) NOT NULL,
  `newsletter_status` tinyint(4) NOT NULL,
  `members_status` tinyint(4) NOT NULL,
  `orders_status` tinyint(4) NOT NULL,
  `shop_status` tinyint(4) NOT NULL,
  `shop_settings_status` tinyint(4) NOT NULL,
  `default_currency_id` int(11) NOT NULL,
  `languages_by_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latest_news_section_id` int(11) NOT NULL,
  `header_menu_id` int(11) NOT NULL,
  `footer_menu_id` int(11) NOT NULL,
  `home_banners_section_id` int(11) NOT NULL,
  `home_text_banners_section_id` int(11) NOT NULL,
  `side_banners_section_id` int(11) NOT NULL,
  `contact_page_id` int(11) NOT NULL,
  `newsletter_contacts_group` int(11) NOT NULL,
  `new_comments_status` tinyint(4) NOT NULL,
  `links_status` tinyint(4) NOT NULL,
  `register_status` tinyint(4) NOT NULL,
  `permission_group` int(11) NOT NULL,
  `api_status` tinyint(4) NOT NULL,
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_content1_section_id` int(11) NOT NULL,
  `home_content2_section_id` int(11) NOT NULL,
  `home_content3_section_id` int(11) NOT NULL,
  `home_contents_per_page` int(11) NOT NULL,
  `mail_driver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_host` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_port` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_encryption` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_no_replay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_template` longtext COLLATE utf8mb4_unicode_ci,
  `nocaptcha_status` tinyint(4) NOT NULL,
  `nocaptcha_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nocaptcha_sitekey` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_tags_status` tinyint(4) NOT NULL,
  `google_tags_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_analytics_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_facebook_status` tinyint(4) NOT NULL,
  `login_facebook_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_facebook_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_twitter_status` tinyint(4) NOT NULL,
  `login_twitter_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_twitter_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_google_status` tinyint(4) NOT NULL,
  `login_google_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_google_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_linkedin_status` tinyint(4) NOT NULL,
  `login_linkedin_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_linkedin_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_github_status` tinyint(4) NOT NULL,
  `login_github_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_github_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_bitbucket_status` tinyint(4) NOT NULL,
  `login_bitbucket_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_bitbucket_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dashboard_link_status` tinyint(4) NOT NULL,
  `text_editor` tinyint(4) NOT NULL DEFAULT '0',
  `tiny_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iziengdb_webmaster_settings`
--

INSERT INTO `iziengdb_webmaster_settings` (`id`, `seo_status`, `analytics_status`, `banners_status`, `inbox_status`, `calendar_status`, `settings_status`, `newsletter_status`, `members_status`, `orders_status`, `shop_status`, `shop_settings_status`, `default_currency_id`, `languages_by_default`, `latest_news_section_id`, `header_menu_id`, `footer_menu_id`, `home_banners_section_id`, `home_text_banners_section_id`, `side_banners_section_id`, `contact_page_id`, `newsletter_contacts_group`, `new_comments_status`, `links_status`, `register_status`, `permission_group`, `api_status`, `api_key`, `home_content1_section_id`, `home_content2_section_id`, `home_content3_section_id`, `home_contents_per_page`, `mail_driver`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `mail_encryption`, `mail_no_replay`, `mail_title`, `mail_template`, `nocaptcha_status`, `nocaptcha_secret`, `nocaptcha_sitekey`, `google_tags_status`, `google_tags_id`, `google_analytics_code`, `login_facebook_status`, `login_facebook_client_id`, `login_facebook_client_secret`, `login_twitter_status`, `login_twitter_client_id`, `login_twitter_client_secret`, `login_google_status`, `login_google_client_id`, `login_google_client_secret`, `login_linkedin_status`, `login_linkedin_client_id`, `login_linkedin_client_secret`, `login_github_status`, `login_github_client_id`, `login_github_client_secret`, `login_bitbucket_status`, `login_bitbucket_client_id`, `login_bitbucket_client_secret`, `dashboard_link_status`, `text_editor`, `tiny_key`, `timezone`, `version`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 5, 'ar', 3, 1, 2, 1, 2, 3, 2, 1, 1, 0, 0, 3, 0, '402784613679330', 3, 4, 9, 20, 'smtp', '', '', '', '', '', '', '{title}', '{details}', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 1, 1, NULL, 'UTC', '8.6.0', 1, 1, '2022-04-03 16:51:07', '2022-05-14 09:12:41');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
